<?php
    $curlHandle = curl_init("http://empirech.com:8080/AS2/api/user");
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlHandle, CURLOPT_HEADER, true);
    curl_setopt($curlHandle, CURLOPT_NOBODY, true);
    curl_setopt($curl, CURLOPT_REFERER, $_SERVER["HTTP_REFERER"]);

    if ($result = curl_exec($curlHandle)) {
        $code = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        $status = curl_getinfo($curlHandle, CURLINFO_HTTP_STATUS);
        if ($code == 401) {
            $headers = split("\n", $result);
            foreach($headers as $header) {
                $one_header = split(":", $header);
                $digest_response = array();
                if ($one_header[0] == "WWW-Authenticate") {
                    $digest_response = split(",", $one_header[1]);
                    break;
                }
                $digest_cache = array();
                foreach($digest_response as $digest_element) {
                    $digest_element = split("=", preg_replace('/"/', '', trim($digest_element)));
                    $digest_cache[$digest_element[0]] = $digest_element[1];
                }
            }
            $digest_cache = array();
            foreach($digest_response as $digest_element) {
                $digest_element = split("=", preg_replace('/"/', '', trim($digest_element)));
                $digest_cache[$digest_element[0]] = $digest_element[1];
            }

            echo json_encode($digest_cache);
        }
    }
?>
