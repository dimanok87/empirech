<?php
    $curlHandle = curl_init("http://empirech.com:8080/AS2/api/captcha");
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlHandle, CURLOPT_HEADER, true);
    $CKey = false;
    if ($result = curl_exec($curlHandle)) {
        $header = substr($result,0 , curl_getinfo($curlHandle, CURLINFO_HEADER_SIZE));
        $body = substr($result, curl_getinfo($curlHandle, CURLINFO_HEADER_SIZE));
        $headers = split("\n", $header);
        foreach($headers as $one_header) {
            $sp_header = split(":", $one_header);
            if (trim($sp_header[0]) == "CKey") {
                header($one_header);
                break;
            }
        }
        die('data:image/png;base64,'.base64_encode($body));
    }
?>
