//--- Загрузка SYSTEM ---

var SYSTEM, appsFactory;

var preLoaderContainer = $("<div>").addClass("absolute-container preload-bg").append(
    $("<div>").addClass("relative-container").append(
        $("<div>").addClass("pre-loader medium").append("<div>")
    )
);
var showPreloader = function() {
    preLoaderContainer.appendTo(body);
};
//--- Загрузка ресурсов (подарки, персонажи, транспорт, головы, тела) ---
var sourcesList = {
    "heads": {
        size: [2, 3]
    },
    "bodies": {
        size: [2, 3]
    },
    "clothes": {
        size: [2, 3],
        product: true,
        shopFv: {
            type: "fv",
            size: 3
        }
    },
    "models": {
        size: [2, 3],
        product: true
    },
    "pers": {
        size: [2],
        product: true
    },
    "gifts": {
        size: [1],
        product: true,
        shopFv: {
            size: 3
        }
    },
    "magic": {
        size: [2]
    },
    "bg": {
        size: [2]
    },
    "comp": {
        size: [2],
        product: true
    },
    "money": {
        product: true
    },
    "authority": {
        product: true
    }
};

var urlCategories = {
    "gifts": "/proxy/AS2/api/p/giftscat",
    "pers": "/proxy/AS2/api/p/perscat",
    "clothes": "/configs/outfit.json"
};

var startApps = [
    "Background",
    "Content",
    "User",
    "Personages"
];


//--- Стартуем систему ---
var startSystem = function() {
    var cb = function(data) {
        if (data["error"]) return;
        appsFactory("User").Event.addHandlers({
            onInit: function(user) {
                user["setUser"](data);
                SYSTEM["Controller"]("Empirech");
                preLoaderContainer.fadeOut("slow", function() {
                    preLoaderContainer.remove();
                });
            }
        });
    };
    if (!Functions.authUser(false, cb)) {
        SYSTEM["Controller"]("Start");
        preLoaderContainer.fadeOut("slow", function() {
            preLoaderContainer.remove();
        });
    }
};

var onLoadAllSources = function() {
    Functions.loadLanguage(
        $.cookie("lng") || (navigator.language || navigator.systemLanguage || navigator.userLanguage).substr(0, 2).toLowerCase(),
        startSystem
    );
};

var loadResources = function() {
    var allSize = 0, loadedSize = 0;
    for (var k in sourcesList) {
        allSize++;
    }
    for (var k in sourcesList) {
        Functions.loadSource({
            name: k,
            params: sourcesList[k]
        }, function() {
            loadedSize++;
            if (loadedSize == allSize) {
                onLoadAllSources();
            }
        });
    }
};

var loadCategories = function() {
    var allSize = 0, loadedSize = 0;
    for (var k in urlCategories) {
        allSize++;
    }
    for (var k in urlCategories) {
        Functions.loadCategory({
            name: k,
            url: urlCategories[k]
        }, function() {
            loadedSize++;
            if (loadedSize == allSize) {
                loadResources();
            }
        });
    }
};

var loadBasedApplications = function() {
    var loadedApps = 0;
    for (var k = 0; k < startApps.length; k++) {
        appsFactory(startApps[k]).Event.addHandlers({
            onInit: function() {
                loadedApps++;
                if (loadedApps == startApps.length) {
                    appsFactory("Background")["setBackground"]("images/backgrounds/login.jpg");
                    loadCategories();
                }
            }
        });
    }
    showPreloader();
};
$(function() {
    Functions.loadApplication("_SYSTEM_", function(app) {
        appsFactory = (SYSTEM = new app())["Application"];
        loadBasedApplications();
    });
});

