var jqElements = {
    'main-block': $('<div>').addClass('menu-block-container')
};

var mainMenuBlock = $('<ul>').addClass('menu-block app-menu main-menu').appendTo(jqElements['main-block']);

var dopMenuContainer = $('<div>').addClass('dop-main-menu').appendTo(jqElements['main-block']);
var dopMenuBlock = $('<ul>').addClass('menu-block app-menu dop-menu').appendTo(dopMenuContainer);


this.getDopMenuContainer = function() {
    return dopMenuContainer;
};
this.activateForDrag = function() {
    dopMenuContainer.addClass("actived");
};
this.deActivateForDrag = function() {
    dopMenuContainer.removeClass("actived");
};

this.getJQElements = function(name) {
    return jqElements[name];
};

var options = false;
var config;
var selectedItem = false;
var menuApp = this;
var namedItemsMenu = {};


this.getItem = function(label) {
    return namedItemsMenu[label] || false;
};

var forTranslate = {};

var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        var t = TRANSLATES[i] || i;
        forTranslate[i].text(t).attr('title', t);
    }
};

var handlerChange = function(itemMenu, itemParam) {
    var handlers = {};
    if (itemParam['action']) {
        itemMenu.addClass('action-method');
    }

    handlers['click'] = function () {
        if (itemMenu.is(selectedItem)) return;
        if (!(itemParam['action'] || false)) {
            selectedItem ? selectedItem.removeClass('active') : false;
            selectedItem = itemMenu.addClass('active');
        }
        var returnParams = {};
        for (var i in itemParam) {
            returnParams[i] = itemParam[i];
        }
        returnParams["button"] = itemMenu;
        menuApp.Event.callHandlers("onChange", returnParams);
    };
    return handlers;
};

this.setSelection = function(itemMenu, itemParam) {
    if (!itemMenu) {
        selectedItem ? selectedItem.removeClass('active') : false;
        selectedItem = false;
        return;
    }
    if (itemMenu.is(selectedItem)) return;
    if (!(itemParam['action'] || false)) {
        selectedItem ? selectedItem.removeClass('active') : false;
        selectedItem = itemMenu.addClass('active');
    }
    itemParam["button"] = selectedItem;
};

this.resetSelection = function() {
    selectedItem ? selectedItem.removeClass('active') : false;
    selectedItem = false;
};

var createItemMenu = function(itemParam, dop) {
    dop = dop || false;
    var itemMenu = $('<li>').appendTo(!dop ? mainMenuBlock : dopMenuBlock).addClass("item-main-menu");
    var itemMenuContent;
    itemMenu.append(
        itemMenuContent = $('<div>').addClass('item-menu-content').append(
            forTranslate[itemParam['title']] = $('<span>').text(TRANSLATES[itemParam['title']])
        ).addClass(itemParam['label'])
    );
    namedItemsMenu[itemParam["label"]] = itemMenuContent;
    itemMenuContent.bind(handlerChange(itemMenuContent, itemParam));

    if (dop) {
        var deleteButton = $("<div>").addClass("close-button").appendTo(itemMenu).hide().click(function() {
            itemMenu.remove();
            var newDopItems = [], newNamedItemsMenu = {}, newDopItemsMenu = [];
            for (var k = 0; k < dopItems.length; k++) {
                if (itemParam != dopItems[k]) {
                    newDopItems.push(dopItems[k]);
                    newNamedItemsMenu[dopItems[k]["label"]] = namedItemsMenu[dopItems[k]["label"]];
                }
            }
            for (var k = 0; k < dopItemsMenu.length; k++) {
                if (itemMenu != dopItemsMenu[k]) {
                    newDopItemsMenu.push(dopItemsMenu[k]);
                }
            }
            dopItemsMenu = newDopItemsMenu;
            namedItemsMenu = newNamedItemsMenu;
            dopItems = newDopItems;
            menuApp.Event.callHandlers("onChangeMenu", dopItems);
            iniHeight();
        });

        var timeOutShowButton = true;
        var timeOutHideButton = true;

        itemMenu["mouseover"](function() {
            if (timeOutHideButton && timeOutHideButton !== true) {
                if (timeOutHideButton !== true)
                    clearTimeout(timeOutHideButton);
            } else {
                timeOutShowButton = setTimeout(function() {
                    timeOutShowButton = false;
                    deleteButton.fadeIn("fast");
                }, 800)
            }
        })["mouseout"](function() {
            if (timeOutShowButton) {
                if (timeOutShowButton !== true)
                    clearTimeout(timeOutShowButton);
            } else {
                timeOutHideButton = setTimeout(function() {
                    timeOutHideButton = false;
                    deleteButton.fadeOut("fast");
                }, 400);
            }
        });

    }
    return itemMenu;
};

var dopItemsMenu = [];

var iniHeight = this.iniHeight = function() {
    if (!dopItemsMenu.length) return;

    var roundItems = Math.floor(dopMenuContainer.height() / dopItemsMenu[0].height());
    if (roundItems < dopItemsMenu.length) {
        var visibledItems = roundItems - 1;
        if (!jqElements["more-items"]) {
            var isShowedDop = false;
            jqElements["more-items"] = $('<div>').addClass("more-item item-main-menu").
                append($('<div>').addClass('item-menu-content').addClass('more')
                ).click(function() {
                    !isShowedDop ?
                        jqElements["more-items-list"].show() :
                        jqElements["more-items-list"].hide();
                    isShowedDop = !isShowedDop;
                    jqElements["more-items"][isShowedDop ? "addClass" : "removeClass"]("active");
                });
            jqElements["more-items-list"] = $('<ul>').hide().addClass("menu-block app-menu more-items");
        }
        jqElements["more-items-list"].appendTo(options["more-container"]);
        for (var i = 0; i < dopItemsMenu.length; i++) {
            if (i + 1 > visibledItems) {
                jqElements["more-items-list"].append(dopItemsMenu[i]);
            } else {
                dopMenuBlock.append(dopItemsMenu[i]);
            }
        }
        jqElements["more-items"].appendTo(jqElements["main-block"]);
    } else {
        if (jqElements["more-items"]) {
            jqElements["more-items"].detach();
            jqElements["more-items-list"].detach();
        }
        for (var i = 0; i < dopItemsMenu.length; i++) {
            dopMenuBlock.append(dopItemsMenu[i]);
        }
    }
};

var dopItems = [];

var dopItemCreate = this.addItem = function(params) {
    for (var k = 0; k < dopItems.length; k++) {
        if (params["label"] == dopItems[k]["label"]) {
            return false;
        }
    }
    dopItemsMenu.push(createItemMenu(params, true));
    dopItems.push(params);
    return dopItems;
};

var iniMenu = function() {
    $.ajax({
        url: 'configs/Menu.json',
        dataType: 'json',
        async: false,
        success: function(json) {
            config = json;
            for (var i = 0; i < config.length; i++) {
                createItemMenu(config[i]);
            }
            var dopMenu = JSON.parse($.cookie("menu") || "[]");
            for (var k = 0; k < dopMenu.length; k++) {
                dopItemCreate(dopMenu[k]);
            }
            menuApp.onInit();
            iniHeight();
        }
    });
};

this._constructor = function(opt) {
    options = opt;
    iniMenu();
    this.Event.addHandlers({
        onClose: function() {
            jqElements["more-items-list"] ?
                jqElements["more-items-list"].remove() :
                false;
        }
    });
    win.resize(iniHeight);
};
