var jqElements = {
    'main-block': $('<div>').addClass('user-panel-block'),
    'user_avatar': $('<form>').addClass('avatar-block').attr("enctype", "multipart/form-data"),
    'user_name_container': $('<div>').addClass('name-block'),
    'user_params': $('<ul>').addClass('params-block')
};

jqElements['user_rank']     = $('<li>').appendTo(jqElements['user_params']).addClass('user-rank').text(5);
jqElements['user_money']    = $('<li>').appendTo(jqElements['user_params']).addClass('user-money').text(100);
jqElements['user_add_money']      =
    $('<li>').appendTo(jqElements['user_params']).
    addClass('user-add-money').html($('<span>').addClass('icon-user user-add-money'));
jqElements['avatar_container'] = $('<div>').addClass('avatar-container').appendTo(jqElements["user_avatar"]);

jqElements['user_name'] = $('<span>').appendTo(jqElements['user_name_container']);
jqElements['gender-icon'] = $('<span>').appendTo(jqElements['user_name_container']).addClass("gender-icon");

var options = false;

this.getJQElements = function(name) {
    return jqElements[name];
};

var userParams = {
    'user_name': false,
    'user_rank': 0,
    'user_money': 0
};

var setUserData = function(a, b) {
    switch(a) {
        case "user_avatar":
            setAvatar(b);
            break;
        case "gender":
            jqElements['gender-icon'].addClass(b);
            break;
        default:
            jqElements[a] ? jqElements[a].text(userParams[a] = b) : false;
            break;
    }
};

this.setUserData = function(a, b) {
    if (typeof a == "object") {
        for (var n in a) {
            setUserData(n, a[n]);
        }
    } else {
        setUserData(a, b);
    }
};

this.setUserBalance = function(balance) {
    userParams["user_money"] = balance;
    jqElements['user_money'].text(balance);
};

var thisApp = this;
var avatarImage = false;
var setAvatar = function(src) {
    if (!src) return;
    if (!avatarImage) {
        avatarImage = new Image();
        avatarImage.onload = function() {
            $(avatarImage).fadeIn(function() {
                jqElements['avatar_container'].css({
                    background: '#fff'
                });
            });
            Functions.setImageToBlock(avatarImage, jqElements['avatar_container']);
        };
        $(avatarImage).appendTo(jqElements['avatar_container']);
    }
    $(avatarImage).fadeOut(function() {
        avatarImage.src = src;
    });
};

this._constructor = function() {
    jqElements['main-block'].append(
        jqElements['user_avatar'],
        jqElements['user_name_container'],
        jqElements['user_params']
    );

    $('<input>').
        change(function() {
            Functions.getBase64Image(this.files[0], function(base64) {
                setAvatar(base64);
            });
            Functions.sendForm('api/user/view/photo', jqElements['user_avatar']);
        }).
        appendTo(jqElements['user_avatar']).
        attr({type: "file", name: "file"});

    this.onInit();
};
