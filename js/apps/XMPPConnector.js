var XMPPCONST = {
    URL: "/http-bind/",
    XMPP_DOMAIN: "empirech.com",
    ROOM: "@muc.empirech.com",
    ROOM_DEF_PREF: "+"
};

var roomChatId = false;

var getPresenceInfo = function(presence) {
    var xdata = presence.find("x:first"), itemdata = presence.find("item:first");
    if (!itemdata.length) itemdata = xdata;
    var from = presence.attr("from");
    if (!itemdata.length) {
        return false
    }
    roomChatId = from.split("@")[0];
    return {
        type:   presence.attr("type"),
        from:   from,
        to:     presence.attr("to"),
        user: {
            id: itemdata.attr("nick"),
            role: itemdata.attr("role"),
            affiliation: itemdata.attr("affiliation")
        },
        recipient: (xdata.attr("xmln") || "").split("#")[1] || false
    };
};

var isValidJSON = function(src) {
    var filtered = src;
    filtered = filtered.replace(/\\["\\\/bfnrtu]/g, '@');
    filtered = filtered.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
    filtered = filtered.replace(/(?:^|:|,)(?:\s*\[)+/g, '');
    return (/^[\],:{}\s]*$/.test(filtered));
};
var parseDate = function (dateString) {
    var strings = dateString.split("T"),
        year    = parseInt(strings[0].substr(0, 4)),
        month   = strings[0].substr(4, 2) - 1,
        day     = strings[0].substr(6, 2),
        hours   = strings[1].substr(0, 2),
        minutes = strings[1].substr(3, 2),
        seconds = strings[1].substr(6, 2);
    return new Date(Date.UTC(year, month, day, hours, minutes, seconds));
};

var getMessageInfo = function(message) {
    var xdata   = message.find("x:first"),
        text    = message.find("body:first").text(),
        subject = message.find("subject:first"),
        response = {
            type: message.attr("type"),
            from: message.attr("from").split("/"),
            to: message.attr("to")
        };
    if (!text.length) {
        return false;
    }
    if (!response.from[1]) {
        if (isValidJSON(text)) {
            response["message"] = {
                type: "json",
                json: JSON.parse(text)
            }
        }
    } else {
        response["message"] = {
            type: "text",
            text: text
        }
    }
    if (xdata.length) {
        response["xdata"] = {
            date: parseDate(xdata.attr("stamp")),
            from: xdata.attr("from")
        };
    }
    return response;
};

this.sendMessage = function(msg) {
    var message = $msg({
        from: connection["jid"],
        type: 'groupchat',
        to: roomChatId + "@muc.empirech.com"
    }).c('body').t(msg);
    connection.send(message);
};

var connection;

this.getRoster = function(cb) {
    connection.roster.get(function(data) {
        cb ? cb(data) : false;
    });
};

var xmlInput = function(a) {
    var bodyMsg = $(a),
        messages =  bodyMsg.find("message"),
        presences = bodyMsg.find("presence"),
        presencesResponse = [],
        messagesResponse = [];
    presences.each(function() {
        var pres = getPresenceInfo($(this));
        pres ? presencesResponse.push(pres) : false;
    });

    messages.each(function() {
        var msg = getMessageInfo($(this));
        msg ? messagesResponse.push(msg) : false;
    });

    XMPPEvent.callHandlers("onPresences", presencesResponse);
    XMPPEvent.callHandlers("onMessages", messagesResponse);
};

var connectRoom = function(userId, roomName) {
    connection = new Strophe.Connection(XMPPCONST.URL);
    connection.xmlInput = xmlInput;
    connection.connect(
        userId + "@" + XMPPCONST.XMPP_DOMAIN, null,
        function(stat) {
            if (stat == Strophe.Status.CONNECTED) {
                connection.muc.join(roomName, userId);
            }
        }
    );
};

var XMPPEvent = false;
var defaultRoom = XMPPCONST.ROOM_DEF_PREF + Functions.getLanguage() + XMPPCONST.ROOM;

this.join = function(userId, community) {
    if (community) {
        Functions.sendToAPI("api/c/" + community + "/j", function() {
            connectRoom(userId, community);
        });
    } else {
        connectRoom(userId, defaultRoom);
    }
};

this._constructor = function(params) {
    XMPPEvent = this.Event;
    this.onInit();
};
