var jqElements = {
    "main-block": $('<ul>').addClass('language-start-select')
};

this.getJQElements = function(name) {
    return jqElements[name];
};
var selectedItem = false;

var createItemMenu = function(code, title) {
    var itemMenu = $('<li>').appendTo(jqElements['main-block']).text(title);
    itemMenu.click(function() {
        if (itemMenu.is(selectedItem)) {
            return;
        }
        itemMenu.prependTo(jqElements['main-block']);
        selectedItem ? selectedItem.removeClass("selected") : false;
        selectedItem = itemMenu.addClass("selected");
        app["Event"]["callHandlers"]("onChange", code);
    });
};
var app = this;
this._constructor = function() {
    $.ajax({
        url: 'configs/languages.json',
        dataType: 'json',
        async: false,
        success: function(config) {
            for (var i in config) {
                createItemMenu(i, config[i]);
            }
            app.onInit();
        }
    });
};
