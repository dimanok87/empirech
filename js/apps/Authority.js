var jqElements = {
    'main-block': $('<div>').addClass('authority-application')
};

var contentApp = $("<div>").addClass("content-app shop-cart-element").appendTo(jqElements["main-block"]);

this.getJQElements = function(name) {
    return jqElements[name];
};

//--- Объект с блоками для перевода ---
var forTranslate = {};
this.setUserBalance = function(balance) {
    myBalance.html(userBalance = balance);
};

//--- Добавление блока с текстом для перевода ---
var addToTranslate = function(label, element) {
    forTranslate[label] = (forTranslate[label] || []);
    forTranslate[label].push(element.attr("placeholder", TRANSLATES[label] || label));
    return element;
};

var topNavigation = $("<div>").addClass("shop-top-panel");

//--- Блок для баланса пользователя ---
var myBalance = $("<span>").text(0);

//--- Блок для стоимости выбранных товаров ---
var fullAmount = $("<span>").text(0);

//--- Кнопка "перейти в корзину" / "купить" ---
var buyButton = $("<div>").addClass("nav-shops-button").append(
        $("<span>").addClass("buy-icon")
    ).click(function() {
    }
);

var app = this;

var addToDesires = function() {
    addingIndicator.text("+").hide().appendTo(desButton).show("fast");
    Functions.sendToAPI("api/user/wishes", function() {
        app.Event.callHandlers("onAddToDesires");
        setTimeout(function() {
            addingIndicator.hide("fast", function() {
                addingIndicator.detach();
            })
        }, 2000);
    }, "POST", [])
};

var addingIndicator = $("<div>").addClass("adding-indicator");

//--- Кнопка "Добавить в желания" ---
var desButton = $("<div>").addClass("nav-shops-button top-index").append(
        $("<span>").addClass("desires-icon")
    ).click(function() {
        addToDesires();
    });

var userBalance = 0;

topNavigation.append(
    $("<span>").addClass("my-money").append(
        myBalance,
        $("<span>").addClass("money-icon")
    ),
    $("<span>").addClass("amount-money").append(
        fullAmount,
        $("<span>").addClass("money-icon")
    ),
    buyButton,
    desButton
).appendTo(jqElements["main-block"]);

var selectedPrice;

var priceLabel = function(params) {
    var item = $("<label>");
    var radio = $("<input>").attr({
        type: "radio",
        name: "authority"
    }).change(function() {
        selectedPrice = params;
        setAmount();
    });
    item.append(
        radio,
        $("<span>"),
        $("<span>").addClass("label-text-radio").append(
            $("<span>").addClass("price-product").append(
                "+" + params["count"],
                $("<span>").addClass("rank-icon")
            )
        )
    );
    if (!selectedPrice) {
        selectedPrice = params;
        setAmount();
    }
    this.getItem = function() {
        return item;
    };
};

var setAmount = function() {
    fullAmount.html(selectedPrice["price"] / 100);
};

var createPriceList = function(price) {
    var priceList = $("<div>").addClass("price-select").appendTo(contentApp);
    for (var i = 0; i < price.length; i++) {
        var label = new priceLabel(price[i]);
        priceList.append(label.getItem());
    }
};

var createContent = function() {
    var genderImage = new Image();
    genderImage.src = "images/authority/" + gender + ".png";
    $(genderImage).addClass("float-left");
    contentApp.prepend(genderImage);
    createPriceList(price);
};


var User, gender, price;

this._constructor = function(params) {
    User = params["user"];
    price = params["price"];
    gender = User["male"] ? "male" : "female";
    createContent();
    this.onInit();
};
