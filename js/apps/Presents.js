var jqElements = {
    'main-block': $('<div>').addClass('character-application')
};

this.getJQElements = function(name) {
    return jqElements[name];
};

//--- Объект с блоками для перевода ---
var forTranslate = {};

//--- Добавление блока с текстом для перевода ---
var addToTranslate = function(label, element) {
    forTranslate[label] = (forTranslate[label] || []);
    forTranslate[label].push(element.text(TRANSLATES[label] || label));
    return element;
};

//--- Установка нового языка ---
this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k = 0; k < forTranslate[i].length; k++) {
            forTranslate[i][k].text(TRANSLATES[i] || i);
        }
    }
};

//==================== Управление показом прелоадера ====================

//--- Блок с прелоадером ---
var preLoaderContainer = $("<div>").addClass("absolute-container shop-loader").append(
    $("<div>").addClass("relative-container").append(
        $("<div>").addClass("pre-loader medium").append("<div>")
    )
);

//--- Показываем ---
var showShopPreLoader = function(f) {
    f = f || false;
    preLoaderContainer[f ? "appendTo" : "prependTo"](jqElements["main-block"])
};

//--- Скрываем ---
var hideShopPreLoader = function() {
    preLoaderContainer.detach();
};

//==================== / Управление показом прелоадера ====================

//--- Добавление в желания ---
var app = this;

var addToDesires = function() {
    var products = shopApplication.getSelectedProducts(),
        desItems = [];

    for (var k = 0; k < products.length; k++) {
        var itemParams = products[k]["getParams"]();
        desItems.push({
            id: itemParams["id"],
            count: 1
        });
    }
    addingIndicator.text("+" + desItems.length).hide().appendTo(desButton).show("fast");
    Functions.sendToAPI("api/user/wishes", function() {
        app.Event.callHandlers("onAddToDesires");
        setTimeout(function() {
            addingIndicator.hide("fast", function() {
                addingIndicator.detach();
            })
        }, 2000);
    }, "POST", desItems)
};

//================================== Правй блок с кнопками ==================================

//--- Линии для кнопок в правом баре ---
var linesButton = {"line-1": $("<div>").addClass("buttons-line"), "line-2": $("<div>").addClass("buttons-line")};

//--- Кнопки правого бара магазина ---

// Перейти к моим продуктам
var myProductsButton = $("<div>").addClass("nav-shops-button float-left").append(
        $("<span>").addClass("gifts-icon")
    ).click(function() {
        showMyProducts();
    }).appendTo(linesButton["line-1"]);

// Перейти в магазин
var shopButton = $("<div>").addClass("nav-shops-button float-right").append(
        $("<span>").addClass("shop-icon")
    ).click(function() {
        showShowCase();
    }).appendTo(linesButton["line-1"]);

// Применить выбранное
var categoriesButton = $("<div>").addClass("nav-shops-button categories-button").append(
        $("<span>").addClass("categories-icon")
    ).click(function() {
        showCategoriesMenu();
    });

var shopNavigation = $("<div>").addClass('nav-shops').append(linesButton["line-1"], linesButton["line-2"]);
shopNavigation.appendTo(jqElements["main-block"]);
//================================= / Правй блок с кнопками =================================

//==================================== Верхний зелёный бар ==================================

//--- Верхний бар магазина ---
var topNavigation = $("<div>").addClass("shop-top-panel");

//--- Блок для баланса пользователя ---
var myBalance = $("<span>").text(0);

//--- Блок для стоимости выбранных товаров ---
var fullAmount = $("<span>").text(0);

//--- Кнопка поиска ---
var searchButton = $("<div>").addClass("nav-shops-button").append(
    $("<span>").addClass("search-icon")
);

//--- Кнопка "перейти в корзину" / "купить" ---
var buyButton = $("<div>").addClass("nav-shops-button").append(
        $("<span>").addClass("buy-icon")
    ).click(function() {

    }
);

var addingIndicator = $("<div>").addClass("adding-indicator");
//--- Кнопка "Добавить в желания" ---
var desButton = $("<div>").addClass("nav-shops-button top-index").append(
        $("<span>").addClass("desires-icon")
    ).click(function() {
        addToDesires();
    });

var shopNav = $("<div>")


topNavigation.append(
    $("<span>").addClass("my-money").append(myBalance, $("<span>").addClass("money-icon")),
    shopNav.append(
        $("<span>").addClass("amount-money").append(fullAmount, $("<span>").addClass("money-icon")),
        searchButton
    )
).appendTo(jqElements["main-block"]);


var userBalance = 0;
this.setUserBalance = function(balance) {
    myBalance.html(userBalance = balance);
};

//=================================== / Верхний зелёный бар =================================

//=================================== Меню выбора категории ===================================

var categoriesMenu = $("<div>").addClass("categories-list").click(function() {
    showShowCase();
});

var categoriesList = $("<ul>").addClass("categories-menu gifts-menu").appendTo(categoriesMenu);

var fullviewContainer = $("<div>").addClass("relative-container fullview-container");
var sliderList = $("<ul>").addClass("slider-list");

var showNewSlide = function(rangeIndex) {
    var oldSlide = activeSlide;
    var oldIndex = activeSlide.getIndex();
    var newIndex = oldIndex + rangeIndex;
    var allProducts = activeSlide.getShowcase();
    var maxIndex = allProducts.length - 1;
    newIndex = newIndex > maxIndex ? 0 : newIndex < 0 ? maxIndex : newIndex;
    var newSlide = activeSlide = allProducts[newIndex];
    var oldSlideBlock = oldSlide.getItemBlock();
    var newSlideBlock = newSlide.getItemBlock();
    if ((oldIndex < newIndex && !(newIndex == maxIndex && oldIndex == 0)) || (oldIndex == maxIndex && newIndex == 0)) {
        sliderList.append(newSlideBlock);
        oldSlideBlock.animate({
            "margin-left": "-100%"
        }, function() {
            oldSlide.detachPreview();
            oldSlideBlock.css({"margin-left": "0%"});
        });
        newSlide.iniHeightPreview();
    } else {
        sliderList.prepend(newSlideBlock);
        newSlideBlock.css({
            "margin-left": "-100%"
        }).animate({
                "margin-left": "0%"
            }, function() {
                oldSlide.detachPreview();
            });
        newSlide.iniHeightPreview();
    }
};

fullviewContainer.append(
    sliderList,
    $("<div>").addClass("absolute-container").click(function() {
        showShowCase();
    }),
    $("<div>").addClass("changer-button next-element").click(function() {
        showNewSlide(1);
    }),
    $("<div>").addClass("changer-button prev-element").click(function() {
        showNewSlide(-1);
    })
);

var previewItems = {};
var activeSlide = false;

var itemFull = function(item, nameCat) {
    var itemBlock = item.getProductContainer();
    var itemParams = item.getParams();
    var index = previewItems[nameCat].length;
    var previewSlideImage = new Image();
    var previewItem = $("<li>");
    var created = false;
    previewItems[nameCat].push(this);
    this.getShowcase = function() {
        return previewItems[nameCat];
    };
    this.select = function() {
        previewCheckbox.get(0).checked = true;
    };
    this.unselect = function() {
        previewCheckbox.get(0).checked = false;
    };
    var previewCheckbox = $("<input>").
        attr("type", "checkbox").
        change(function() {
            previewCheckbox.is(":checked") ?
                item.selectItem() :
                item.unSelectItem();
        });
    this.detachPreview = function() {
        previewItem.detach();
        win.unbind("resize", iniHeightPreview);
    };
    var createItem = function() {
        created = true;
        var loader = $("<div>").
            addClass("pre-loader medium").
            append("<div>").
            appendTo(previewItem);
        $("<label>").
            addClass("fullview-checkbox").
            append(
                previewCheckbox,
                $("<span>"),
                $("<span>").text(itemParams["name"])
            ).appendTo(previewItem);

        previewSlideImage['onload'] = function() {
            loader.detach();
            previewItem.append(previewSlideImage);
            iniHeightPreview();
        };
        previewSlideImage.src = itemParams["views"]["fv"];
    };
    var iniHeightPreview = this.iniHeightPreview = function() {
        $(previewSlideImage).css({
            marginTop: -$(previewSlideImage).height() / 2
        });
    };
    this.getItemBlock = function() {
        if (!created) {
            createItem();
        }
        win.bind("resize", iniHeightPreview);
        return previewItem;
    };
    this.getIndex = function() {
        return index;
    };
    var itemObject = this;
    itemBlock.click(function() {
        activeSlide = itemObject;
        showFullPreview();
    });
};

var createItemCategoriesMenu = function(name, params) {
    $("<li>").
        addClass("categories-item " + params["label"]).
        append(
            $("<div>").append(
                addToTranslate(params["title"], $("<span>"))
            )
        ).appendTo(categoriesList).click(function() {
            if (!showCases[name]) {
                showCases[name] = shopApplication.getShowCase(name);
                var products = showCases[name].getProducts();
                for (var k = 0; k < products.length; k++) {
                    products[k].setData("fvItem", new itemFull(products[k], name));
                }
            }
            showedShowcase = showCases[name];
        });
    previewItems[name] = [];
};

//=================================== / Меню выбора категории ===================================


var shopApplication = false;

var amountCount = 0, oldAmount;

var setAmount = function(range) {
    oldAmount = amountCount;
    amountCount+= range;
    fullAmount.html(amountCount / 100);

    if (oldAmount && !amountCount) {
        buyButton.detach();
        desButton.detach();
    } else if (!oldAmount && amountCount) {
        searchButton.before(buyButton, desButton);
    }
};


//--- Функция установки магазина ---
this.setShopApplication = function(shop) {
    shopApplication = shop;
    shopApplication.setHandlers({
        addSelection: function(item) {
            setAmount(item.getCartItem().getSelectedPriceItem()["new"]["price"]);
            item.getData("fvItem").select();
        },
        delSelection: function(item) {
            setAmount(-item.getCartItem().getSelectedPriceItem()["new"]["price"]);
            if (!shopApplication.getSelectedProducts().length && showedContent == "cart") {
                showShowCase();
            }
            item.getData("fvItem").unselect();
        },
        changeSelection: function(item) {
            var priceItem = item.getCartItem().getSelectedPriceItem();
            setAmount(priceItem["new"]["price"] - priceItem["old"]["price"]);
        }
    });
};

var showedContent = false,
    showedBlock = false,
    showedShowcase = false;

//--- Снимаем класс с активной кнопочки и убирем контент ---
var deActivateContent = function() {
    showedBlock ? showedBlock.detach() : false;
    switch (showedContent) {
        case "showcase":
            categoriesButton.detach();
            shopButton.removeClass("active");
            break;
        case "myproducts":
            myProductsButton.removeClass("active");
            shopNavigation.after(topNavigation);
            break;
        case "preview":
            activeSlide.detachPreview();
            topNavigation.before(shopNavigation);
            break;
    }
};

var showFullPreview = function() {
    deActivateContent();
    showedContent = "preview";
    shopNavigation.detach();
    jqElements["main-block"].append(showedBlock = fullviewContainer);
    sliderList.append(activeSlide.getItemBlock());
    activeSlide.iniHeightPreview();
};

var showCases = {};
var showShowCase = function() {
    if (!showedShowcase) {
        if (showedContent != "categories") {
            showCategoriesMenu();
        }
        return;
    }

    if (showedContent == "showcase") return;

    if (showedContent == "categories") {
        jqElements["main-block"].prepend(shopNavigation, topNavigation);
    }

    categoriesButton.appendTo(linesButton["line-2"]);
    deActivateContent();
    shopButton.addClass("active");
    showedContent = "showcase";
    jqElements["main-block"].append(showedBlock = showedShowcase.getContainer());
    showedShowcase.restoreScroll();
    showedShowcase.iniProductsImages();
};


var showMyProducts = function() {
    if (showedContent == "myproducts") return;
    deActivateContent();
    topNavigation.detach();
    showedContent = "myproducts";
    myProductsButton.addClass("active")
    loadMyProducts(function() {

    });
};

var showCategoriesMenu = function() {
    deActivateContent();
    topNavigation.detach();
    shopNavigation.detach();
    showedContent = "categories";
    jqElements["main-block"].prepend(showedBlock = categoriesMenu);
};

//--- Мои продукты ---
var myProducts = [];

//=== Загрузка моих товаров ===
var loadMyProducts = function(cb) {
    showShopPreLoader();
    Functions.sendToAPI("api/user/p/" + User["id"] + "/gifts", function(data) {
        myProducts = data;
        cb(data);
        hideShopPreLoader();
    });
};

//=== Загрузка категорий товаров ===

var Categories = Functions.getSources()["categories"]["gifts"];

var createCatMenu =  function() {
    for (var i in Categories) {
        createItemCategoriesMenu(i, Categories[i]);
    }
};

//=== Обработчик отображения приложения ===
this.onShow = function() {
    switch (showedContent) {
        case "showcase":
            showedShowcase.restoreScroll();
            showedShowcase.iniProductsImages();
            break;
        case "preview":
            activeSlide.iniHeightPreview();
            break;
    }
};

var User = false;

this._constructor = function(params) {
    createCatMenu();
    User = params["user"];
    loadMyProducts(function() {
        if (!myProducts.length) {
            showShowCase();
        } else {
            showMyProducts();
        }
    });
    this.onInit();
};
