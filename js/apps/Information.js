var jqElements = {
    'main-block': $("<div>").addClass('avatar-application'),
    'menu-block': $('<ul>').addClass('app-menu info-app')
};
jqElements['menu-block'].appendTo(jqElements['main-block']);
var itemsMenu = [];
this.getJQElements = function(name) {
    return jqElements[name];
};
var forTranslate = {};
var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        var t = TRANSLATES[i] || i;
        forTranslate[i].text(t).attr('title', t);
    }
};
var menuApp = this;

this.getItems = function() {
    return itemsMenu;
};

var createItemMenu = function(itemParam) {
    var itemMenu = $('<li>').appendTo(jqElements["menu-block"]).addClass("item-main-menu info-app-item");
    itemsMenu.push({
        item: itemMenu,
        params: itemParam
    });
    var itemMenuContent;
    itemMenu.
        append(
            itemMenuContent = $('<div>').addClass('item-menu-content').append(
                forTranslate[itemParam['title']] = $('<span>')
            ).addClass(itemParam['label'])
        );
    if (!itemParam["application"]) {
        itemMenuContent.addClass("disabled").append($("<div>").addClass("closed-position"));
    } else {
        itemMenu.click(function() {
            menuApp.Event.callHandlers("onChange", itemParam);
        })
    }

    return itemMenu;
};

this._constructor = function() {
    var app = this;
    $.ajax({
        url: 'configs/MyInfoMenu.json',
        dataType: 'json',
        async: false,
        success: function(config) {
            for (var i = 0; i < config.length; i++) {
                createItemMenu(config[i]);
            }
            jqElements["menu-block"].append($("<li>").addClass("clear"));
            setLanguage();
            app.onInit();
        }
    });
};
