var options = false;

var jqElements = {
    'main-block':          $('<div>').addClass('page-holder'),
    'content-section':      $('<div>').addClass('content-section'),
    'application-wrapper':  $('<div>').addClass('application-wrapper'),
    'application-section':  $('<div>').addClass('application-section')
};

this.getJQElements = function(name) {
    return jqElements[name];
};
this.createApplicationContent = function() {
    jqElements['main-block'].prepend(
        jqElements['content-section'].append(
            jqElements['application-wrapper'].append(
                jqElements['application-section']
            )
        )
    )
};

var openedApplication = {};
this.getApplication = function() {
    return openedApplication["content"];
};
this.setApplication = function(appContent, appButton) {
    if (!appContent.is(openedApplication["content"])) {
        openedApplication["content"] ? openedApplication["content"].detach() : false;
        openedApplication["button"] ? openedApplication["button"].removeClass("active") : false;
        openedApplication["content"] = appContent;
        openedApplication["button"] = appButton;
    } else {
        jqElements['application-section'].append(appContent);
        return;
    }
    jqElements['application-section'].append(appContent);
    appButton.addClass('active');
};

this._constructor = function(opt) {
    options = opt;
    body.append(
        jqElements['main-block']
    );
    this.onInit();
};
