var jqElements = {
    'main-block': $('<div>').addClass('chat-application'),
    'chat-button': $('<div>').addClass('chat-button')
};

var countryName = $('<span>').addClass("bold").appendTo(jqElements["chat-button"]).text("Beginners");
var countUsers = $('<span>').addClass("count-users").appendTo(jqElements["chat-button"]).text(0);
var countUsersNumber = 0;

this.addUser = function() {
    countUsersNumber++;
    updateCOuntInBlock();
};
this.delUser = function() {
    countUsersNumber--;
    updateCOuntInBlock();
};
var updateCOuntInBlock = function() {
    countUsers.text(countUsersNumber);
    chatApp.Event.callHandlers("onChangeCount");
};


var chatApp = this;
var sentForm = $('<form>').addClass('sent-message-form').appendTo(jqElements["main-block"]).submit(function(e) {
    e.preventDefault();
    $(".smile", messageField).each(function() {
        var sm = $(this).attr("alt");
        $(this).replaceWith("[sm] " + sm + " [/sm]");
    });
    if (!textInput.text().length) return;
    var text = textInput.text().replace(/\[sm\]\s([ab0-9]+)\s\[\/sm]/g, "<sm> $1 </sm>");
    textInput.html("").focus();
    chatApp.Event.callHandlers("onSendMessage", text);
});
var messageField = $('<div>').
    addClass("textarea").
    appendTo(sentForm);

var textInput = $('<div>').
    appendTo(messageField).
    addClass("input-text-field").
    attr("contenteditable", true).
    attr("tabindex", 12).keydown(function(e) {
        if (e.which == 13) {
            sentForm.submit();
            return false;
        }
    });

var submitFormButton =  $('<div>').addClass("messenger-button submit-button").click(function() {
    sentForm.submit();
}).appendTo(sentForm).attr("tabindex", 13);

$("<div>").addClass("messenger-button-icon").appendTo(submitFormButton);
var addSmileButton =  $('<div>').addClass("messenger-button smile-button").focus(function(e) {
    e.preventDefault();
}).mousedown(function(e) {
    e.preventDefault();
    addSmileButton.focus();
}).prependTo(sentForm).attr("tabindex", 11);

$("<div>").addClass("messenger-button-icon").appendTo(addSmileButton);

var smilesBlock = $("<div>").addClass("smiles-block").appendTo(addSmileButton);
var pathSmiles = "/images/smiles/";
var smileFormat = ".png";
var directoriesSmiles = {
    a: {start: 1, end: 38},
    b: {start: 1, end: 29}
};

var createSmile = function(i, k) {
    var oneSmileBlock = $("<div>").addClass("one-smile");
    var imageSmile = $("<img>").attr("src", pathSmiles + "/" + i + k + smileFormat);
    oneSmileBlock.append(imageSmile).appendTo(smilesBlock);
    oneSmileBlock.mousedown(function(e) {
        e.preventDefault();
        textInput.append(imageSmile.clone().addClass("smile").attr("alt", i + k));
    });
};

for (var i in directoriesSmiles) {
    for (var k = directoriesSmiles[i]["start"]; k <= directoriesSmiles[i]["end"]; k++) {
        createSmile(i, k);
    }
}

var messagesListContainer = $('<div>').addClass("messages-list-container").prependTo(jqElements["main-block"]);
var messagesListBlock = $("<ul>").addClass("messages-list").appendTo(messagesListContainer);

this.getJQElements = function(name) {
    return jqElements[name];
};

var startScroll = 0;
var scrollMargin = 20;
var startHeight = 0;
var index = 0;

var messagesByUser = {};

var audio =  new Audio();
var sourceAudio = document.createElement("source");
sourceAudio.src = "/media/sounds/im.mp3";
sourceAudio.type = "audio/mpeg";
sourceAudio.codecs = "mp3";
audio.appendChild(sourceAudio);

this.updateUserOfMessages = function(userData) {
    var userId = "user-" + userData["id"];
    if (!messagesByUser[userId]) return;
    for (var k = 0; k < messagesByUser[userId].length; k++) {
        messagesByUser[userId][k].updateData(userData);
    }
};

var chatMessage = function(messageItem) {
    var userId = "user-" + messageItem["from"][1];
    messagesByUser[userId] = messagesByUser[userId] || [];
    messagesByUser[userId].push(this);
    this.updateData = function(userData) {
        if (!userData) return;
        user = userData;
        userNameContainer.text(userData["nick"]);
        var background = false;
        if (userData["view"]["photo"]) {
            background = _CONSTS_.UserPicPath.replace(/\{sz\}/, 4).replace(/\{userId\}/, userData["id"]) + '?_' + (new Date()).getTime();
        } else {
            var headId = userData["view"]["head"]["id"],
                RESOURCES = Functions.getSources();
            background = RESOURCES["heads"]["all"]["element-" + headId]["views"]["size-2"]["img"];
        }
        forLoadAvatar.src = background;
    };

    startScroll = messagesListContainer.scrollTop();
    startHeight = messagesListBlock.height();
    index++;

    var itemContainer =     $("<li>").addClass("message-block").appendTo(messagesListBlock);
    var itemBlock =         $("<div>").addClass("full-message-content").appendTo(itemContainer);
    var avatarContainer =   $("<div>").addClass("user-avatar").appendTo(itemBlock);
    var content =           $("<div>").addClass("content-message").appendTo(itemBlock);
    var titleContent =      $("<div>").addClass("title-content-message").appendTo(content);
    var userNameContainer = $('<span>').addClass("bold nick-name").appendTo(titleContent).click(function() {
            textInput.prepend(user["nick"] + ", ").focus();
        }),
        avatarImage = new Image(), forLoadAvatar = new Image(),
        user = messageItem["user"] || false;

    var visibleAvatar = false;

    forLoadAvatar.onload = function() {
        avatarImage.src = forLoadAvatar.src;
        !visibleAvatar ? avatarContainer.append(avatarImage) : false;
        visibleAvatar = true;
        Functions.setImageToBlock(avatarImage, avatarContainer, !(user && user["view"]["photo"]));
    };
    if (messageItem["self"]) itemContainer.addClass('self-message');
    itemContainer.addClass(!(index % 2) ? "even" : "not-even");
    var date = messageItem["xdata"] ? messageItem["xdata"]["date"] : new Date();

    var dateTimeH = date.getHours();
    var dateTimeI = date.getMinutes();

    dateTimeH = (dateTimeH < 10 ? '0' : '') + dateTimeH;
    dateTimeI = (dateTimeI < 10 ? '0' : '') + dateTimeI;
    titleContent.append(userNameContainer, " / " + dateTimeH + ":" + dateTimeI);
    var text = messageItem["message"]["text"];
    var newText = text.replace(regExp, "<span class='smile-message'><img src='" + pathSmiles + "$1" + smileFormat + "'/></span>");
    $('<div>').addClass("message-text").appendTo(content).html(newText);
    this.updateData(messageItem["user"]);
};


this.addMessage = function(messageItem) {
    new chatMessage(messageItem);
    audio.play();
    iniLastMessage();
};

var regExp = /<sm>\s(a(1?[0-9]|2[0-9]|3[0-8])|b(1?[0-9]|2[0-9]))\s<\/sm>/g;

var toBottomPosition = function() {
    messagesListContainer.scrollTop(messagesListBlock.height() - messagesListContainer.height());
};
var iniLastMessage = function() {
    if (startScroll < startHeight - messagesListContainer.height() - scrollMargin) return;
    messagesListBlock.stop();
    toBottomPosition();
};

win.resize(iniLastMessage);
this.onShow = function() {
    toBottomPosition();
};
this._constructor = function() {
    this.onInit();
};
