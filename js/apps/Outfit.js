var jqElements = {
    'main-block': $('<div>').addClass('outfit-application')
};

this.getJQElements = function(name) {
    return jqElements[name];
};

//--- Объект с блоками для перевода ---
var forTranslate = {};

//--- Добавление блока с текстом для перевода ---
var addToTranslate = function(label, element) {
    forTranslate[label] = (forTranslate[label] || []);
    forTranslate[label].push(element.text(TRANSLATES[label] || label));
    return element;
};

//--- Установка нового языка ---
this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k = 0; k < forTranslate[i].length; k++) {
            forTranslate[i][k].text(TRANSLATES[i] || i);
        }
    }
};


//==================== Управление показом прелоадера ====================

//--- Блок с прелоадером ---
var preLoaderContainer = $("<div>").addClass("absolute-container shop-loader").append(
    $("<div>").addClass("relative-container").append(
        $("<div>").addClass("pre-loader medium").append("<div>")
    )
);

//--- Показываем ---
var showShopPreLoader = function(f) {
    f = f || false;
    preLoaderContainer[f ? "appendTo" : "prependTo"](jqElements["main-block"])
};

//--- Скрываем ---
var hideShopPreLoader = function() {
    preLoaderContainer.detach();
};

//==================== / Управление показом прелоадера ====================

//--- Добавление в желания ---
var app = this;

var getSelectedList = function(unselect) {
    var selectedProducts = shopApplication.getSelectedProducts(),
        selectedParams = [],
        usedProducts = {};

    for (var k = selectedProducts.length - 1; k >= 0; k--) {
        var paramsProduct = selectedProducts[k].getParams(),
            price = selectedProducts[k].getCartItem().getSelectedPriceItem()["new"],
            used = false,
            id = paramsProduct["id"],
            t = paramsProduct["type"],
            tname = "type-" + t;

        switch (gender) {
            case "male":
                switch (t) {
                    case 6:
                    case 7:
                    case 8:
                        if (!(tname in usedProducts)) {
                            usedProducts["type-9"] = false;
                            usedProducts[tname] = used = true;
                        }
                        break;
                    case 9:
                        if (!(tname in usedProducts)) {
                            usedProducts["type-6"] = false;
                            usedProducts["type-7"] = false;
                            usedProducts["type-8"] = false;
                            usedProducts[tname] = used = true;
                        }
                        break;
                    default:
                        if (!usedProducts[tname]) {
                            usedProducts[tname] = used = true;
                        }
                        break;
                }
                break;
            case "female":
                switch (t) {
                    case 3:
                        if (!(tname in usedProducts)) {
                            usedProducts[tname] = used = true;
                            usedProducts["type-5"] = false;
                            usedProducts["type-6"] = false;
                        }
                        break;
                    case 5:
                        if (!(tname in usedProducts)) {
                            usedProducts[tname] = used = true;
                            usedProducts["type-3"] = false;
                            usedProducts["type-6"] = false;
                        }
                        break;
                    case 6:
                        if (!(tname in usedProducts)) {
                            usedProducts[tname] = used = true;
                            usedProducts["type-3"] =
                            usedProducts["type-5"] =
                            usedProducts["type-7"] =
                            usedProducts["type-8"] = false;
                        }
                        break;
                    case 7:
                        if (!(tname in usedProducts)) {
                            usedProducts[tname] = used = true;
                            usedProducts["type-8"] = false;
                            usedProducts["type-6"] = false;
                        }
                        break;
                    case 8:
                        if (!(tname in usedProducts)) {
                            usedProducts[tname] = used = true;
                            usedProducts["type-7"] = false;
                            usedProducts["type-6"] = false;
                        }
                        break;
                    default:
                        if (!usedProducts[tname]) {
                            usedProducts[tname] = used = true;
                        }
                        break;
                }
                break;
        }
        unselect = unselect || false;
        unselect ? selectedProducts[k].unSelectItem() : false;
        var itemParams = false;
        if (unselect) {
            itemParams = {
                id: id,
                count: price['count'] * 1440,
                use: used
            };
        } else if (used) {
            itemParams = {
                name: "clothes-" + t,
                id: id
            };
        }
        itemParams ? selectedParams.push(itemParams) : false;
    }
    return selectedParams;
};
var showedContent = false;


var applyProducts = function(params) {
    var data = {};
    for (var i in params) {
        if (params[i]) {
            data[i] = params[i];
        }
    }
    applyModel();
    params.models = selectedModel.getParams()["id"];
};



var applyMyProductsGlob = function() {
    var newData = {}, setupData = [];
    for (var k in usedClothes) {
        if (usedClothes[k]) {
            var params = usedClothes[k].getParams();
            newData[k] = params["product"]["id"];
            setupData.push({
                id: params["id"],
                count: 1
            });
        }
    }
    applyProducts(newData);
    Functions.sendToAPI("api/user/dress", function(data) {}, "POST", {
        items: setupData
    });
};

var buyProducts = function() {
    if (userBalance < amountCount / 100) {
        return;
    }
    var forUpdateModel = getSelectedList(), selectedParams = getSelectedList(true), data = {};
    for (var k = 0; k < forUpdateModel.length; k++) {
        data[forUpdateModel[k]["name"]] = forUpdateModel[k]["id"];
    }
    applyProducts(data);
    showShopPreLoader();
    deActivateContent();
    showedContent = false;
    Functions.sendToAPI("api/user/rent?u=" + User["id"], function() {
        app.Event.callHandlers("onChangeBalance");
        loadMyProducts(function() {
            showMyProducts(true);
            hideShopPreLoader();
        });
    }, "POST", {items: selectedParams});
};

var addToDesires = function() {
    var products = shopApplication.getSelectedProducts(),
        desItems = [];

    for (var k = 0; k < products.length; k++) {
        var itemParams = products[k]["getParams"]();
        desItems.push({
            id: itemParams["id"],
            count: 1
        });
    }
    addingIndicator.text("+" + desItems.length).hide().appendTo(desButton).show("fast");
    Functions.sendToAPI("api/user/wishes", function() {
        app.Event.callHandlers("onAddToDesires");
        setTimeout(function() {
            addingIndicator.hide("fast", function() {
                addingIndicator.detach();
            })
        }, 2000);
    }, "POST", desItems)
};

var applyModel = function() {
    if (selectedModel == activedItem) return;
    var model = (selectedModel = activedItem).getParams()["id"];
    app.Event.callHandlers("onChangeModel", model);
    Functions.sendToAPI("api/user/model", function() {}, "POST", {
        id: model,
        count: 1
    });
};

//================================== Правй блок с кнопками ==================================

//--- Линии для кнопок в правом баре ---
var linesButton = {
    "line-1": $("<div>").addClass("buttons-line"),
    "line-2": $("<div>").addClass("buttons-line"),
    "line-3": $("<div>").addClass("buttons-line"),
    "line-4": $("<div>").addClass("buttons-line"),
    "line-5": $("<div>").addClass("buttons-line")
};


//--- Кнопки правого бара магазина ---

var resetMyProductsButton = $("<div>").addClass("nav-shops-button").click(function() {
    for (var i in usedClothes) {
        usedClothes[i] ? usedClothes[i].setParams("inUse", false) : false;
    }
    modelPersonage.setData(resetPersModelData());
});

addToTranslate("remove_all", resetMyProductsButton);

// Перейти к моим продуктам
var myProductsButton = $("<div>").addClass("nav-shops-button float-left").append(
        $("<span>").addClass("clothes-icon")
    ).click(function() {
        showMyProducts();
    }).appendTo(linesButton["line-1"]);

// Перейти с выбору манекена
var modelsButton = $("<div>").addClass("nav-shops-button").append(
        $("<span>").addClass("model-icon")
    ).click(function() {
        showModelsList();
    }).appendTo(linesButton["line-3"]);

// Перейти в магазин
var shopButton = $("<div>").addClass("nav-shops-button float-right").append(
        $("<span>").addClass("shop-icon")
    ).click(function() {
        if (!shopChapterOpened || shopChapterOpened == "showcase") {
            showShowCase();
        } else {
            showCart();
        }
    }).appendTo(linesButton["line-1"]);


var okButton = $("<div>").addClass("nav-shops-button").append(
        $("<span>").addClass("ok-icon")
    ).click(function() {
        if (showedContent == "myproducts") {
            applyMyProductsGlob();
        } else if (showedContent == "models") {
            applyModel();
        }
    }).appendTo(linesButton["line-4"]);


// Применить выбранное
var categoriesButton = $("<div>").addClass("nav-shops-button categories-button").append(
        $("<span>").addClass("categories-icon")
    ).click(function() {
        showCategoriesMenu();
    }).appendTo(linesButton["line-2"]);

var shopNavigation = $("<div>").addClass('nav-shops').append(linesButton["line-1"], linesButton["line-3"]);
shopNavigation.appendTo(jqElements["main-block"]);
//================================= / Правй блок с кнопками =================================

//==================================== Верхний зелёный бар ==================================

//--- Верхний бар магазина ---
var topNavigation = $("<div>").addClass("shop-top-panel");

//--- Блок для баланса пользователя ---
var myBalance = $("<span>").text(0);

//--- Блок для стоимости выбранных товаров ---
var fullAmount = $("<span>").text(0);

//--- Кнопка поиска ---
var searchButton = $("<div>").addClass("nav-shops-button").append(
    $("<span>").addClass("search-icon")
).click(function() {
    searchButton.after(searchField).addClass("active");
    searchField.focus();
});
var openedSearch = false;

//--- Поле для поиска товара ---
var searchField = $("<input>").attr("type", "text").bind("keypress", function() {
    openedSearch = searchField.val().length;
}).blur(function() {
    if (!searchField.val().length) {
        searchField.detach();
        searchButton.removeClass("active");
    }
});

//--- Кнопка "перейти в корзину" / "купить" ---
var buyButton = $("<div>").addClass("nav-shops-button").append(
        $("<span>").addClass("buy-icon")
    ).click(function() {
        if (showedContent == "showcase") {
            showCart();
        } else {
            buyProducts();
        }
    }
);

var backButton = $("<div>").addClass("back-button shop-navigation").click(function() {
    showShowCase();
});

var addingIndicator = $("<div>").addClass("adding-indicator");
//--- Кнопка "Добавить в желания" ---
var desButton = $("<div>").addClass("nav-shops-button top-index").append(
        $("<span>").addClass("desires-icon")
    ).click(function() {
        addToDesires();
    });

var shopNav = $("<div>");


topNavigation.append(
    $("<span>").addClass("my-money").append(myBalance, $("<span>").addClass("money-icon")),
    shopNav.append(
        $("<span>").addClass("amount-money").append(fullAmount, $("<span>").addClass("money-icon"))
    )
).appendTo(jqElements["main-block"]);

var userBalance = 0;
this.setUserBalance = function(balance) {
    myBalance.html(userBalance = balance);
};


//=================================== / Верхний зелёный бар =================================

//=================================== Меню выбора категории ===================================

var categoriesMenu = $("<div>").addClass("categories-list").click(function() {
    showShowCase();
});

var categoriesList = $("<ul>").addClass("categories-menu clothes-menu").appendTo(categoriesMenu);


var fullviewContainer = $("<div>").addClass("relative-container fullview-container");
var sliderList = $("<ul>").addClass("slider-list");

var showNewSlide = function(rangeIndex) {
    var oldSlide = activeSlide;
    var oldIndex = activeSlide.getIndex();
    var newIndex = oldIndex + rangeIndex;
    var allProducts = activeSlide.getShowcase();
    var maxIndex = allProducts.length - 1;
    newIndex = newIndex > maxIndex ? 0 : newIndex < 0 ? maxIndex : newIndex;
    var newSlide = activeSlide = allProducts[newIndex];
    var oldSlideBlock = oldSlide.getItemBlock();
    var newSlideBlock = newSlide.getItemBlock();
    if ((oldIndex < newIndex && !(newIndex == maxIndex && oldIndex == 0)) || (oldIndex == maxIndex && newIndex == 0)) {
        sliderList.append(newSlideBlock);
        oldSlideBlock.animate({
            "margin-left": "-100%"
        }, function() {
            oldSlide.detachPreview();
            oldSlideBlock.css({"margin-left": "0%"});
        });
        newSlide.iniHeightPreview();
    } else {
        sliderList.prepend(newSlideBlock);
        newSlideBlock.css({
            "margin-left": "-100%"
        }).animate({
                "margin-left": "0%"
            }, function() {
                oldSlide.detachPreview();
            });
        newSlide.iniHeightPreview();
    }
};

fullviewContainer.append(
    sliderList,
    $("<div>").addClass("absolute-container").click(function() {
        showShowCase();
    }),
    $("<div>").addClass("changer-button next-element").click(function() {
        showNewSlide(1);
    }),
    $("<div>").addClass("changer-button prev-element").click(function() {
        showNewSlide(-1);
    })
);

var previewItems = {};
var activeSlide = false;

var itemFull = function(item, nameCat) {
    var itemBlock = item.getProductContainer();
    var itemParams = item.getParams();
    var index = previewItems[nameCat].length;
    var previewSlideImage = new Image();
    var previewItem = $("<li>");
    var created = false;
    previewItems[nameCat].push(this);
    this.getShowcase = function() {
        return previewItems[nameCat];
    };
    this.select = function() {
        previewCheckbox.get(0).checked = true;
    };
    this.unselect = function() {
        previewCheckbox.get(0).checked = false;
    };
    var previewCheckbox = $("<input>").
        attr("type", "checkbox").
        change(function() {
            previewCheckbox.is(":checked") ?
                item.selectItem() :
                item.unSelectItem();
        });
    this.detachPreview = function() {
        previewItem.detach();
        win.unbind("resize", iniHeightPreview);
    };
    var createItem = function() {
        created = true;
        var loader = $("<div>").
            addClass("pre-loader medium").
            append("<div>").
            appendTo(previewItem);
        $("<label>").
            addClass("fullview-checkbox").
            append(
                previewCheckbox,
                $("<span>"),
                $("<span>").text(itemParams["name"])
            ).appendTo(previewItem);

        previewSlideImage['onload'] = function() {
            loader.detach();
            previewItem.append(previewSlideImage);
            iniHeightPreview();
        };
        previewSlideImage.src = itemParams["views"]["fv"];
    };
    var iniHeightPreview = this.iniHeightPreview = function() {
        $(previewSlideImage).css({
            marginTop: -$(previewSlideImage).height() / 2
        });
    };
    this.getItemBlock = function() {
        if (!created) {
            createItem();
        }
        win.bind("resize", iniHeightPreview);
        return previewItem;
    };
    this.getIndex = function() {
        return index;
    };
    var itemObject = this;
    itemBlock.click(function() {
        activeSlide = itemObject;
        showFullPreview();
    });
};

var showCases = {};
var createItemCategoriesMenu = function(name, params) {
    $("<li>").
        addClass("categories-item " + params["label"]).
        append(
            $("<div>").append(
                addToTranslate(params["title"], $("<span>"))
            )
        ).appendTo(categoriesList).click(function() {
            if (!showCases[name]) {
                showCases[name] = shopApplication.getShowCase(name);
                showCases[name].getContainer().addClass("clothes-" + params["type"] + "-" + gender);
                var products = showCases[name].getProducts();
                for (var k = 0; k < products.length; k++) {
                    products[k].setData("fvItem", new itemFull(products[k], name));
                }
            }
            showedShowcase =  showCases[name];
        });
    previewItems[name] = [];
};

//=================================== / Меню выбора категории ===================================
var modelBlock = $("<div>").addClass("model-preview");
var modelPersonage = false;

this.setPersonage = function(personage) {
    modelBlock.append((modelPersonage = personage)["getContainer"]());
};

var shopApplication = false,
    amountCount = 0,
    oldAmount;

var setAmount = function(range) {
    oldAmount = amountCount;
    amountCount+= range;
    fullAmount.html(amountCount / 100);

    if (oldAmount && !amountCount) {
        buyButton.detach();
        desButton.detach();
    } else if (!oldAmount && amountCount) {
        searchButton.before(buyButton, desButton);
    }
};



var selectedProducts = {};
var modelList = $("<div>").addClass("models-list showcase-list");

var activedItem = false;
var selectedModel = false;
var createModelItem = function(params) {
    var loader = $("<div>").addClass("pre-loader small animated").append("<div>");
    var img = new Image();
    img.onload = function() {
        loader.empty().remove();
        item.append(img);
    };
    img.src = params["views"]["size-2"]["img"];
    this.getItem = function() {
        return item;
    };
    this.getParams = function() {
        return params;
    };
    var modelItem = this;
    var item = $("<div>").addClass("personage-block size_2 model-item-preview").click(function() {
        activedItem.getItem().removeClass("active");
        item.addClass("active");
        activedItem = modelItem;
        modelPersonage.setData({
            models: params["id"]
        });
    }).append(loader);

    if (modelPersonage.getData()["models"] == params["id"]) {
        activedItem = modelItem;
        selectedModel = modelItem;
        item.addClass("active");
    }
    modelList.append(item);
};

var createModelsList = function() {
    var models = Functions.getSources()["models"][gender]["byInd"];
    for (var i = 0; i < models.length; i++) {
        new createModelItem(models[i]);
    }
};


//--- Функция установки магазина ---
this.setShopApplication = function(shop) {
    shopApplication = shop;
    shopProducts = shop.getProducts()["byId"];
    shopApplication.setHandlers({
        addSelection: function(item) {
            var params = item.getParams();
            selectedProducts["type-" + params["type"]] = selectedProducts["type-" + params["type"]] || [];
            selectedProducts["type-" + params["type"]].push(params["id"]);
            var dataPers = {};
            dataPers["clothes-" + params["type"]] = params["id"];
            modelPersonage.setData(dataPers);
            setAmount(item.getCartItem().getSelectedPriceItem()["new"]["price"]);
        },
        delSelection: function(item) {
            var params = item.getParams(),
                productsType = selectedProducts["type-" + params["type"]],
                newArray = [];
            var dataPers = {};
            for (var i = 0; i < productsType.length; i++) {
                if (params["id"] != productsType[i]) {
                    newArray.push(productsType[i]);
                }
            }
            selectedProducts["type-" + params["type"]] = productsType = newArray;
            if (productsType.length) {
                dataPers["clothes-" + params["type"]] = productsType[productsType.length - 1];
            } else {
                dataPers["clothes-" + params["type"]] = false;
            }
            modelPersonage.setData(dataPers);
            setAmount(-item.getCartItem().getSelectedPriceItem()["new"]["price"]);
            if (!shopApplication.getSelectedProducts().length && showedContent == "cart") {
                showShowCase();
            }
        },
        changeSelection: function(item) {
            var priceItem = item.getCartItem().getSelectedPriceItem();
            setAmount(priceItem["new"]["price"] - priceItem["old"]["price"]);
        }
    });
};

var showedBlock = false,
    shopChapterOpened = false,
    showedShowcase = false;

//--- Снимаем класс с активной кнопочки и убирем контент ---
var deActivateContent = function() {
    showedBlock ? showedBlock.detach() : false;
    switch (showedContent) {
        case "cart":
            shopButton.removeClass("active");
            backButton.detach();
            break;
        case "showcase":
            linesButton["line-2"].detach();
            linesButton["line-3"].detach();
            shopButton.removeClass("active");
            modelBlock.detach();
            searchField.detach();
            searchButton.detach();
            break;
        case "myproducts":
            myProductsButton.removeClass("active");
            shopNavigation.after(topNavigation);
            linesButton["line-4"].detach();
            linesButton["line-3"].detach();
            linesButton["line-5"].detach();
            modelBlock.detach();
            break;
        case "models":
            modelsButton.removeClass("active");
            shopNavigation.after(topNavigation);
            linesButton["line-4"].detach();
            linesButton["line-3"].detach();
            modelBlock.detach();
            break;
        case "preview":
            activeSlide.detachPreview();
            topNavigation.before(shopNavigation);
            break;
    }
};

var showFullPreview = function() {
    deActivateContent();
    showedContent = "preview";
    shopNavigation.detach();
    jqElements["main-block"].append(showedBlock = fullviewContainer);
    sliderList.append(activeSlide.getItemBlock());
    activeSlide.iniHeightPreview();
};

var showShowCase = function() {
    if (!showedShowcase) {
        if (showedContent != "categories") {
            showCategoriesMenu();
        }
        return;
    }
    if (showedContent == "showcase") return;
    if (showedContent == "categories") {
        jqElements["main-block"].prepend(shopNavigation, topNavigation);
    }
    deActivateContent();
    var selectedProducts = shopApplication.getSelectedProducts(),
        newData = resetPersModelData();
    for (var k = selectedProducts.length - 1; k >= 0; k--) {
        var params = selectedProducts[k].getParams(),
            name = "clothes-" + params["type"];
        newData[name] = newData[name] || params["id"];
    }
    modelPersonage.setData(newData);
    searchButton.appendTo(shopNav);
    if (openedSearch) {
        searchButton.after(searchField);
    }
    jqElements["main-block"].append(modelBlock);
    linesButton["line-1"].after(linesButton["line-3"], linesButton["line-2"]);
    shopButton.addClass("active");
    shopChapterOpened = showedContent = "showcase";
    jqElements["main-block"].append(showedBlock = showedShowcase.getContainer());
    showedShowcase.restoreScroll();
    showedShowcase.iniProductsImages();
};


var showCart = function() {
    if (showedContent == "cart") return;
    var Cart = shopApplication.getCart().getBlockCart();
    deActivateContent();
    shopButton.addClass("active");
    jqElements["main-block"].append(showedBlock = Cart, backButton);
    shopChapterOpened = showedContent = "cart";
};


var showMyProducts = function(fl) {
    fl = fl || false;
    if (showedContent == "myproducts") return;
    deActivateContent();

    var createMyProductsContent = function() {
        applyMyProducts();
        linesButton["line-1"].after(linesButton["line-3"], linesButton["line-4"], linesButton["line-5"]);
        topNavigation.detach();
        myProductsButton.addClass("active");
        jqElements["main-block"].append(modelBlock, showedBlock = myProductsList);
    };

    if (!fl) {
        showShopPreLoader();
        modelBlock.detach();
        myProductsList.detach();
        loadMyProducts(function() {
            jqElements["main-block"].append(modelBlock, myProductsList);
            hideShopPreLoader();
            createMyProductsContent();
        });
    } else {
        createMyProductsContent();
    }

    for (var i in usedClothes) {
        if (usedClothes[i]) {
            break;
        }
        if (!usedClothes[i]) {
            okButton.detach();
            resetMyProductsButton.detach();
        }
    }
    showedContent = "myproducts";
};


var showCategoriesMenu = function() {
    deActivateContent();
    topNavigation.detach();
    shopNavigation.detach();
    showedContent = "categories";
    jqElements["main-block"].prepend(showedBlock = categoriesMenu);
};

var showModelsList = function() {
    if (showedContent == "models") return;
    deActivateContent();
    okButton.appendTo(linesButton["line-4"]);
    linesButton["line-1"].after(linesButton["line-3"]);
    jqElements["main-block"].append(modelBlock);
    topNavigation.detach();
    linesButton["line-4"].appendTo(shopNavigation);
    showedContent = "models";
    modelsButton.addClass("active");
    jqElements["main-block"].append(showedBlock = modelList);
};

//--- Мои продукты ---
var shopProducts = false,
    myProducts = {},
    myActiveProducts = [],
    myProductsList = $("<div>").addClass("my-products-content clothes-7-male"),
    usedClothes = {};

var resetPersModelData = function() {
    var persData = modelPersonage.getData(), newData = {};
    for (var i in persData) {
        if (persData[i] && i != "models") {
            newData[i] = false;
        }
    }
    return newData;
};
var applyMyProducts = function() {
    var newData = resetPersModelData();
    for (var k in usedClothes) {
        if (!newData[k] && usedClothes[k]) {
            newData[k] = usedClothes[k].getParams()["product"]["id"];
        }
    }
    modelPersonage.setData(newData);
};


var MyProductItem = function(params) {
    var product = shopProducts["element-" + params["product"]["id"]];
    var nameproduct = "clothes-" + product["type"];
    var itemProduct = this;

    //--- Создаём блок продукта ---
    var itemBlock = $("<label>").addClass("product-element clothes-element").mousedown(function() {
            isChecked = radioButton.is(":checked");
        }).click(function(e) {
            if (isChecked && radioButton.is(e.target)) {
                itemProduct.setParams("inUse", false);
                applyMyProducts();
            }
        }),
        productContainer = $("<div>").addClass("product-element-container").appendTo(itemBlock),
        loader = $("<div>").
            addClass("pre-loader small animated").
            append("<div>").
            appendTo(itemBlock),
        productImg = new Image(),
        radioButton = $("<input>").attr({
            type: "radio",
            name: "my-clothes-" + product["type"]
        }).change(function() {
            if (!isChecked) {
                setChecked();
                applyMyProducts();
            }
        });

    var isChecked = false;
    productImg["onload"] = function() {
        loader.empty().remove();
        productContainer.append(
            productImg,
            $("<span>").text(product["name"]).addClass("name-product"),
            $("<div>").addClass("label-checkbox").append(
                radioButton, $("<span>")
            )
        );
    };

    productImg.src = product["views"]["size-3"]["img"];



    this.setParams = function(a, b) {
        params[a] = b;
        switch (a) {
            case "inUse":
                if (b) {
                    setChecked();
                } else {
                    if (itemProduct == usedClothes[nameproduct])
                        usedClothes[nameproduct] = false;
                    for (var i in usedClothes) {
                        if (usedClothes[i]) {
                            break;
                        }
                    }
                    if (!usedClothes[i]) {
                        okButton.detach();
                        resetMyProductsButton.detach();
                    }
                }
                radioButton.get(0).checked = !(!b);
                break;
        }
    };


    var setChecked = function() {
        usedClothes[nameproduct] ? usedClothes[nameproduct].setParams("inUse", false) : false;
        usedClothes[nameproduct] = itemProduct;

        //--- Деактивируем конфликтующие элементы ---
        switch (gender) {
            case "male":
                switch(product["type"]) {
                    case 6:
                    case 7:
                    case 8:
                        usedClothes["clothes-9"] ? usedClothes["clothes-9"].setParams("inUse", false) : false;
                        break;
                    case 9:
                        usedClothes["clothes-6"] ? usedClothes["clothes-6"].setParams("inUse", false) : false;
                        usedClothes["clothes-7"] ? usedClothes["clothes-7"].setParams("inUse", false) : false;
                        usedClothes["clothes-8"] ? usedClothes["clothes-8"].setParams("inUse", false) : false;
                        break;
                }
                break;
            case "female":
                switch(nameproduct) {
                    case 3:
                        usedClothes["clothes-5"] ? usedClothes["clothes-5"].setParams("inUse", false) : false;
                        usedClothes["clothes-6"] ? usedClothes["clothes-6"].setParams("inUse", false) : false;
                        break;
                    case 5:
                        usedClothes["clothes-6"] ? usedClothes["clothes-6"].setParams("inUse", false) : false;
                        usedClothes["clothes-3"] ? usedClothes["clothes-3"].setParams("inUse", false) : false;
                        break;
                    case 6:
                        usedClothes["clothes-3"] ? usedClothes["clothes-3"].setParams("inUse", false) : false;
                        usedClothes["clothes-5"] ? usedClothes["clothes-5"].setParams("inUse", false) : false;
                        usedClothes["clothes-7"] ? usedClothes["clothes-7"].setParams("inUse", false) : false;
                        usedClothes["clothes-8"] ? usedClothes["clothes-8"].setParams("inUse", false) : false;
                        break;
                    case 7:
                        usedClothes["clothes-6"] ? usedClothes["clothes-6"].setParams("inUse", false) : false;
                        usedClothes["clothes-8"] ? usedClothes["clothes-8"].setParams("inUse", false) : false;
                        break;
                    case 8:
                        usedClothes["clothes-6"] ? usedClothes["clothes-6"].setParams("inUse", false) : false;
                        usedClothes["clothes-7"] ? usedClothes["clothes-7"].setParams("inUse", false) : false;
                        break;
                }
                break;
        }
        okButton.appendTo(linesButton["line-4"]);
        resetMyProductsButton.appendTo(linesButton["line-5"]);
    };

    if (params["inUse"]) {
        radioButton.get(0).checked = true;
        setChecked();
    }

    this.getParams = function() {
        return params;
    };
    this.getItemBlock = function() {
        return itemBlock;
    };
};

//=== Загрузка моих товаров ===
var loadMyProducts = function(cb) {
    cb = cb || false;
    Functions.sendToAPI("api/user/p/clothes", function(data) {
        for (var i in myProducts) {
            myProducts[i].getItemBlock().detach();
        }
        myActiveProducts = data;
        okButton.detach();
        for (var k = 0; k < data.length; k++) {
            var productId = "product-" + data[k].product.id;
            if (!myProducts[productId]) {
                myProducts[productId] = new MyProductItem(data[k]);
            } else {
                myProducts[productId].setParams("id", data[k]["id"]);
                myProducts[productId].setParams("inUse", data[k]["inUse"]);
            }
            myProductsList.append(myProducts[productId].getItemBlock());
        }
        cb ? cb(data) : false;
    });
};


//=== Загрузка категорий товаров ===

//=== Загрузка категорий товаров ===
var Categories = Functions.getSources()["categories"]["clothes"];

var createCatMenu =  function() {
    for (var i in Categories[gender]) {
        createItemCategoriesMenu(i, Categories[gender][i]);
    }
};

//=== Обработчик отображения приложения ===
this.onShow = function() {
    switch (showedContent) {
        case "showcase":
            showedShowcase.restoreScroll();
            showedShowcase.iniProductsImages();
            break;
        case "myproducts":
            deActivateContent();
            showedContent = false;
            showMyProducts();
            break;
    }
};

var User = false, gender;
this._constructor = function(params) {
    User = params["user"];
    gender = User["male"] ? "male" : "female";
    createCatMenu();
    showShopPreLoader();
    loadMyProducts(function() {
        createModelsList();
        if (!myActiveProducts.length) {
            showShowCase();
        } else {
            showMyProducts(true);
        }
        hideShopPreLoader();
    });
    this.onInit();
};
