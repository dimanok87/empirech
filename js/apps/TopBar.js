var jqElements = {
    'main-block': $('<div>').addClass('top-bar')
};

this.getJQElements = function(name) {
    return jqElements[name];
};

var elementsNoApps = [];
var appsInBar = {};

var appsBar = $('<div>').appendTo(jqElements["main-block"]).addClass("apps-list");
var appsList = $('<ul>').appendTo(appsBar);
var counterLength = $('<div>').appendTo(jqElements["main-block"]);

var forTranslate = {};
var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        if (forTranslate[i])
            forTranslate[i].text(TRANSLATES[i] || i);
    }
    iniAppsBar();
    animateAppsList();
};

var detachElement = this.detachElement = function(name) {
    appsInBar[name]["button"].detach();
    appsInBar[name]["active"] = false;
    animateAppsList();
};

this.addElement = function(element, name, handlers, title) {
    element = element || false;
    if (element) {
        if (name) {
            jqElements['main-block']["append"](element.addClass('item-bar ' + name + '-item'));
        } else {
            jqElements['main-block']["append"](element);
        }
        elementsNoApps.push(element);
        iniAppsBar();
        iniListButtons();
    } else {
        var n = handlers["appName"];
        if (!appsInBar[n]) {
            appsInBar[n] = {
                button: $("<li>").addClass("app-button app-button-" + name).click(function() {
                        handlers.onselect ? handlers.onselect($(this)) : false;
                    }),
                active: true
            };

            var closeButton = $("<div>").addClass("close-app-button close-button").click(function() {
                detachElement(n);
                handlers.onclose ? handlers.onclose() : false;
                return false;
            });

            var titleBlock = forTranslate[title] = $("<span>").text(TRANSLATES[title] || title);
            appsInBar[n]["button"].append(closeButton, titleBlock).appendTo(appsList);
            iniListButtons();
        } else {
            if (!appsInBar[n]["active"]) {
                appsInBar[n]["button"].appendTo(appsList);
                appsInBar[n]["active"] = true;
            }
        }

        var listBlockLeft = appsBar.offset()["left"],
            listWidth = appsBar.width(),
            buttonLeft = appsInBar[n]["button"].offset()["left"],
            buttonWidth = appsInBar[n]["button"].outerWidth(true),
            oldLeft = marginLeft;
        if (buttonLeft - listBlockLeft < 0) {
            marginLeft-= listBlockLeft - buttonLeft + prevButton.width();
        } else if (buttonLeft - listBlockLeft > listWidth - buttonWidth) {
            marginLeft+= buttonLeft - listBlockLeft - listWidth + buttonWidth + nextButton.width();
        }

        if (marginLeft != oldLeft) {
            animateAppsList();
        }

        return appsInBar[n]["button"];
    }
};

var stepListing = 140;
var marginLeft = 0;

var prevButton = $('<div>').addClass("list-apps-button back-list").click(function() {
    marginLeft-= stepListing;
    animateAppsList();
});
var nextButton = $('<div>').addClass("list-apps-button go-list").click(function() {
    marginLeft+= stepListing;
    animateAppsList();
});

var animateAppsList = function() {
    iniListButtons();
    appsList.stop().animate({
        left: -marginLeft
    }, "linear");
};

var iniListButtons = function() {
    var maxMargin = appsList.innerWidth() - appsBar.width();
    marginLeft = marginLeft < 0 ? 0 : (marginLeft > maxMargin ? maxMargin : marginLeft);
    if (maxMargin > 0) {
        if (!marginLeft) {
            nextButton.prependTo(appsBar);
            prevButton.detach();
        } else {
            if (maxMargin == marginLeft) {
                nextButton.detach();
            } else {
                nextButton.prependTo(appsBar);
            }
            prevButton.prependTo(appsBar);
        }
    } else {
        marginLeft = 0;
        appsList.css({left: 0});
        nextButton.detach();
        prevButton.detach();
    }
};

var iniAppsBar = this.iniAppsBar = function() {
    var noWidth = 0;
    for (var k = 0; k < elementsNoApps.length; k++) {
        noWidth+= elementsNoApps[k].outerWidth(true);
    }
    var widthForBar = counterLength.width() - noWidth - 1;
    appsBar.width(widthForBar);
    appsBar.appendTo(jqElements["main-block"]);
};

this._constructor = function() {
    win.resize(iniAppsBar);
    this.onInit();
};
