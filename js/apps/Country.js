var jqElements = {
    'main-block': $('<div>').addClass('country-application').text("Country")
};

this.getJQElements = function(name) {
    return jqElements[name];
};

this._constructor = function() {
    this.onInit();
};
