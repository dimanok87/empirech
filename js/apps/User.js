var userData = {};

this.setUser = function(params) {
    userData["user"] = params;
    userData["data"] = {};
};

this.setData = function(name, value) {
    userData["data"][name] = value;
};

this.getData = function(name) {
    return userData["data"][name];
};

this.getUser = function() {
    return userData["user"];
};

this._constructor = function() {
    this.onInit();
};
