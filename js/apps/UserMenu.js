var jqElements = {
    'main-block': $('<div>').addClass('user-menu')
};
var menu = $("<ul>").addClass("big-menu").appendTo(jqElements['main-block']);

this.getJQElements = function(name) {
    return jqElements[name];
};

var forTranslate = {};
var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        forTranslate[i].text(TRANSLATES[i] || i);
    }
};

var userMenu = this;

var createItemMenu = function(itemParam) {
    var itemMenu = $('<li>').appendTo(menu);
    forTranslate[itemParam['title']] = $("<span>").appendTo(itemMenu.addClass("item-start-menu"));
    itemParam.item = itemMenu;
    itemMenu.addClass(itemParam['label']).click(function() {
        itemParam.selected = !itemParam.selected;
        userMenu["Event"]["callHandlers"]("onSelectItem", itemParam);
    });
};

//--- Подгружаем конфиги стартового меню, отображаем его, выполняем Handlers-функции ---
this._constructor = function() {
    var app = this;
    $.ajax({
        url: 'configs/UserMenu.json',
        dataType: 'json',
        async: false,
        success: function(config) {
            for (var i = 0; i < config.length; i++) {
                createItemMenu(config[i]);
            }
            menu.append($("<div>").addClass("clear"));
            setLanguage();
            app.onInit();
        }
    });
};
