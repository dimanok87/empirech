var jqElements = {
    'main-block': $('<div>').addClass('user-search')
};

this.getJQElements = function(name) {
    return jqElements[name];
};
var CONSTS = {
    "classes": {
        "close-button": "back-button",
        "submit-button": "submit-button"
    },
    "placeholders": {
        "login-input": "nick",
        "age": "age",
        "from": "from",
        "to": "to",
        "country": "country",
        "city": "city",
        "online": "online"
    },
    "names-fields": {
    }
};

var sendSearchForm = function(e) {
    e.preventDefault();
};

var forTranslate =  {
    "placeholder": {},
    "text": {}
};
var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k in forTranslate[i]) {
            switch(i) {
                case "placeholder":
                    forTranslate[i][k].attr("placeholder", TRANSLATES[k] || k);
                    break;
                case "text":
                    forTranslate[i][k].text(TRANSLATES[k] || k);
                    break;
            }
        }
    }
};

this._constructor = function() {
    var form = $('<form>').appendTo(jqElements['main-block']);

    //--- Поле ввода Никнейма ---
    $('<div>').addClass("search-field-holder").appendTo(form).append(
        $('<label>').append(
            forTranslate["text"][CONSTS["placeholders"]["login-input"]] =
                $("<span>").addClass("label"),

            forTranslate["placeholder"][CONSTS["placeholders"]["login-input"]] =
                $('<input>').attr({type: 'text'})
        )
    );

    //--- Поле выбора пола ---
    $('<div>').addClass("search-field-holder").appendTo(form).append(
        $("<label>").addClass("gender-select").append(
            $("<input>").attr({"type": "checkbox"}),
            $("<span>"),
            $("<span>").addClass("gender-icon male")
        ),
        $("<label>").addClass("gender-select").append(
            $("<input>").attr({"type": "checkbox"}),
            $("<span>"),
            $("<span>").addClass("gender-icon female")
        )
    );

    //--- Поле ввода интервала возраста ---
    $('<div>').addClass("search-field-holder age-holder").appendTo(form).append(

        forTranslate["text"][CONSTS["placeholders"]["age"]] =
            $('<label>').text("Age").addClass("label"),

        forTranslate["placeholder"][CONSTS["placeholders"]["from"]] =
            $('<input>').attr({type: 'text'}),

        forTranslate["placeholder"][CONSTS["placeholders"]["to"]] =
            $('<input>').attr({type: 'text'})
    );

    //--- Поле ввода страны ---
    $('<div>').addClass("search-field-holder").appendTo(form).append(
        $('<label>').append(
            forTranslate["text"][CONSTS["placeholders"]["country"]] =
                $("<span>").addClass("label"),

            $('<input>').attr({type: 'text'})
        )
    );

    //--- Поле ввода города ---
    $('<div>').addClass("search-field-holder").appendTo(form).append(
        $('<label>').append(
            forTranslate["text"][CONSTS["placeholders"]["city"]] =
                $("<span>").addClass("label"),
            $('<input>').attr({type: 'text'})
        )
    );

    //--- Чекбокс Online ---
    $('<div>').addClass("search-field-holder").appendTo(form).append(
        $('<label>').append(
            forTranslate["text"][CONSTS["placeholders"]["online"]] =
                $("<span>").addClass("label"),
            $('<input>').attr({type: 'checkbox'}),
            $('<span>')
        )
    );

    //--- Кнопка "отправить форму" ---
    $('<div>').
        addClass(CONSTS.classes["submit-button"]).
        appendTo(form).click(sendSearchForm);

    form.append($('<div>').addClass("search-arrow"));

    setLanguage();
    this.onInit();
};
