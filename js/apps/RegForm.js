var jqElements = {
    "main-block": $('<form>').addClass('start-form reg-form')
};
this.getJQElements = function(name) {
    return jqElements[name];
};
var CONSTS = {
    "classes": {
        "header-form": "header-form",
        "input-holder": "input-holder",
        "back-button": "back-button",
        "second-section": "second-section",
        "submit-button": "submit-button"
    },
    "placeholders": {
        "login-input": "nick",
        "password-input": "password",
        "confirm-password-input": "repeat_password",
        "email-input": "email"
    },
    "title-form": "sign_up",
    "names-fields": {
        "login": "name",
        "gender": "male",
        "password": "pswd",
        "email": "email"
    }
};
var Event = this.Event;

var forTranslate =  {
    "placeholder": {},
    "text": {}
};

var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k in forTranslate[i]) {
            switch(i) {
                case "placeholder":
                    forTranslate[i][k].attr("placeholder", TRANSLATES[k] || k);
                    break;
                case "text":
                    forTranslate[i][k].text(TRANSLATES[k] || k);
                    break;
            }
        }
    }
};

/* Сюда сохраняем все поля формы для отправки */
var regForm = this;
var secondBlock = false;
var inputs = {};

var Exit = function() {
    regForm.Event.callHandlers("onExit");
};

var changeGender = function(gender) {
    regForm.Event.callHandlers("onChangeGender", gender);
};

var sendAuthForm = function() {
    var data = {};
    for (var i in inputs) {
        if (i != CONSTS["names-fields"]["gender"]) {
            data[i] = inputs[i].val();
        } else {
            for (var z in inputs[i]) {
                if (inputs[i][z].is(':checked')) {
                    data[i] = inputs[i][z].val();
                    break;
                }
            }
        }
    }
    regForm.Event.callHandlers("onSendForm", data);
};
this.getSecondBlock = function() {
    return secondBlock;
};
this.setGender = function(gender) {
    var genderInputs = inputs[CONSTS["names-fields"]["gender"]];
    for (var i in genderInputs) {
        i != gender ?
            genderInputs[i].removeAttr('checked') :
            genderInputs[i].attr('checked', 'checked');
    }
};

this._constructor = function(options) {

    //------------------------------ Заголовок формы ---------------------------------
    var headerForm = $('<div>').
        addClass(CONSTS.classes["header-form"]).
        appendTo(jqElements['main-block']);

    forTranslate["text"][CONSTS["title-form"]] = $("<span>").prependTo(headerForm);

    $('<div>').
        addClass(CONSTS.classes["back-button"]).
        prependTo(headerForm).
        click(Exit);
    //---------------------------- /Заголовок формы ----------------------------------

    //-------------------- Блок для дополнительного контента -------------------------
    secondBlock = $('<div>').
        addClass(CONSTS.classes["second-section"]).
        appendTo(jqElements['main-block']);
    //-------------------- / Блок для дополнительного контента -----------------------

    //---------------------------- Поле выбора пола ----------------------------------
    inputs[CONSTS["names-fields"]["gender"]] = {};
    var genderControl = $("<div>").addClass("gender-control").
        appendTo(jqElements['main-block']);

    /*---- Для мужского пола ----*/
    var maleLabel = $("<label>").addClass("gender-select").appendTo(genderControl);
    var maleInput = inputs[CONSTS["names-fields"]["gender"]]["male"] = $("<input>").attr({
        "type": "radio",
        "name": "gender",
        "value": 1
    }).change(function() {
        changeGender("male");
    });
    maleLabel.append(
        maleInput,
        "<span>",
        $("<span>").addClass("gender-icon male")
    );

    /*---- Для женского  пола ----*/
    var feMaleLabel = $("<label>").addClass("gender-select").appendTo(genderControl);
    var feMaleInput = inputs[CONSTS["names-fields"]["gender"]]["female"] = $("<input>").attr({
        "type": "radio",
        "name": "gender",
        "value": 0
    }).change(function() {
        changeGender("female");
    });
    feMaleLabel.append(
        feMaleInput,
        "<span>",
        $("<span>").addClass("gender-icon female")
    );
    //---------------------------- / Поле выбора пола --------------------------------

    //---------------------------- Поле ввода логина ---------------------------------
    var loginInput = inputs[CONSTS["names-fields"]["login"]] = $('<input>').
        addClass(CONSTS.classes["login-input"]).attr({
            'type': 'text'
        });

    forTranslate["placeholder"][CONSTS["placeholders"]["login-input"]] = loginInput;
    $('<div>').
        addClass(CONSTS.classes["input-holder"]).
        append(loginInput).
        appendTo(jqElements['main-block']);
    //-------------------------- / Поле ввода логина ---------------------------------

    //--------------- Всё путём сейчас у меня :) Поле ввода пароля -------------------
    var pwdInput = inputs[CONSTS["names-fields"]["password"]] = $('<input>').
        addClass(CONSTS.classes["password-input"]).attr({
            'type': 'password'
        });
    forTranslate["placeholder"][CONSTS["placeholders"]["password-input"]] = pwdInput;
    $('<div>').
        addClass(CONSTS.classes["input-holder"]).
        append(pwdInput).
        appendTo(jqElements['main-block']);
    //---------------------------- / Поле ввода пароля ---------------------------------

    //---------------------------- Поле ввода пароля -----------------------------------
    var rptPwdInput = $('<input>').
        addClass(CONSTS.classes["password-input"]).attr({
            'type': 'password'
        });
    forTranslate["placeholder"][CONSTS["placeholders"]["confirm-password-input"]] = rptPwdInput;

    $('<div>').
        addClass(CONSTS.classes["input-holder"]).
        append(rptPwdInput).
        appendTo(jqElements['main-block']);
    //---------------------------- / Поле ввода пароля ---------------------------------

    //---------------------------- Поле ввода email`а ----------------------------------
    var EmailInput = inputs[CONSTS["names-fields"]["email"]] = $('<input>').
        addClass(CONSTS.classes["email-input"]).attr({
            'type': 'text'
        });
    forTranslate["placeholder"][CONSTS["placeholders"]["email-input"]] = EmailInput;
    $('<div>').
        addClass(CONSTS.classes["input-holder"]).
        append(EmailInput).
        appendTo(jqElements['main-block']);
    //---------------------------- / Поле ввода email`а --------------------------------

    //---------------------------- Кнопка "отправить форму" ----------------------------
    $('<div>').
        addClass(CONSTS.classes["submit-button"]).
        appendTo(jqElements['main-block']).
        click(sendAuthForm);
    //-------------------------- / Кнопка "отправить форму" ----------------------------
    setLanguage();
    this.onInit();
};
