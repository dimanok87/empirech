var STATUSES = {
    "IN_LOAD":      1,
    "ON_LOAD":      2,
    "STARTED":      3
};

//--- Набор объектов приложений ---
window.applications = {};


//-- Объект обработчика событий --//
var Events = function() {
    var handlers = {}, application;
    //--- Метод добавляет обработчик "b" для события "a" ---
    this.addHandlers = function(a, b) {
        if (typeof a == "object") {
            for (var i in a) {
                this.addHandlers(i || false, a[i || false]);
            }
        } else if (b || false) {
            (handlers[a] = handlers[a] || []).push(b);
            if (a == "onInit" && application && application["status"] == STATUSES["STARTED"]) {
                application["application"].Event.callHandlers(a);
                application["application"].Event.removeHandlers(a);
            }
        }

    };

    //--- Метод вызывает обработчик ---
    this.callHandlers = function(a, params) {
        for (var i = 0; i < (handlers[a] || []).length; i++) {
            handlers[a][i](params || application["application"]);
        }
    };

    //--- Метод удаляет обработчики ---
    this.removeHandlers = function(a) {
        if (a) {
            handlers[a] = false;
        } else {
            handlers = {};
        }
    };

    //--- Метод установки приложения ---
    this.setApplication = function(app) {
        application = applications[app];
    };
};


//--- Функция запуска приложений ---
var Application = function(name, options) {
    applications[name] = {
        Event: new Events(),
        status: STATUSES["IN_LOAD"],
        Close: function() {
            applications[name]["Event"].removeHandlers();
        }
    };

    /* Загружаем файл приложения */
    Functions.loadApplication(name, function(app) {
        app.prototype.Close = function() {
            if (this.getJQElements) {
                this.getJQElements("main-block").empty().remove();
            }
            this.Event.callHandlers("onClose");
            this.Event.removeHandlers();
            delete applications[name]["application"];
            applications[name]["status"] = STATUSES["ON_LOAD"];
        };
        app.prototype.onInit = function() {
            applications[name]["status"] = STATUSES["STARTED"];
            this.Event.callHandlers("onInit");
            this.Event.removeHandlers("onInit");
        };
        app.prototype.name = name;
        app.prototype.isStarted = function() {
            return applications[name]["status"] == STATUSES["STARTED"];
        };

        var _app = new app();
        app.prototype.Event = applications[name]["Event"];
        applications[name] = {
            "application": _app,
            "prototype": app,
            "status": STATUSES["ON_LOAD"],
            "Event": applications[name]["Event"]
        };

        _app["Event"].setApplication(name);
        _app["_constructor"](options);
    });
    return applications[name];
};

this.Application = function(name, options) {
    if (!applications[name]) {
        return Application(name, options);
    } else if (applications[name]["status"] == STATUSES["STARTED"]) {
        return applications[name]['application'];
    } else if (applications[name]["status"] == STATUSES["ON_LOAD"]) {
        applications[name]['application'] = new applications[name]['prototype']();
        applications[name]['application']["Event"].setApplication(name);
        setTimeout(function() {
            applications[name]['application']["_constructor"](options);
        }, 10);
        return applications[name]['application'];
    } else {
        return applications[name];
    }
};

//-- Метод запуск контроллера --//
this.Controller = function(name) {
    Functions.startController(name, function(controller) {
        controller();
    });
};
