var jqElements = {
    "main-block": $('<ul>').addClass('start-menu-block big-menu')
};


this.getJQElements = function(name) {
    return jqElements[name];
};
var forTranslate = {};
var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        forTranslate[i].text(TRANSLATES[i] || i);
    }
};

var startMenu = this;

var createItemMenu = function(itemParam) {
    var itemMenu = $('<li>').appendTo(jqElements['main-block']);
    forTranslate[itemParam['title']] = $("<span>").appendTo(itemMenu.addClass("item-start-menu"));
    itemParam.item = itemMenu;
    itemMenu.addClass(itemParam['label']).click(function() {
        itemParam.selected = !itemParam.selected;
        startMenu["Event"]["callHandlers"]("onSelectItem", itemParam);
    });
};

//--- Подгружаем конфиги стартового меню, отображаем его, выполняем Handlers-функции ---
this._constructor = function() {
    var app = this;
    $.ajax({
        url: 'configs/StartMenu.json',
        dataType: 'json',
        async: false,
        success: function(config) {
            for (var i = 0; i < config.length; i++) {
                createItemMenu(config[i]);
            }
            setLanguage();
            app.onInit();
        }
    });
};
