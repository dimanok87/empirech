//============================== Базовые ф-ции и переменные ==============================

//--- Объект с блоками для перевода ---
var forTranslate = {};

//--- Добавление блока с текстом для перевода ---
var addToTranslate = function(label, element) {
    forTranslate[label] = (forTranslate[label] || []);
    forTranslate[label].push(element.text(TRANSLATES[label] || label));
    return element;
};

//--- Установка нового языка ---
this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k = 0; k < forTranslate[i].length; k++) {
            forTranslate[i][k].text(TRANSLATES[i] || i);
        }
    }
};

//--- Блоки для отображения баланса пользователя ---
var userBalanceBlocks = [];

//--- Устанавливаем баланс пользователя ---
this.setUserBalance = function(balance) {
    for (var k = 0; k < userBalanceBlocks.length; k++) {
        userBalanceBlocks[k].text(balance);
    }
};

//============================== / Базовые ф-ции и переменные ==============================

//--- Объект одного товара в витрине ---
var showCaseItem = function(options) {

    var product = options["product"];
    var params = options["params"];
    this.getParams = function() {
        return product;
    };
    //--- Элемент витрины ---
    var Item = $(product["views"]["fv"] ? "<div>" : "<label>").addClass("product-element "+ product["source"] + "-element");

    var previewItem = new Image(),

        checkbox = $("<input>").
            attr("type", "checkbox").
            change(function() {
                checkbox.is(":checked") ? selectItem() : unSelectItem();
            }
        ),

        imgContainer = $("<div>").
            addClass("product-element-container").
            appendTo(Item),

        loader = $("<div>").
            addClass("pre-loader small").
            append("<div>").
            appendTo(imgContainer),

        checkboxProduct = $("<label>").
            addClass("label-checkbox").
            append(
                checkbox, $("<span>")
            ),

        nameProduct = $("<span>").
            addClass("name-product").
            text(product["name"]);

    this.getProductContainer = function() {
        return imgContainer;
    };

    //--- Завершена загрузка изображения ---
    previewItem["onload"] = function() {

        //--- Удаляем прелоадер изображения ---
        loader.empty().remove();

        //--- Вставляем загруженное изображение ---
        imgContainer.append(previewItem);

        //--- Показываем имя продукта и чекбокс для выделения ---
        Item.append(checkboxProduct, nameProduct);

        shopCartItem = new cartItem({
            id: product["id"],
            name: product["name"],
            price: params["price"],
            src: previewItem["src"],
            ondelete: function() {
                unSelectItem();
                checkbox.attr("checked", false);
            },
            onchange: function() {
                params["changeselect"](thItem);
            }
        });
    };

    //--- Загружаем изображение продукта ---
    this.loadImage = function() {
        loader.addClass("animated");
        var size = 2;
        switch(product["source"]) {
            case "gifts":
                size = 1;
                break;
            case "clothes":
                size = 3;
                break;
        }
        previewItem.src = product["views"]["size-" + size]["img"];
    };

    //--- Возвращает элемент витрины ---
    this.getItemShowCase = function() {
        return Item;
    };

    var thItem = this;
    //--- Выделяем элемент ---
    var selectItem = this.selectItem = function() {
        checkbox.get(0).checked = true;
        Item.addClass("checked");
        params["onselect"](thItem);
    };

    //--- Снимаем выделение элемента ---
    var unSelectItem = this.unSelectItem = function() {
        checkbox.get(0).checked = false;
        Item.removeClass("checked");
        params["onunselect"](thItem);
    };

    //--- Объект корзины ---
    var shopCartItem = false;

    this.getCartItem = function() {
        return shopCartItem;
    };

    var data = {};

    this.setData = function(a, b) {
        data[a] = b;
    };

    this.getData = function(a) {
        return data[a];
    };
};

//--- Объект одного товара в корзине ---

var cartItem = function(options) {

    //--- Блок элемента корзины ---
    var Item = $("<div>").addClass("shop-cart-element");

    //--- Создаём контент продукта для корзины ---
    var cartItemImage = new Image();

    var selectedPriceItem = false, oldSelectedPriceItem = false;

    //--- Блок с выбором срока / кол-ва аренды ---
    var priceLabels = function() {
        var priceLabels = [];
        for (var i = 0; i < options["price"].length; i++) {
            priceLabels.push(onePriceLabel(options["price"][i]));
        }
        return priceLabels;
    };

    //--- Один пункт выбора ---
    var onePriceLabel = function(labelParams) {
        var label = $("<label>"),
            radio = $("<input>").attr({
                type: "radio",
                name: "product-select-" + options["id"]
            }).change(function() {
                oldSelectedPriceItem = selectedPriceItem;
                selectedPriceItem = labelParams;
                options['onchange']();
            });

        if (!selectedPriceItem) {
            selectedPriceItem = labelParams;
            radio.attr("checked", "checked");
        }
        return label.append(
            radio, $("<span>"), $("<span>").addClass("label-text-radio").append(
                $("<span>").append(
                    labelParams["price"] / 100, $("<span>").addClass("money-icon")
                ).addClass("price-product"),
                $("<span>").text(labelParams["count"] + " "),
                addToTranslate(labelParams["label"], $("<span>"))
            )
        );
    };


    Item.append(
        $("<div>").addClass("product-element cart-element").append(
            $("<div>").addClass("product-element-container").append(cartItemImage),
            $("<div>").addClass("close-button").click(function() {
                options["ondelete"]();
            }),
            $("<span>").addClass("name-product").text(options["name"])
        ),
        $("<div>").addClass("price-select").append(priceLabels())
    );

    cartItemImage.src = options["src"];

    this.getItem = function() {
        return Item;
    };

    this.getParams = function() {
        return options;
    };

    this.getSelectedPriceItem = function() {
        return {
            "old": oldSelectedPriceItem,
            "new": selectedPriceItem
        };
    };
};

//--- Объект одной витрины (тип и категория) ---
var showCase = function() {

    //--- Контейнер витрины ---
    var container = $("<div>").addClass("showcase-list");

    //--- Список продуктов ---showCase["container"].scrollTop(showCase.scrollTop);
    var products = [];

    //--- Скролл витрины (нужен для правильного отображения при активации витрины) ---
    var scrollTop = 0;

    //--- Сохраняем позицию скролла ---
    container.bind("scroll", function() {
        scrollTop = container.scrollTop();
    });

    //--- Проверка элемента витрины в поле видимости ---
    var testVisibleImg = function(product) {
        if (product.isIni) return;

        var itemBlock = product.getItemShowCase(),
            itemPosition = itemBlock.offset()['top'] - container.offset()['top'],
            itemHeight = itemPosition + itemBlock.height();

        if (itemHeight > 0 && itemPosition < container.height()) {
            product.isIni = true;
            product.imgLoader = false;
            container.unbind("scroll", product.imgLoader);
            product.loadImage();
        }
    };

    var createdShowCase = false;

    //--- Создание витрины ---
    var createProductItem = function(product) {
        product.getItemShowCase().appendTo(container);
        product.imgLoader = function() {
            testVisibleImg(product);
        };
        container.bind("scroll", product.imgLoader);
    };

    //--- Создание витрины (элементов витрины / товаров) ---
    this.createShowCase = function() {
        if (createdShowCase) return;
        createdShowCase = true;
        for (var i = 0; i < productsParams.length; i++) {
            var product = new showCaseItem(productsParams[i]);
            products.push(product);
            createProductItem(product);
        }
    };


    //--- Добавление продукта и его отображение в витрине ---
    var productsParams = [];

    this.getProducts = function() {
        return products;
    };

    this.addProduct = function(product) {
        productsParams.push(product);
    };

    //--- Инициализируем изображения витрины ---
    this.iniProductsImages = function() {
        for (var i = 0; i < products.length; i++) {
            testVisibleImg(products[i]);
        }
    };

    //--- Возвращает контейнер с витриной ---
    this.getContainer = function() {
        return container;
    };

    //--- Восстанавливаем скролл ---
    this.restoreScroll = function() {
        container.scrollTop(scrollTop);
    };
};


//--- Корзина ---
var shopCart = function() {
    var cartBlock = $("<div>").addClass("shop-cart");

    this.getBlockCart = function() {
        return cartBlock;
    };
};

//--- Объект витрины и корзины магазина ---
this.shopShowcase = function(options) {
    return new (function() {

        //========================= Блоки, содержащие контент =========================

        //--- Товары магазина ---
        var products = options["products"]["byInd"] || false;
        this.getProducts = function() {
            return options["products"];
        };
        //--- Витрины ---
        var showCases = {};
        var selectedProducts = [];
        //========================= / Блоки, содержащие контент =========================

        this.getSelectedProducts = function() {
            return selectedProducts;
        };

        //================= Сортировка товаров по витринам =================
        var sortProducts = function() {
            for (var k = 0; k < products['length']; k++) {
                var cat = products[k]["type"] || 0, showCaseId = "category-" + cat;

                //---- Если нет витрины для определённого типа товара - строим её ----//
                if (!showCases[showCaseId]) {
                    showCases[showCaseId] = new showCase();
                }

                //--- Добавляем товар в витрину ---
                showCases[showCaseId].addProduct({
                    product: products[k],
                    params: {
                        "onselect": function(item) {
                            Cart.getBlockCart().append(item.getCartItem().getItem());
                            selectedProducts.push(item);
                            eventHandlers["addSelection"] ?
                                eventHandlers["addSelection"](item) :
                                false;
                        },
                        "onunselect": function(item) {
                            var newItems = [];
                            item.getCartItem().getItem().detach();
                            for (var k = 0; k < selectedProducts.length; k++) {
                                if (item != selectedProducts[k]) {
                                    newItems.push(selectedProducts[k]);
                                }
                            }
                            selectedProducts = newItems;
                            eventHandlers["delSelection"] ?
                                eventHandlers["delSelection"](item) :
                                false;
                        },
                        "changeselect": function(item) {
                            eventHandlers["changeSelection"] ?
                                eventHandlers["changeSelection"](item) :
                                false;
                        },
                        "price": options["price-list"]
                    }
                });
            }
        };
        sortProducts();

        //================= / Сортировка товаров по витринам =================

        //--- Возвращает блок витрины ---
        this.getShowCase = function(id) {
            id = id || false;
            if (!id) {
                for (var i in showCases) {
                    id = i;
                    break;
                }
            }
            showCases[id].createShowCase();
            return showCases[id];
        };

        //--- Возвращает блок корзины ----
        var Cart = new shopCart();

        this.getCart = function() {
            return Cart;
        };

        var eventHandlers = false;

        this.setHandlers = function(handlers) {
            eventHandlers = handlers;
        };
    })
};

this._constructor = function() {
    this.onInit();
};
