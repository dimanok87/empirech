var jqElements = {
    'main-block': $('<div>').addClass('avatar-application')
};

this.getJQElements = function(name) {
    return jqElements[name];
};


var shopTopPanel = $("<div>").addClass("shop-top-panel").appendTo(jqElements["main-block"]);
var myMoneyText = $("<span>");
var balance = 0;

this.setUserBalance = function(b) {
    myMoneyText.text(balance = b);
};


$("<span>").addClass("my-money").appendTo(shopTopPanel).append(myMoneyText, $("<span>").addClass("money-icon"));


var buyNewElements = function() {
    var request = {};
    for (var z in Selected) {
        User["view"][userParamsNames[z]]["id"] = request[userParamsNames[z]] = Selected[z];
    }
    setAmount(0);
    Functions.sendToAPI("api/user/changehb", function() {
        app.Event.callHandlers("onChangeBalance");
        resetPersonage();
    }, "POST", request);
};

var app = this;
var resetPersonage = function() {
    Functions.sendToAPI("api/user/view/r");
};

var fullAmountText = $("<span>");
var am = 0;
var setAmount = function(amount) {
    fullAmountText.text(am = amount);
};

$("<span>").addClass("amount-money").appendTo(shopTopPanel).append(fullAmountText, $("<span>").addClass("money-icon"));
var thApp = this;

$("<div>").addClass("nav-shops-button").append($("<span>").addClass("buy-icon")).click(function() {
    if (am) {
        am < balance ? buyNewElements() : false;
    } else {
        resetPersonage();
    }

    thApp.Event.callHandlers("onBuy");
}).appendTo(shopTopPanel);

var avatarContainer = $("<div>").addClass("avatar-app-container").appendTo(jqElements["main-block"]);

var userParamsNames = {
    "bodies": "body",
    "heads": "head"
};
var createPersonage = function(factory) {
    var avatarPersonage = factory({
        "gender": gender,
        "size": 3,
        "personage": {
            "heads": Selected["heads"],
            "bodies": Selected["bodies"]
        },
        "loader": $('<div class="pre-loader medium">').append($("<div>"))
    });

    avatarPersonage.addControl({
        controls: {
            heads: true,
            bodies: true
        }
    }, function(data) {
        Selected[data["type"]] = data["data"];
        for (var k in Selected) {
            if (Selected[k] != User["view"][userParamsNames[k]]["id"]) {
                setAmount(priceList / 100);
                return;
            }
            setAmount(0);
        }
    });
    avatarPersonage.load();
    avatarContainer.append(avatarPersonage["getContainer"]());
};

var Selected, User, gender, priceList;

this._constructor = function(params) {
    User = params["user"];
    priceList = params["price"];
    gender = User.male ? "male" : "female";
    Selected = {
        bodies: User["view"]["body"]["id"],
        heads: User["view"]["head"]["id"]
    };
    createPersonage(params["personageFactory"]);
    setAmount(0);
    this.onInit();
};
