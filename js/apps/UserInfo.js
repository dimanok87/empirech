var jqElements = {
    'main-block': $('<div>').addClass('userinfo-application')
};
var form = $("<form>").appendTo(jqElements["main-block"]);


this.getJQElements = function(name) {
    return jqElements[name];
};

//--- Объект с блоками для перевода ---
var forTranslate = {};

//--- Добавление блока с текстом для перевода ---
var addToTranslate = function(label, element) {
    forTranslate[label] = (forTranslate[label] || []);
    forTranslate[label].push(element.attr("placeholder", TRANSLATES[label] || label));
    return element;
};

//--- Установка нового языка ---
this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k = 0; k < forTranslate[i].length; k++) {
            forTranslate[i][k].attr("placeholder", TRANSLATES[i] || i);
        }
    }
};

var nickNameInput =     addToTranslate("nick", $("<input>")).attr("type", "text");
var lastNameInput =     addToTranslate("last_name", $("<input>")).attr("type", "text");
var firstNameInput =    addToTranslate("first_name", $("<input>")).attr("type", "text");
var birthDayInput =     addToTranslate("date_of_birth", $("<input>")).attr("type", "text");
var countryInput =      addToTranslate("country", $("<input>")).attr("type", "text");
var cityInput =         addToTranslate("city", $("<input>")).attr("type", "text");
var statusInput =       addToTranslate("status", $("<input>")).attr("type", "text");
var sendButton = $("<div>").addClass("send-button");

form.append(
    nickNameInput,
    lastNameInput,
    firstNameInput,
    birthDayInput,
    countryInput,
    cityInput,
    statusInput,
    sendButton
);

this._constructor = function() {
    this.onInit();
};
