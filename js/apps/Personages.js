var RESOURCES = Functions.getSources();
var sizes = {
    size_2: {
        h: 142
    }
};
this.newPersonage = function(params) {
    return new (function(params) {

        //--- Используемые классы блоков персонажа ---
        var CLASSES = {
            "element": "personage-image",
            "loader": "pre-loader big",
            "container": "personage-container",
            "size-prefix": "size",
            "user-name": "personage-user-name",
            "static-block": "personage-static-container"
        };
        //--- Конструктор персонажа ---

        /* Контйенер для персов типа: манекен+одежда, аватар */
        var staticContainer = $("<div>").addClass(CLASSES["static-block"]);

        /* Контейнер с нашим персонажем */
        var containerPersonage = $('<div>').
             addClass(CLASSES["container"]).
             addClass(CLASSES["size-prefix"] + "_" + params["size"]).
             addClass("gender-" + params["gender"]);

        /* Содержит все изображения персонажа */
        var personageElements = {};

        /* Содержит статус элементов персонаж: true - загружено; false || undefined - не загружено */
        var personageElementsStatus = {};

        /* Содержит параметры отображаемого персонажа  */
        var userParamsElements = params.personage;

        //--- Имя персонажа ---
        var userNameBlock = $('<div>').addClass(CLASSES["user-name"]);
        if (params["userName"]) {
            containerPersonage.append(userNameBlock.text(params["userName"]));
        }
        /* Проверяем, загружены ли все элементы персонажа */
        var onloadedElements = function() {
            for (var i in personageElements) {
                if (!personageElementsStatus[i]) {
                    return false;
                }
            }
            return true;
        };

        /* Обработчик, который выполняется при загрузке всех элементов персонажа */
        var onLoadElementsCallback = false;
        var clonesImages = {};
        var CSSs = {};
        /* Устанавливает новый элемент */
        var intervalLoader = false;
        var setImageElement = function(newElementData, type) {
            var collection = RESOURCES[type.split("-")[0]][params.gender],
            //--- Новый элемент из набора ---//
                SOURCE = newElementData == "random" ?
                    collection["byInd"][Math.round(Math.random() * (collection["byInd"]["length"] - 1))] :
                    collection["byId"]["element-" + newElementData];

            //--- Удаляем изображение элемента, если в параметрах false ---
            if (!newElementData) {
                if (personageElements[type]) {
                    personageElements[type].detach();
                }
                userParamsElements[type] = false;
                personageElementsStatus[type] = true;
                return;
            }

            userParamsElements[type] = SOURCE["id"];
            personageElementsStatus[type] = false;
            clonesImages[type] = new Image();

            if (!personageElements[type]) {
                personageElements[type] = $("<img>").
                    addClass(CLASSES["element"]).
                    addClass(CLASSES["element"] + '-' + type).
                    attr({draggable: "false"});
            }

            var view = SOURCE["views"]["size-" + params["size"]];
            switch(type.split("-")[0]) {
                case "models":
                case "heads":
                case "bodies":
                case "magic":
                case "clothes":
                    CSSs[type] = {
                        marginLeft: -(view["x"] ? view["x"] : 0),
                        marginTop: view["y"]
                    };
                    break;
                case "pers":
                    if (sizes["size_" + params["size"]]) {
                        CSSs[type] = {
                            marginTop: sizes["size_" + params["size"]]["h"] - view["h"]
                        };
                    }
                    break;
                default:
                    CSSs[type] = {};
                    break;
            }
            var onloadAcces = function() {
                personageElementsStatus[type] = true;
                if (onloadedElements()) {
                    intervalLoader ? clearTimeout(intervalLoader) : params["loader"] ? params["loader"].detach() : false;
                    intervalLoader = false;

                    for (var t in clonesImages) {
                        if (clonesImages[t]) {
                            var container = containerPersonage;
                            if (t != "pers" && t != "comp" && t != "magic") {
                                container = staticContainer;
                            }
                            personageElements[t].css(CSSs[t]);
                            container.prepend(
                                personageElements[t].attr({
                                    "src": clonesImages[t].src
                                }));
                            clonesImages[t] = false;
                        }
                    }

                    if (userParamsElements["models"] || (userParamsElements["heads"] && userParamsElements["bodies"])) {
                        staticContainer.appendTo(containerPersonage);
                    } else {
                        staticContainer.detach();
                    }
                    if (userParamsElements["comp"]) {
                        personageElements["comp"].prependTo(containerPersonage);
                    }
                    if (params["userName"]) {
                        userNameBlock.prependTo(containerPersonage);
                    }
                    onLoadElementsCallback ? onLoadElementsCallback() : false;
                    onLoadElementsCallback = false;
                }
            };
            clonesImages[type]["onload"] = function() {
                onloadAcces();
            };
            if (clonesImages[type].src != view["img"]) {
                clonesImages[type].src = view["img"];
            } else {
                onloadAcces();
            }
        };

        this.setLoader = function(loader) {
            params["loader"] ? params["loader"].empty().remove() : false;
            params["loader"] = loader;
        };

        /* Метод, который начинает загрузку изображений и по завршении вызывает обработчик */
        this.load = function(callback) {
            onLoadElementsCallback = callback || false;
            setData(userParamsElements);
        };

        /* Возвращает контейнер с персонажем */
        this.getContainer = function() {
            return containerPersonage;
        };

        /* Возвращает параметры элементы персонажа */
        this.getElement = function(name) {
            return userParamsElements[name];
        };
        this.setNick = function(nick) {
            userNameBlock.text(params["userName"] = nick)
        };
        var getIndexElement = function(type, range) {
            var collection = RESOURCES[type.split("-")[0]][params.gender]["byInd"];
            var index = 0;
            for (var k = 0; k < collection.length; k++) {
                if (userParamsElements[type] === collection[k]["id"]) {
                    k+= range;
                    index = k > collection.length - 1 ? 0 : (k < 0 ? collection.length - 1 : k);
                    break;
                }
            }
            return collection[index]["id"];
        };

        var setData = this.setData = function(newData) {
            var showLoader = false;
            for (var i in newData) {
                if (!showLoader && newData[i] && params["loader"]) {
                    showLoader = true;
                }
                setImageElement(newData[i], i);
            }
            if (showLoader) {
                intervalLoader = setTimeout(function() {
                    if (params["loader"]) {
                        containerPersonage.append(params["loader"]);
                        intervalLoader = false;
                    }
                }, 20);
            }
        };
        this.getData = function() {
            return userParamsElements;
        };

        this.setGender = function(gender) {
            containerPersonage.removeClass("gender-" + params.gender);
            containerPersonage.addClass("gender-" + gender);
            params.gender = gender;
        };

        var createControlElement = function(nameControl, onchangeCallback) {
            var controlBlock = $('<div>').
                addClass('personage-control personage-' + nameControl + '-control');
            switch (nameControl) {
                case "heads":
                case "bodies":
                    var newData = {};
                    /* Кнопка показа следующего элемента коллекции */
                    $('<div>').click(function() {
                        newData[nameControl] = getIndexElement(nameControl, 1);
                        setData(newData);
                        if (onchangeCallback) {
                            onchangeCallback({
                                type: nameControl,
                                data: newData[nameControl]
                            })
                        }
                    }).appendTo(controlBlock).addClass("changer-button next-element");

                    /* Кнопка показа предыдущего элемента коллекции */
                    $('<div>').click(function() {
                        newData[nameControl] = getIndexElement(nameControl, -1);
                        setData(newData);
                        if (onchangeCallback) {
                            onchangeCallback({
                                type: nameControl,
                                data: newData[nameControl]
                            })
                        }
                    }).appendTo(controlBlock).addClass("changer-button prev-element");
                    break;
                case "random":
                    controlBlock.click(function() {
                        setData({
                            "heads": "random",
                            "bodies": "random"
                        });
                    });
                    break;
            }
            containerPersonage.append(controls[nameControl] = controlBlock);
        };

        var controls = {};

        /* Метод добавляет управление персонажем */
        var addControl = this.addControl = function(options, cb) {
            cb = cb || false;
            options = $.extend({
                "classControl": false
            }, options);
            for (var i in options["controls"]) {
                var nameControl = i || false;
                if (controls[nameControl] || false) {
                    controls[nameControl].empty().remove();
                }
                if (options["controls"][i])
                    createControlElement(nameControl, cb);
            }
        };

    })(params);
};

this._constructor = function() {
    this.onInit();
};
