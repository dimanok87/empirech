var jqElements = {
    "main-block": $('<div>').addClass('country-preview'),
    "personages": $('<ul>').addClass('personages-list')
};

var countryPreviewBackground = $("<div>").addClass("country-preview-background").appendTo(jqElements["main-block"]);
jqElements["personages"].appendTo(countryPreviewBackground);

this.getJQElements = function(name) {
    return jqElements[name];
};

this.setBackground = function(src, callback) {
    var CORRECTSIZE = 3;
    var bgImage = new Image();
    bgImage.src = src;
    bgImage.onload = function() {
        var canvas = document.createElement("canvas"),
            context = canvas.getContext("2d"),
            w = bgImage.width - CORRECTSIZE,
            h = canvas.height = bgImage.height;
        canvas.width = w * 2;
        context.drawImage(bgImage, 0, 0, w, h);
        context.save();
        context.translate(w, 0);
        context.scale(-1, 1);
        context.drawImage(bgImage, -w, 0, w, h);
        context.restore();
        countryPreviewBackground.css({
            backgroundImage: 'url(' + canvas.toDataURL() + ')'
        });
        callback ? callback() : false;
    };
};

//========================== Анимация страны ========================
var animateRange = 0.3;
var backgroundPosition = 0, oldBgPosition;
var isAnimate = false;
var duration = 300;
var listPosition = 0;
var marginOut = 100;

var reanimateListPosition = function(fl) {
    widthContainer = jqElements["personages"].width();
    countryWidth = jqElements["main-block"].width();
    fl = !(fl || false);
    if (countryWidth - widthContainer > 0) {
        animateList((countryWidth - widthContainer) / 2, fl);
        prevButton.detach();
        nextButton.detach();
    } else {
        if (listPosition < 0) {
            countryPreviewBackground.prepend(prevButton);
            setTimeout(function() {
                prevButton.addClass("visibled");
            }, 5);
            iniTextsButtons();
        } else {
            prevButton.removeClass("visibled");
            setTimeout(function(){
                prevButton.detach();
            }, 200);
            animateList(Math.min(listPosition, marginOut), fl, iniTextsButtons);
        }
        if (listPosition <= countryWidth - widthContainer) {
            nextButton.removeClass("visibled");
            setTimeout(function() {
                nextButton.detach();
            }, 200);
            animateList(Math.max(countryWidth - widthContainer - marginOut, listPosition), fl, iniTextsButtons);
        } else {
            countryPreviewBackground.prepend(nextButton);
            setTimeout(function() {
                nextButton.addClass("visibled");
            }, 5);
            iniTextsButtons();
        }
    }
};

var iniTextsButtons = function() {
    var right = personagesList.length;
    var left = 0;
    for (var i = 0; i < personagesList.length; i++) {
        var item = personagesList[i];
        var itemOffset = item.offset()["left"];
        if (itemOffset < 0) {
            left++;
        } else {
            if (itemOffset + item.outerWidth() > countryWidth) {
                break;
            } else {
                right--;
            }
        }
    }
    counterPrev.text(left || "");
    counterNext.text(right - left || "");
};

var animateList = function(to, isAnim, cb) {
    isAnim = isAnim || false;
    cb = cb || false;
    if (isAnimate) return;
    listPosition = to;
    oldBgPosition = backgroundPosition;
    backgroundPosition = to * animateRange;

    if (isAnim) {
        isAnimate = true;
        jqElements["personages"].animate({
            "margin-left": to
        }, duration, function() {
            isAnimate = false;
            cb ? cb() : false;
            iniTextsButtons();
        });
        $({temporary_x: oldBgPosition}).animate({temporary_x: backgroundPosition}, {
            duration: duration,
            step: function() {
                countryPreviewBackground.css({
                    "background-position": this.temporary_x + "px bottom"
                });
            }
        });
    } else {
        jqElements["personages"].css({"margin-left": to});
        countryPreviewBackground.css({
            "background-position": backgroundPosition + "px bottom"
        });
    }
};
//========================== / Анимация страны ========================

var startPosition = 0;
var mouseUpIni = function() {
    jqElements["personages"].bind("mousedown", downCountry);
    win.unbind("mousemove", iniMovieng);
    win.unbind("mouseup", mouseUpIni);
    reanimateListPosition();
};
var mouseUpNoIni = function() {
    jqElements["personages"].bind("mousedown", downCountry);
    win.unbind("mousemove", iniFirstMove);
    win.unbind("mouseup", mouseUpNoIni);
};

var iniMovieng = function(e) {
    animateList(e.clientX - startPosition);
};
var iniFirstMove = function(e) {
    startPosition = e.clientX - listPosition;
    win.unbind("mousemove", iniFirstMove);
    win.unbind("mouseup", mouseUpNoIni);
    win.bind("mouseup", mouseUpIni);
    win.bind("mousemove", iniMovieng);
};
var downCountry = function(e) {
    if (e.which != 1) return;
    jqElements["personages"].unbind("mousedown", downCountry);
    win.bind("mousemove", iniFirstMove);
    win.bind("mouseup", mouseUpNoIni);
};


var personagesList = [];
var counterPrev = $("<span>").addClass("personages-counter");
var counterNext = $("<span>").addClass("personages-counter");

var prevButton = $('<div>').addClass('prev-personages navigation-button').click(function() {
    animateList(listPosition + countryWidth * 0.4, true, reanimateListPosition);
}).append(counterPrev, $("<div>").addClass("country-control prev-element"));

var nextButton = $('<div>').addClass('next-personages navigation-button').click(function() {
    animateList(listPosition - countryWidth * 0.4, true, reanimateListPosition);
}).append(counterNext, $("<div>").addClass("country-control next-element"));

this.addPersonage = function(item) {
    item.appendTo(jqElements["personages"]);
    reanimateListPosition();
    personagesList.push(item);
};

this.removePersonage = function(item) {
    var newItems = [];
    for (var k = 0; k < personagesList.length; k++) {
        if (!personagesList[k].is(item)) {
            newItems.push(personagesList[k]);
        }
    }
    personagesList = newItems;
    reanimateListPosition();
};

var widthContainer, countryWidth;

var endmousewheelTimeout = false;

jqElements["personages"].bind("mousewheel", function(event) {
    animateList(listPosition - event.originalEvent.deltaY);
    endmousewheelTimeout ? clearTimeout(endmousewheelTimeout) : false;
    endmousewheelTimeout = setTimeout(function(){
        reanimateListPosition();
    }, 100);
    iniTextsButtons();
});

this.iniCountry = function() {
    jqElements["personages"].bind("mousedown", downCountry);
    reanimateListPosition();
};

win.bind("resize", function() {
    reanimateListPosition(true);
});

this._constructor = function() {
    this.onInit();
};
