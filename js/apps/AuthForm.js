var jqElements = {
    "main-block": $('<form>').addClass('start-form auth-form')
};

this.getJQElements = function(name) {
    return jqElements[name];
};

var CONSTS = {
    "classes": {
        "header-form": "header-form",
        "login-input": "with-icon login-input",
        "password-input": "with-icon password-input",
        "input-holder": "input-holder",
        "remember-holder": "remember-holder",
        "back-button": "back-button",
        "submit-button": "submit-button"
    },
    "placeholders": {
        "login-input": "nick",
        "password-input": "password",
        "remember-input": "remember_me"
    },
    "title-form": "login",
    "names-fields": {
        "login": "login",
        "password": "pwd",
        "remember": "remember"
    },
    "regexps": {
        "login": /^[a-zA-Z0-9_]+$/,
        "pwd": /^[a-zA-Z0-9]+$/
    }
};

var forTranslate =  {
    "placeholder": {},
    "text": {}
};

var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k in forTranslate[i]) {
            switch(i) {
                case "placeholder":
                    forTranslate[i][k].attr("placeholder", TRANSLATES[k] || k);
                    break;
                case "text":
                    forTranslate[i][k].text(TRANSLATES[k] || k);
                    break;
            }
        }
    }
};

var inputs = {};
var authForm = this;
var Exit = function() {
    authForm.Event.callHandlers("onExit");
};

jqElements["main-block"].submit(function(e) {
    e.preventDefault();
    sendAuthForm();
});

var sendAuthForm = function() {
    var data = {};
    for (var i in inputs) {
        if (i != CONSTS["names-fields"]["remember"]) {
            data[i] = inputs[i].val();
            console.log(i);
            if (!CONSTS["regexps"][i].test(data[i])) {
                inputs[i].addClass("error-field");
                return;
            } else {
                inputs[i].removeClass("error-field");
            }
        } else {
            data[i] = inputs[i].is(":checked");
        }
    }
    authForm.Event.callHandlers("onSendForm", data);
};

this._constructor = function() {
    //------------------------------ Заголовок формы ---------------------------------
    var headerForm = $('<div>').
        addClass(CONSTS.classes["header-form"]).
        appendTo(jqElements['main-block']);

    forTranslate["text"][CONSTS["title-form"]] = $("<span>").prependTo(headerForm);

    $('<div>').
        addClass(CONSTS.classes["back-button"]).
        prependTo(headerForm).
        click(Exit);
    //---------------------------- /Заголовок формы ----------------------------------
    //---------------------------- Поле ввода логина ---------------------------------
    var loginInput = inputs[CONSTS["names-fields"]["login"]] = $('<input>').
        addClass(CONSTS.classes["login-input"]).attr({
            'type': 'text'
        }).bind("keypress",function(e) {
            if (e.which == 13) {
                sendAuthForm();
            }
        });
    forTranslate["placeholder"][CONSTS["placeholders"]["login-input"]] = loginInput;

    $('<div>').
        addClass(CONSTS.classes["input-holder"]).
        append(loginInput).
        appendTo(jqElements['main-block']);
    //-------------------------- / Поле ввода логина ---------------------------------
    //---------------------------- Поле ввода пароля ---------------------------------
    var pwdInput = inputs[CONSTS["names-fields"]["password"]] = $('<input>').
        addClass(CONSTS.classes["password-input"]).attr({
            'type': 'password'
        }).bind("keypress",function(e) {
            if (e.which == 13) {
                sendAuthForm();
            }
        });

    forTranslate["placeholder"][CONSTS["placeholders"]["password-input"]] = pwdInput;

    $('<div>').
        addClass(CONSTS.classes["input-holder"]).
        append(pwdInput).
        appendTo(jqElements['main-block']);
    //---------------------------- / Поле ввода пароля ---------------------------------
    //------------------------- Чекбокс "запомнить меня" -------------------------------
    var rememberInput = inputs[CONSTS["names-fields"]["remember"]] = $('<input>').attr({
        'type': 'checkbox'
    }).bind("keypress",function(e) {
        if (e.which == 13) {
            sendAuthForm();
        }
    });

    $('<label>').
        addClass(CONSTS.classes["remember-holder"]).
        appendTo(jqElements['main-block']).
        append(
            rememberInput,
            forTranslate["text"][CONSTS["placeholders"]["remember-input"]] = $('<span>')
        );
    //-------------------------- / Чекбокс "запомнить меня" ---------------------------------
    //-------------------------- Кнопка "Восстановить пароль" -------------------------------
    $("<div>").addClass("dop-button").click(function() {
        authForm.Event.callHandlers("onClickForgot");
    }).appendTo(jqElements['main-block']).text("Восстановить пароль");

    //------------------------- / Кнопка "Восстановить пароль" ------------------------------
    //---------------------------- Кнопка "отправить форму" ---------------------------------
    $('<div>').
        attr({
            'type': 'submit',
            'role': 'button'
        }).
        addClass(CONSTS.classes["submit-button"]).
        appendTo(jqElements['main-block']).
        click(sendAuthForm);
    //-------------------------- / Кнопка "отправить форму" ---------------------------------

    setLanguage();
    this.onInit();
};
