var jqElements = {
    "background": $("<img>").addClass('page-background').appendTo(body)
};

//--- Инициализация фона страницы ---

this._constructor = function() {
    jqElements['background'].load(function() {

        //--- Делаем невидимым, но доступным для вычисления высоты и ширины ---

        jqElements['background'].show().css({
            opacity: 0
        });

        //--- Выравниваем и изменяем мастштаб ---

        iniBackground();

        //--- Отображаем ---
        jqElements['background'].hide().css({
            opacity: ''
        }).fadeIn();

        app.Event.callHandlers("onLoad");
    });

    win.bind('resize', iniBackground);
    this.onInit();
};

this.getImg = function() {
    return jqElements["background"];
};

//--- Подгоняем размер изображения под экран компьютера пользователя ---

var correct = {};
this.getCorrectParams = function() {
    return correct;
};

var iniBackground = function() {

    var winWidth = win.width(),
        winHeight = win.height();

    jqElements['background'].css({
        width: 'auto',
        height: 'auto'
    });

    //--- Высчитываем соотношение сторон ---

    var widthK = winWidth / jqElements['background'].width(),
        heightK = winHeight / jqElements['background'].height();

    //--- Из соотношений сторон определяем растягивание страницы (100% - высота, 100% - ширина)

    if ((widthK > 1 && widthK < heightK) || (widthK < heightK)) {

        //--- 100% высота ---

        jqElements['background'].css({
            height: '100%'
        });
        jqElements['background'].css({
            top: correct.top = 0,
            left: correct.left = -(jqElements['background'].width() - winWidth) / 2
        });
    } else {
        //--- 100% ширина ---
        jqElements['background'].css({
            width: '100%'
        });
        jqElements['background'].css({
            left: correct.left = 0,
            top: correct.top = - (jqElements['background'].height() - winHeight) / 2
        });
    }
    app.Event.callHandlers("onResize");
};
var app = this;
this.setBackground = function(src) {
    jqElements["background"].fadeOut(function() {
        jqElements["background"].attr("src", "").attr("src", src);
    });
};
