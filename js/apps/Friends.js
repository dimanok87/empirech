var jqElements = {
    'main-block': $('<div>').addClass('friends-application')
};

//==================== Управление показом прелоадера ====================

//--- Блок с прелоадером ---
var preLoaderContainer = $("<div>").addClass("absolute-container shop-loader").append(
    $("<div>").addClass("relative-container").append(
        $("<div>").addClass("pre-loader medium").append("<div>")
    )
);

//--- Показываем ---
var showShopPreLoader = function(f) {
    f = f || false;
    preLoaderContainer[f ? "appendTo" : "prependTo"](jqElements["main-block"])
};

//--- Скрываем ---
var hideShopPreLoader = function() {
    preLoaderContainer.detach();
};

//==================== / Управление показом прелоадера ====================

this.getJQElements = function(name) {
    return jqElements[name];
};

this.setFriendsList = function(list) {
    console.log(list);
    hideShopPreLoader();
};


this._constructor = function() {
    showShopPreLoader();
    this.onInit();
};
