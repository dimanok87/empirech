var jqElements = {
    'main-block': $('<div>').addClass('character-application')
};

this.getJQElements = function(name) {
    return jqElements[name];
};

//==================== Управление показом прелоадера ====================

    //--- Блок с прелоадером ---
    var preLoaderContainer = $("<div>").addClass("absolute-container shop-loader").append(
        $("<div>").addClass("relative-container").append(
            $("<div>").addClass("pre-loader medium").append("<div>")
        )
    );

    //--- Показываем ---
    var showShopPreLoader = function(f) {
        f = f || false;
        preLoaderContainer[f ? "appendTo" : "prependTo"](jqElements["main-block"])
    };

    //--- Скрываем ---
    var hideShopPreLoader = function() {
        preLoaderContainer.detach();
    };

//==================== / Управление показом прелоадера ====================

//--- Добавление в желания ---
var app = this;

var addToDesires = function() {
    var products = shopApplication.getSelectedProducts(),
        desItems = [];

    for (var k = 0; k < products.length; k++) {
        var itemParams = products[k]["getParams"]();
        desItems.push({
            id: itemParams["id"],
            count: 1
        });
    }
    addingIndicator.text("+" + desItems.length).hide().appendTo(desButton).show("fast");
    Functions.sendToAPI("api/user/wishes", function() {
        app.Event.callHandlers("onAddToDesires");
        setTimeout(function() {
            addingIndicator.hide("fast", function() {
                addingIndicator.detach();
            })
        }, 2000);
    }, "POST", desItems)
};

var buyProducts = function() {
    if (userBalance < amountCount / 100) {
        return;
    }
    var selectedProducts = shopApplication.getSelectedProducts(), selectedParams = [], usedPersonage;

    for (var k = 0; k < selectedProducts.length; k++) {
        var price = selectedProducts[k].getCartItem().getSelectedPriceItem()["new"],
            used = false,
            id = selectedProducts[k].getParams()["id"];
        selectedProducts[k].unSelectItem();

        if (k == selectedProducts.length - 1) {
            used = true;
            usedPersonage = id;
        }
        selectedParams.push({
            id: id,
            count: price['count'] * 1440,
            use: used
        });
    }
    showShopPreLoader();
    deActivateContent();
    showedContent = false;
    Functions.sendToAPI("api/user/rent?u=" + User["id"], function() {
        app.Event.callHandlers("onChangeBalance");
        loadMyProducts(function() {
            showMyProducts(true);
            hideShopPreLoader();
        });
    }, "POST", {items: selectedParams});
};
// Применить выбранное
var applyMyProduct = function() {
    Functions.sendToAPI("api/user/view/comp?p=" + usedComp.getParams()["id"], function(data) {}, "GET");
};

//================================== Правй блок с кнопками ==================================

//--- Линии для кнопок в правом баре ---
    var linesButton = {
        "line-1": $("<div>").addClass("buttons-line"),
        "line-2": $("<div>").addClass("buttons-line")
    };

//--- Кнопки правого бара магазина ---

    // Перейти к моим продуктам
    var myProductsButton = $("<div>").addClass("nav-shops-button float-left").append(
            $("<span>").addClass("comp-icon")
        ).click(function() {
            showMyProducts();
        }).appendTo(linesButton["line-1"]);

    // Перейти в магазин
    var shopButton = $("<div>").addClass("nav-shops-button float-right").append(
            $("<span>").addClass("shop-icon")
        ).click(function() {
            if (!shopChapterOpened || shopChapterOpened == "showcase") {
                showShowCase();
            } else {
                showCart();
            }
        }).appendTo(linesButton["line-1"]);

    // Применить выбранное
    var okButton = $("<div>").addClass("nav-shops-button").append(
        $("<span>").addClass("ok-icon")
    ).click(function() {
        applyMyProduct();
    }).appendTo(linesButton["line-2"]);

    var shopNavigation = $("<div>").addClass('nav-shops').append(linesButton["line-1"], linesButton["line-2"]);
    shopNavigation.appendTo(jqElements["main-block"]);
//================================= / Правй блок с кнопками =================================

//==================================== Верхний зелёный бар ==================================

    //--- Верхний бар магазина ---
    var topNavigation = $("<div>").addClass("shop-top-panel");

    //--- Блок для баланса пользователя ---
    var myBalance = $("<span>").text(0);

    //--- Блок для стоимости выбранных товаров ---
    var fullAmount = $("<span>").text(0);

    //--- Кнопка поиска ---
    var searchButton = $("<div>").addClass("nav-shops-button").append(
        $("<span>").addClass("search-icon")
    );

    //--- Кнопка "перейти в корзину" / "купить" ---
    var buyButton = $("<div>").addClass("nav-shops-button").append(
            $("<span>").addClass("buy-icon")
        ).click(function() {
            if (showedContent == "showcase") {
                showCart();
            } else {
                buyProducts();
            }
        }
    );

    var addingIndicator = $("<div>").addClass("adding-indicator");
    //--- Кнопка "Добавить в желания" ---
    var desButton = $("<div>").addClass("nav-shops-button top-index").append(
            $("<span>").addClass("desires-icon")
        ).click(function() {
            addToDesires();
        });

    var shopNav = $("<div>")


    topNavigation.append(
        $("<span>").addClass("my-money").append(myBalance, $("<span>").addClass("money-icon")),
        shopNav.append(
            $("<span>").addClass("amount-money").append(fullAmount, $("<span>").addClass("money-icon")),
            searchButton
        )
    ).appendTo(jqElements["main-block"]);

var backButton = $("<div>").addClass("back-button shop-navigation").click(function() {
    showShowCase();
});

var userBalance = 0;
this.setUserBalance = function(balance) {
    myBalance.html(userBalance = balance);
};

//=================================== / Верхний зелёный бар =================================

var shopApplication = false;

var amountCount = 0, oldAmount;

var setAmount = function(range) {
    oldAmount = amountCount;
    amountCount+= range;
    fullAmount.html(amountCount / 100);

    if (oldAmount && !amountCount) {
        buyButton.detach();
        desButton.detach();
    } else if (!oldAmount && amountCount) {
        searchButton.before(buyButton, desButton);
    }
};


//--- Функция установки магазина ---
this.setShopApplication = function(shop) {
    shopApplication = shop;
    shopProducts = shop.getProducts()["byId"];
    shopApplication.setHandlers({
        addSelection: function(item) {
            setAmount(item.getCartItem().getSelectedPriceItem()["new"]["price"]);
        },
        delSelection: function(item) {
            setAmount(-item.getCartItem().getSelectedPriceItem()["new"]["price"]);
            if (!shopApplication.getSelectedProducts().length && showedContent == "cart") {
                showShowCase();
            }
        },
        changeSelection: function(item) {
            var priceItem = item.getCartItem().getSelectedPriceItem();
            setAmount(priceItem["new"]["price"] - priceItem["old"]["price"]);
        }
    });
};

var showedContent = false,
    showedBlock = false,
    showedShowcase = false,
    shopChapterOpened = false;

//--- Снимаем класс с активной кнопочки и убирем контент ---
var deActivateContent = function() {
    showedBlock ? showedBlock.detach() : false;
    switch (showedContent) {
        case "cart":
            shopButton.removeClass("active");
            backButton.detach();
            break;
        case "showcase":
            shopButton.removeClass("active");
            break;
        case "myproducts":
            myProductsButton.removeClass("active");
            shopNavigation.after(topNavigation);
            linesButton["line-2"].detach();
            break;
    }
};


var showShowCase = function() {
    if (showedContent == "showcase") return;

    deActivateContent();
    shopButton.addClass("active");

    shopChapterOpened = showedContent = "showcase";
    showedShowcase = shopApplication.getShowCase();
    jqElements["main-block"].append(showedBlock = showedShowcase.getContainer());
    showedShowcase.restoreScroll();
    showedShowcase.iniProductsImages();
};


var showCart = function() {
    if (showedContent == "cart") return;
    var Cart = shopApplication.getCart().getBlockCart();

    deActivateContent();
    shopButton.addClass("active");

    jqElements["main-block"].append(showedBlock = Cart, backButton);
    shopChapterOpened = showedContent = "cart";
};


var showMyProducts = function(fl) {
    fl = fl || false;
    if (showedContent == "myproducts") return;
    var createContent = function() {
        shopNavigation.append(linesButton["line-2"]);
        topNavigation.detach();
        showedContent = "myproducts";
        myProductsButton.addClass("active");
        jqElements["main-block"].append(showedBlock = myProductsList);
    };
    if (showedContent)
        deActivateContent();
    if (!fl) {
        showShopPreLoader();
        loadMyProducts(function() {
            hideShopPreLoader();
            createContent();
        });
    } else {
        createContent();
    }
};



//--- Мои продукты ---
var shopProducts = false;
var myProducts = {};
var myActiveProducts = [];
var myProductsList = $("<div>").addClass("my-products-content");
var usedComp;

var MyProductItem = function(params) {
    this.setParams = function(a, b) {
        params[a] = b;
        switch (a) {
            case "inUse":
                if (!b && radioButton.get(0).checked) {
                    radioButton.get(0).checked = false;
                } else if (b) {
                    radioButton.get(0).true = true;
                    usedComp = itemProduct;
                }
                break;
        }
    };
    var itemBlock = $("<label>").addClass("product-element comp-element");
    var productContainer = $("<div>").addClass("product-element-container").appendTo(itemBlock);
    var loader = $("<div>").
        addClass("pre-loader small animated").
        append("<div>").
        appendTo(itemBlock);
    var productImg = new Image();
    var radioButton = $("<input>").attr({
        type: "radio",
        name: "my-comp"
    }).change(function() {
            usedComp = itemProduct;
        });

    var itemProduct = this;
    if (params["inUse"]) {
        usedComp = this;
        radioButton.get(0).checked = true;
    }

    this.getParams = function() {
        return params;
    };

    productImg.onload = function() {
        loader.empty().remove();
        productContainer.append(
            productImg,
            $("<span>").text(product["name"]).addClass("name-product"),
            $("<div>").addClass("label-checkbox").append(
                radioButton, $("<span>")
            )
        );
    };

    var product = shopProducts["element-" + params["product"]["id"]];
    productImg.src = product["views"]["size-2"]["img"];
    this.getItemBlock = function() {
        return itemBlock;
    };
};

//=== Загрузка моих товаров ===
var loadMyProducts = function(cb) {
    usedComp = false;
    cb = cb || false;
    Functions.sendToAPI("api/user/p/comp", function(data) {
        for (var i in myProducts) {
            myProducts[i].getItemBlock().detach();
        }
        myActiveProducts = data;
        for (var k = 0; k < data.length; k++) {
            var productId = "product-" + data[k].product.id;
            if (!myProducts[productId]) {
                myProducts[productId] = new MyProductItem(data[k]);
            } else {
                myProducts[productId].setParams("id", data[k]["id"]);
                myProducts[productId].setParams("inUse", data[k]["inUse"]);
            }
            myProductsList.append(myProducts[productId].getItemBlock());
        }
        if (!usedComp && data.length) {
            usedComp = myProducts["product-" + data[0]["product"]["id"]];
            usedComp.setParams("inUse", true);
        } else if (!data.length) {
            okButton.detach();
        }
        if (data.length) {
            okButton.appendTo(linesButton["line-3"]);
        }
        cb ? cb(data) : false;
    });
};


//=== Обработчик отображения приложения ===
this.onShow = function() {
    switch (showedContent) {
        case "showcase":
            showedShowcase.restoreScroll();
            showedShowcase.iniProductsImages();
            break;
        case "myproducts":
            deActivateContent();
            showedContent = false;
            showMyProducts();
            break;
    }
};

var User = false;
this._constructor = function(params) {
    User = params["user"];
    showShopPreLoader();
    loadMyProducts(function() {
        if (!myActiveProducts.length) {
            showShowCase();
        } else {
            showMyProducts(true);
        }
        hideShopPreLoader();
    });
    this.onInit();
};
