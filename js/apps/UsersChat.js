window["chatUser"] = {};
var personageFactory = false;
this.setPersonageFactory = function(factory) {
    personageFactory = factory;
};

var RESOURCES = Functions.getSources();

var getPersParams = function(userData, oldUserData) {
    var viewUserData = userData["view"],
        persData = {},
        gender = userData["male"] ? "male" : "female";

    oldUserData = oldUserData || {};
    if (!viewUserData["vm"] || (viewUserData["vm"] == 1 && !viewUserData["w"].length)) {
        viewUserData["vm"] = 0;
    }
    switch (viewUserData["svm"]) {
        case 0:
            switch (viewUserData["vm"]) {
                case 0:
                    persData["bodies"] = viewUserData["body"]["id"];
                    persData["heads"] = viewUserData["head"]["id"];
                    break;
                case 1:
                    persData["models"] = viewUserData["model"] ? viewUserData["model"]["id"] : RESOURCES["models"][gender]["byInd"][0]["id"];
                    for (var i = 0; i < viewUserData["w"].length; i++) {
                        var sourceId = viewUserData["w"][i]["id"];
                        var source = RESOURCES["clothes"]["all"]["element-" + sourceId];
                        persData["clothes-" + source["type"]] = sourceId;
                    }
                    break;
                case 2:
                    persData["pers"] = viewUserData["w"][0]["id"];
                    break;
            }
            break;
        case 1:
            persData["magic"] = viewUserData["smodel"]["id"];
            break;
    }

    if (viewUserData["comp0"]) {
        persData["comp"] = viewUserData["comp0"]["id"];
    }
    for (var i in oldUserData) {
        persData[i] = persData[i] || false;
    }
    return persData;
};

var createPersonage = function(userData) {
    var gender = userData["male"] ? "male" : "female";
    var pers = {
        "gender": gender,
        "size": 2,
        "personage": getPersParams(userData),
        "loader": $('<div class="min-loader-pers pers-gender-' + gender + '">'),
        "userName": userData["nick"]
    };
    return personageFactory(pers);
};


var usersLoading = {};

//---- Загружаем данные одного пользователя ----
this.loadUserData = function(userId, callback) {
    var eventName = "onLoadUser-" + userId;
    _this.Event.addHandlers(eventName, callback);
    if (usersLoading["user-" + userId]) return;
    usersLoading["user-" + userId] = true;

    Functions.sendToAPI("api/user/" + userId, function(data) {
        _this.Event.callHandlers(eventName, data);
        _this.Event.removeHandlers(eventName);
        usersLoading["user-" + userId] = false;
    });
};







var _this = this;

this.updateUser = function(data) {
    var userId = "user-" + data["user"]["id"];
    if (window["chatUser"][userId]) {
        var oldData = window["chatUser"][userId].getData();
        oldData["nick"] = data["user"]["nick"];
        oldData["view"] = {
            svm: data["svm"],
            vm: data["vm"],
            smodel: data["smodel"],
            body: data["body"],
            comp0: data["comp0"],
            head: data["head"],
            model: data["model"],
            photo: data["photo"],
            w: data["w"]
        };
        window["chatUser"][userId].updatePersonage(oldData);
        _this.Event.callHandlers("onUpdateUser", oldData);
    }
};
this.getUser = function(id) {
    var userId = "user-" + id;
    return window["chatUser"][userId] ? window["chatUser"][userId].getData() : false;
};
this.User = function(data) {
    var id =                data["user"]["id"];
    var userId =            "user-" + id;
    var Event =             this.Event;

    if (!window["chatUser"][userId] && data["type"] == "unavailable") return;

    window["chatUser"][userId] ? window["chatUser"][userId]["updateData"](data) : new (function () {
        var Personage = false,
            itemPersonage = $("<li>"),
            _userData = false;

        window["chatUser"][userId] = this;

        var createUserModel = function(userData) {
            _userData = userData;
            Personage = createPersonage(userData);
            if (!data["type"]) {
                itemPersonage.append(Personage.getContainer());
                Personage.load(function() {
                    setTimeout(Event.callHandlers("onLoadPersonage"), 200);
                    Personage.setLoader(false);
                });
            }
        };
        _this.loadUserData(id, function(userData) {
            createUserModel(userData);
        });
        this.getData = function() {
            return _userData;
        };
        this.updatePersonage = function(newdata) {
            var oldModelPers = Personage.getData();
            Personage.setData(getPersParams(newdata, oldModelPers));
            Personage.setNick(newdata["nick"]);
        };
        this.updateData = function(data) {
            switch (data["type"]) {
                case "unavailable":
                    Event.callHandlers("onRemovePersonage", itemPersonage);
                    itemPersonage.empty().remove();
                    delete window["chatUser"][userId];
                    break;
            }
        };
        Event.callHandlers("onCreatePersonage", itemPersonage);
    })();
};

this._constructor = function() {
    this.onInit();
};
