var jqElements = {
    "main-block": $('<form>').addClass('start-form forgot-form')
};

this.getJQElements = function(name) {
    return jqElements[name];
};

var CONSTS = {
    "classes": {
        "header-form": "header-form",
        "email-input": "with-icon email-input",
        "input-holder": "input-holder",
        "back-button": "back-button",
        "submit-button": "submit-button"
    },
    "placeholders": {
        "email-input": "email"
    },
    "title-form": "forgot_pass",
    "names-fields": {
        "email": "email",
        "captcha": "ctext",
        "captchak": "ckey"
    }
};

var forTranslate =  {
    "placeholder": {},
    "text": {}
};

var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k in forTranslate[i]) {
            switch(i) {
                case "placeholder":
                    forTranslate[i][k].attr("placeholder", TRANSLATES[k] || k);
                    break;
                case "text":
                    forTranslate[i][k].text(TRANSLATES[k] || k);
                    break;
            }
        }
    }
};

var inputs = {};
var authForm = this;
var Exit = function() {
    authForm.Event.callHandlers("onExit");
};

jqElements["main-block"].submit(function(e) {
    e.preventDefault();
    sendForgotForm();
});

var sendForgotForm = function() {
    var data = {};
    for (var i in inputs) {
        data[i] = inputs[i].val();
    }
    authForm.Event.callHandlers("onSendForm", data);
};

this._constructor = function() {
    //------------------------------ Заголовок формы ---------------------------------
    var headerForm = $('<div>').
        addClass(CONSTS.classes["header-form"]).
        appendTo(jqElements['main-block']);

    forTranslate["text"][CONSTS["title-form"]] = $("<span>").prependTo(headerForm);

    $('<div>').
        addClass(CONSTS.classes["back-button"]).
        prependTo(headerForm).
        click(Exit);
    //---------------------------- /Заголовок формы ----------------------------------
    //---------------------------- Поле ввода логина ---------------------------------
    var loginInput = inputs[CONSTS["names-fields"]["email"]] = $('<input>').
        addClass(CONSTS.classes["email-input"]).attr({
            'type': 'text'
        });
    forTranslate["placeholder"][CONSTS["placeholders"]["email-input"]] = loginInput;

    $('<div>').
        addClass(CONSTS.classes["input-holder"]).
        append(loginInput).
        appendTo(jqElements['main-block']);
    //-------------------------- / Поле ввода логина ---------------------------------
    var captchaLoader = $("<div>").addClass("pre-loader micro").append("<div>");
    //---- Поле для Captcha ----
    var updateCaptcha = function() {
        captchaContainer.append(captchaLoader);
        Functions.getCaptcha(function(data) {
            captchaImage.src = data["image"];
            captchaInputKey.val(data["CKey"]);
            captchaLoader.detach();
        });
    };
    var captchaInput = inputs[CONSTS["names-fields"]["captcha"]] = $('<input>').attr({'type': 'text'});
    var captchaInputKey = inputs[CONSTS["names-fields"]["captchak"]] = $('<input>').attr({'type': 'hidden'});
    var captchaImage = new Image();
    $(captchaImage).click(function() {updateCaptcha(); });

    var captchaContainer = $('<div>').
        addClass(CONSTS.classes["input-holder"] + " captcha-field").
        append(captchaImage, captchaInput, captchaInputKey).
        appendTo(jqElements['main-block']);
    updateCaptcha();

    //--- / Поле для Captcha ---
    //---------------------------- Кнопка "отправить форму" ---------------------------------
    $('<div>').attr({
        'type': 'submit',
        'role': 'button'
    }).
    addClass(CONSTS.classes["submit-button"]).
    appendTo(jqElements['main-block']).
    click(sendForgotForm);

    //-------------------------- / Кнопка "отправить форму" ---------------------------------

    setLanguage();
    this.onInit();
};
