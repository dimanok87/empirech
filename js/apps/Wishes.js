var RESOURCES = Functions.getSources();

var jqElements = {
    'main-block': $('<div>').addClass('wishes-application')
};

this.getJQElements = function(name) {
    return jqElements[name];
};

//------------------------ Прелоадер ------------------------

var preLoaderContainer = $("<div>").addClass("absolute-container preload-bg").append(
    $("<div>").addClass("relative-container").append(
        $("<div>").addClass("pre-loader medium").append("<div>")
    )
);

var showPreLoader = this.showPreLoader = function() {
    preLoaderContainer.appendTo(jqElements["main-block"])
};

var hidePreLoader = this.hidePreLoader = function() {
    preLoaderContainer.detach();
};

//------------------------ / Прелоадер ------------------------

//------------------------ Работа с языками ------------------------

var forTranslate = {};

var addToTranslate = function(label, element) {
    forTranslate[label] = (forTranslate[label] || []);
    forTranslate[label].push(element.text(TRANSLATES[label] || label));
    return element;
};

var setLanguage = this.setLanguage = function() {
    for (var i in forTranslate) {
        for (var k = 0; k < forTranslate[i].length; k++) {
            forTranslate[i][k].text(TRANSLATES[i] || i);
        }
    }
};

//------------------------ / Работа с языками ------------------------
//--- Устанавливаем баланс везде, где возможно ---
this.setUserBalance = function(b) {
    for (var i in wishesLists) {
        wishesLists[i].setBalance(b);
    }
};

var wishesMenu = false,
    sortedWishes = {};

var wishesLists = {};

var productItem = function(opts) {
    var params = opts["product"];
    var item = {};

    var addClass = params["source"] == "clothes" ? "comp" : params["source"];

    item["preview"] = $("<div>").addClass("product-element").addClass(addClass + "-element");
    var previewItem = new Image();
    var imgContainer = $("<div>").addClass("product-element-container").append(previewItem);
    var size = 2;
    switch (params["source"]) {
        case "gifts":
            size = 1;
            break;
        case "clothes":
            size = 3;
            break;
    }
    item["preview"].append(
        $("<div>").addClass("close-button").click(function() {
            deleteWishes(opts);
            item["preview"].detach();
        })
    );
    previewItem.src = params["views"]["size-" + size]["img"]
    item["preview"].append(imgContainer);
    return item;
};

//--- Удаление из списка желаний ---
var deleteWishes = function(opts) {
    Functions.sendToAPI("api/user/wishes/delete", function(data) {}, "POST", [{
        id: opts["id"],
        count: 1
    }]);
};

//--- Создание списка желаний для одной категории товаров ---
var wishesTypeContent = function(sourceType) {

    var fullContent = $("<div>").addClass("wishes-content");
    wishesLists[sourceType] = this;

    var wishesList = $("<div>").addClass("wishes-list type-" + sourceType);
    var wishesShowcase = false;

    var myBalance = $("<span>").text(0);
    var fullAmount = $("<span>").text(0);

    var buyButton = $("<div>").addClass("nav-shops-button").append($("<span>").addClass("buy-icon")).click(function() {

    });
    var desButton = $("<div>").addClass("nav-shops-button").append($("<span>").addClass("desires-icon")).click(function() {
        setAppContent(wishesMenu);
    });

    var topBar = $("<div>").addClass("shop-top-panel").append(
        $("<span>").addClass("my-money").append(
            myBalance,
            $("<span>").addClass("money-icon")
        ),
        $("<span>").addClass("amount-money").append(
            fullAmount,
            $("<span>").addClass("money-icon")
        ),
        desButton
    ).appendTo(fullContent);

    var activeChapter = false;

    var items = [];

    if (sourceType != "money" && sourceType != "authority") {
        wishesShowcase = $("<div>").addClass("wishes-list");
        if (sortedWishes[sourceType]) {
            for (var i = 0; i < sortedWishes[sourceType]["length"]; i++) {
                var item = new productItem(sortedWishes[sourceType][i]);
                items.push(item);
                wishesList.append(item["preview"]);
            }
        }
    } else {
        topBar.append(buyButton);
        switch (sourceType) {
            case "money":
                wishesList.append("<h1>MONEY</h1>");
                break;
            case "authority":
                wishesList.append("<h1>AUTHORITY</h1>");
                break;
        }
    }

    //--- Устанавливаем общую стоимость покупки ---
    var setAmount = function(i) {
        fullAmount.text(i);
    };

    //--- Устанавливаем витрину желаний ---
    var setShowcaseContent = function() {
        if (!wishesList.is(activeChapter)) {
            if (activeChapter) {
                activeChapter.detach();
            }
            activeChapter = wishesList.appendTo(fullContent);
        }
    };

    //--- Устанавливаем баланс пользователя ---                     +
    this.setBalance = function(i) {
        myBalance.text(i);
    };

    //--- Устанавливаем контент списка желаний ---                  +
    this.setWishesContent = function() {
        setAppContent(fullContent);
        setShowcaseContent();
    };

    //--- Добавляем товары в список желаний ---
    this.addToList = function(products) {

    };

    //--- Удаляем товары из списка желаний ---
    this.delFromList = function() {

    };
};

var oldContent = false;

//--- Устанавливаем контент ---
var setAppContent = function(newContent) {
    if (!newContent.is(oldContent)) {
        oldContent ? oldContent.detach() : false;
        jqElements["main-block"].append(oldContent = newContent);
    }
};

//=================================== Работа с меню ===================================

//--- Создаём пункт меню ---
var createItemMenu = function (options) {
    new wishesTypeContent(options["source"]);
    return $('<li>').
        addClass("categories-item " + options['label']).append($("<div>").append(
            addToTranslate(options['title'], $("<span>"))
        )).click(function() {
            wishesLists[options["source"]]["setWishesContent"]();
        }
    );
};

//--- Создаём меню ---
var createMenu = function(params) {
    wishesMenu = $("<div>").addClass("categories-list");
    var menu = $('<ul>').addClass("categories-menu wishes-menu").appendTo(wishesMenu);
    for (var k = 0; k < params.length; k++) {
        menu.append(createItemMenu(params[k]));
    }
};

var wishesApp = this;

//--- Загружаем конфиг меню ---
var loadMenu = function() {
    showPreLoader();
    $.ajax({
        url: '/configs/Wishes.json',
        dataType: 'JSON',
        success: function(data) {
            createMenu(data);
            wishesApp.Event.callHandlers("onCreateContent");
            setAppContent(wishesMenu);
            hidePreLoader();
        }
    });
};

//=================================== / Работа с меню ===================================

//--- Сортируем желания по категориям ---
var sortWishes = function(wishes) {
    for (var k = 0; k < wishes.length; k++) {
        var product = wishes[k]["product"],
            source = product["source"],
            arrSource = (sortedWishes[source] = sortedWishes[source] || []);

        for (var i = 0; i < arrSource.length; i++) {

        }
        arrSource.push(wishes[k]);
    }
};

//--- Загружаем список желаний ---
var getWishesList = function(cb) {
    showPreLoader();
    Functions.sendToAPI("api/user/wishes", function(data) {
        for (var k = 0; k < data.length; k++) {
            if (RESOURCES["products"]["product-" + data[k]["product"]["id"]]) {
                data[k]["product"] = RESOURCES["products"]["product-" + data[k]["product"]["id"]];
                data[k]["in_wishes"] = true;
            }
        }
        sortWishes(data);
        hidePreLoader();
        cb();
    });
};

this.updateList = function() {

};

this._constructor = function() {
    getWishesList(function() {
        loadMenu();
    });
    this.onInit();
};
