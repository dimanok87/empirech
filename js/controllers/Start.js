//--- Applications factory --
var appsFactory = SYSTEM["Application"];

//--- Объект контента ---
var ContentModel = appsFactory("Content");

//--- jQuery-контейнер контента ---
var ContentContainer = ContentModel["getJQElements"]("main-block").empty();

//--- Объект для работы с фоном приложения ---
var backgroundAppModel = appsFactory("Background");

//--- Стартовое меню ---
var startMenu = appsFactory("StartMenu");

//--- Устанавливаем фон приложения ---
backgroundAppModel.Event.addHandlers({
    onInit: function(background) {
        background["setBackground"]("images/backgrounds/login.jpg");
    }
});

//--- Переменные для хранения объектов jQuery ---
var startMenuJQ, openedApplication;

//--- Скрываем открытое приложение ---
var hideOpenedApplication = function() {
    if (!openedApplication) return;
    var opened = openedApplication;
    opened.animate({
        'left': '-50%'
    }, function() {
        opened.detach();
        opened= false;
    });
};

//--- Убираем стартовое меню ---
var hideStartMenu = function() {
    startMenuJQ.animate({
        top: '150%'
    }, function() {
        startMenuJQ.detach();
    });
};

//--- Показываем вновь открытое приложение ---
var showOpenedApplication = function() {
    hideStartMenu();
    openedApplication.css({
        'left': '150%'
    }).appendTo(ContentContainer).animate({
        'left': '50%'
    });
};

//--- Показываем стартовое меню ---
var showStartMenu = function() {
    hideOpenedApplication();
    startMenuJQ.css({
        top: '-50%'
    }).animate({
        top: '50%'
    }).appendTo(ContentContainer);
};

//--- Персонаж формы регистрации ---
var regPersonage = function(gender, callback) {
    var pers = false;
    appsFactory("Personages").Event.addHandlers({
        onInit: function(personagesFactory) {
            pers = personagesFactory["newPersonage"]({
                "gender": gender,
                "size": 3,
                "personage": {
                    "heads": "random",
                    "bodies": "random"
                },
                "loader": $("<div>").addClass("pre-loader medium").append("<div>")
            });
            pers.addControl({
                controls: {
                    heads: true,
                    bodies: true,
                    random: true
                }
            });
            pers.load();
            callback(pers);
        }
    });
    this.setGender = function(gender) {
        pers.setGender(gender);
        pers.setData({
            "heads": "random",
            "bodies": "random"
        });
    };
    this.getElement = function(n) {
        return pers.getElement(n);
    };
};
//--- Инициализация формы регистрации --
var regFormInitialize = function(app) {
    //--- Создание персонажа для формы регистрации и создание обработчиков для неё ---
    var createRegPersonage = function() {
        var startedGender = ["male", "female"][Math.round(Math.random())],
            personage = new regPersonage(startedGender, function(pers) {
                app["getSecondBlock"]().append(pers.getContainer());
            });
        app.setGender(startedGender);
        app.Event.addHandlers({
            onChangeGender: function(gender) {
                personage.setGender(gender)
            },
            onSendForm: function(data) {
                for (var i in data) {
                    if (i == "male") {
                        data[i] = data[i] == 1;
                    }
                }
                $.extend(data, {
                    "body": personage.getElement('bodies'),
                    "head": personage.getElement('heads'),
                    "lang": Functions.getLanguage()
                });
                Functions.sendToAPI("api/register", function(responseRegister) {
                    if (responseRegister.error) return;
                    var request = {
                        "login": data["name"],
                        "pwd": data["pswd"]
                    };
                    Functions.authUser(request, function(responseData) {
                        if (responseData.error) return;
                        Functions.sendToAPI('api/user/verify', function() {
                            onLogin(responseData);
                        });
                    });
                }, "POST", data)
            }
        });
    };
    createRegPersonage();
};
//--- Инициализация формы восстановления пароля ---
var forgotPassFormInitialize = function(app) {
    app.Event.addHandlers({
        onSendForm: function(responseData) {
            Functions.sendToAPI("api/askpass", function(data) {}, "POST", responseData, true)
        }
    });
};
//--- Показываем/скрываем выбор языка ---
var triggerLanguageForm = function(app, params) {
    if (params.selected) {
        params.item.addClass("selected");
        app["getJQElements"]("main-block").appendTo(params.item);
    } else {
        params.item.removeClass("selected");
        app["getJQElements"]("main-block").detach();
    }
};
//--- Инициализация формы смены языка ---
var languageFormInitialize = function(app, params) {
    triggerLanguageForm(app, params);
    app.Event.addHandlers({
        onChange: function(code) {
            Functions.loadLanguage(code, function() {
                $.cookie("lng", code);
                for (var i in startedApplications) {
                    if (startedApplications[i] && startedApplications[i]["setLanguage"]) {
                        startedApplications[i]["setLanguage"]();
                    }
                }
            });
        }
    });
};
var startedApplications = [];
//--- Завершении входа в систему ---
var onLogin = function(data) {
    appsFactory("User").Event.addHandlers({
        onInit: function(user) {
            user["setUser"](data);
            SYSTEM["Controller"]("Empirech");
        }
    });
    for (var i = 0; i < startedApplications.length; i++) {
        startedApplications[i].Close();
    }
};
//--- Инициализация формы авторизации ---
var authFormInitialize = function(app) {
    app.Event.addHandlers({
        onSendForm: function(responseData) {
            Functions.authUser(responseData, function(data) {
                if (data.error) return;
                onLogin(data);
            });
        },
        onClickForgot: function() {
            showForgotApp();
        }
    });
};

var showForgotApp = function() {
    var app = appsFactory("ForgotPassForm");
    if (app.isStarted && app.isStarted()) {
        hideOpenedApplication();
        openedApplication = app["getJQElements"]("main-block");
        showOpenedApplication();
    } else {
        app.Event.addHandlers({
            onInit: function(application) {
                hideOpenedApplication();
                openedApplication = application["getJQElements"]("main-block");
                forgotPassFormInitialize(application);
                showOpenedApplication();
            },
            onExit: function() {
                showauthForm();
            }
        });
    }
};

var showauthForm = function() {
    var app = appsFactory("AuthForm");
    hideOpenedApplication();
    openedApplication = app["getJQElements"]("main-block");
    showOpenedApplication();
};

//--- Инициализация меню ---
startMenu.Event.addHandlers({
    onInit: function(startMenu) {
        startMenuJQ = startMenu["getJQElements"]("main-block");
        startedApplications.push(startMenu);
        showStartMenu();
    },
    onSelectItem: function(itemParams) {
        switch (itemParams["type"]) {
            case "link":
                window.location = itemParams["href"];
                break;
            case "application":
                if (!itemParams["application"]) return;
                var app = appsFactory(itemParams["application"]);
                switch (itemParams["application"]) {
                    case "RegForm":
                    case "AuthForm":
                        if (app.isStarted && app.isStarted()) {
                            openedApplication = app["getJQElements"]("main-block");
                            showOpenedApplication();
                        }
                        break;
                    case "StartLanguage":
                        if (app.isStarted && app.isStarted()) {
                            triggerLanguageForm(app, itemParams);
                        }
                        break;
                }
                if (app.isStarted && app.isStarted()) {
                    return;
                }
                app.Event.addHandlers({
                    onInit: function(application) {
                        openedApplication = application["getJQElements"]("main-block");
                        startedApplications.push(application);
                        switch (itemParams["application"]) {
                            case "RegForm":
                                regFormInitialize(application);
                                showOpenedApplication();
                                break;
                            case "AuthForm":
                                authFormInitialize(application);
                                showOpenedApplication();
                                break;
                            case "StartLanguage":
                                languageFormInitialize(application, itemParams);
                                break;
                        }
                    },
                    onExit: showStartMenu
                });
                break;
        }
    }
});
