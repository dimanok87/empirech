var priceList = {
    "authority": [
        {
            "count": 1,
            "price": 500
        },
        {
            "count": 2,
            "price": 1000
        },
        {
            "count": 3,
            "price": 1500
        },
        {
            "count": 4,
            "price": 2000
        },
        {
            "count": 5,
            "price": 2500
        }
    ],
    "country": [
        {
            price: 1
        }
    ],
    "country-backgrounds": [
        {
            "count": 30,
            "price": 50,
            "label": "days"
        },
        {
            "count": 90,
            "price": 150,
            "label": "days"
        },
        {
            "count": 180,
            "price": 300,
            "label": "days"
        },
        {
            "count": 360,
            "price": 600,
            "label": "days"
        }
    ],
    "rank": [
        {
            "count": 1,
            "price": 300
        },
        {
            "count": 2,
            "price": 600
        },
        {
            "count": 3,
            "price": 900
        },
        {
            "count": 4,
            "price": 1200
        },
        {
            "count": 5,
            "price": 1500
        }
    ],
    "gifts": [
        {
            "price": 120
        }
    ],
    "clothes": [
        {
            "count": 1,
            "price": 5,
            "label": "day"
        },
        {
            "count": 3,
            "price": 15,
            "label": "day-s"
        },
        {
            "count": 7,
            "price": 35,
            "label": "days"
        },
        {
            "count": 14,
            "price": 70,
            "label": "days"
        }
    ],
    "pers": [
        {
            "count": 1,
            "price": 10,
            "label": "day"
        },
        {
            "count": 3,
            "price": 30,
            "label": "day-s"
        },
        {
            "count": 7,
            "price": 70,
            "label": "days"
        },
        {
            "count": 14,
            "price": 140,
            "label": "days"
        }
    ],
    "comp": [
        {
            "count": 1,
            "price": 15,
            "label": "day"
        },
        {
            "count": 3,
            "price": 45,
            "label": "day-s"
        },
        {
            "count": 7,
            "price": 105,
            "label": "days"
        },
        {
            "count": 14,
            "price": 210,
            "label": "days"
        }
    ],
    "avatar-elements": [
        {
            price: 30
        }
    ]
};

var appsFactory = SYSTEM["Application"],
    userModel = appsFactory("User"),
    selfUser = userModel["getUser"](),
    RESOURCES = Functions.getSources(),
    startedApplications = {},
    balance = 0,
    balanceInLoading = false;
var getUserBalance = function() {
    if (balanceInLoading) return balance;
    balanceInLoading = true;
    Functions.sendToAPI("api/user/acc", function(data) {
        balanceInLoading = false;
        balance = data["balance"];
        setUserBalance();
    });
    return balance;
};
var setUserBalance = function() {
    for (var k in startedApplications) {
        if (startedApplications[k] && startedApplications[k]["setUserBalance"]) {
            startedApplications[k]["setUserBalance"](balance);
        }
    }
};
var startApps = [
    "XMPPConnector",
    "Menu",
    "UserPanel",
    "CountryPreview",
    "TopBar",
    "Chat",
    "Shop",
    "Wishes",
    "UsersChat"
];
var ContentApp = appsFactory("Content");
var BackgroundApp = appsFactory("Background");
var PersonagesApp = appsFactory("Personages");
var backgroundImg = BackgroundApp["getImg"]();
var backgroundContainer = $("<div>").addClass("bg-app-container").appendTo(
    ContentApp["getJQElements"]('application-wrapper')
);
var appContainerBg = new Image();
var correctBgImage = function() {
    appContainerBg.width = backgroundImg.width();
    appContainerBg.height = backgroundImg.height();
    $(appContainerBg).css(BackgroundApp["getCorrectParams"]());
    $(appContainerBg).css("margin-left", - backgroundContainer.offset()["left"]);
};
BackgroundApp.Event.addHandlers({
    onResize: correctBgImage,
    onLoad: function() {
        appContainerBg["onload"] = function() {
            backgroundContainer.append(
                $(appContainerBg).addClass("page-background")
            );
            correctBgImage();
        };
        appContainerBg.src = "images/backgrounds/onsystem-blur.jpg";
    }
});
var appsOptions = {
    Menu: {
        "more-container":  ContentApp["getJQElements"]('application-wrapper')
    },
    Presents: {
        user: selfUser
    },
    Outfit: {
        user: selfUser
    },
    Character: {
        user: selfUser
    },
    Transport: {
        user: selfUser
    },
    Avatar: {
        user: selfUser,
        personageFactory: PersonagesApp["newPersonage"],
        price: priceList["avatar-elements"][0]["price"]
    },
    Authority: {
        user: selfUser,
        price: priceList["authority"]
    }
};
var startApp = function(appName, callback) {
    if (startedApplications[appName]) {
        callback(startedApplications[appName]);
        return true;
    }
    var app = appsFactory(appName, appsOptions[appName] || false);
    startedApplications[appName] = app;
    app.Event.addHandlers({
        onInit: function(app) {
            callback(startedApplications[appName] = app);
        }
    });
    return false;
};
var loadBasedApplications = function() {
    var loadedApps = 0;
    for (var k = 0; k < startApps.length; k++) {
        startApp(startApps[k], function() {
            loadedApps++;
            if (loadedApps == startApps.length) {
                createDisplaying();
            }
        });
    }
};
loadBasedApplications();
var createDisplaying = function() {
    ContentApp["getJQElements"]("main-block").empty();
    ContentApp["createApplicationContent"]();
    BackgroundApp["setBackground"]("images/backgrounds/onsystem.jpg");
    ContentApp["getJQElements"]('application-wrapper').prepend(
        startedApplications["UserPanel"]["getJQElements"]("main-block")
    );
    startedApplications["UserPanel"]["setUserData"]({
        "user_name": selfUser["nick"],
        "gender": selfUser["male"] ? "male" : "female",
        "user_rank": 0,
        "user_money": getUserBalance(),
        "user_avatar": selfUser.view.photo ?
            _CONSTS_.UserPicPath.replace(/\{sz\}/, 4).replace(/\{userId\}/, selfUser.id) + '?_' + (new Date()).getTime() :
            false
    });
    ContentApp["getJQElements"]('application-wrapper').append(
        startedApplications["TopBar"]["getJQElements"]("main-block")
    );
    ContentApp["getJQElements"]('application-wrapper').prepend(
        startedApplications["Menu"]["getJQElements"]("main-block")
    );
    TopBarInitialize();
    messengerIni();
    countryInitialize();
    chatInitialize();
    showChat();
    XMPPConnectorInitialize();
    MainMenuInitialize();
};
var TopBarInitialize = function() {
    ContentApp["getJQElements"]('application-wrapper').append(
        startedApplications["TopBar"]["getJQElements"]("main-block")
    );
    var userMenuButton = $("<div>").click(function() {
        UserMenuInitialize(userMenuButton);
    });
    startedApplications["TopBar"]["addElement"](userMenuButton, 'user-menu');
    var searchButton = $("<div>").click(function() {
        SearchFormInitialize(searchButton);
    });
    startedApplications["TopBar"]["addElement"]($("<div>").addClass("separator float-right"));
    startedApplications["TopBar"]["addElement"](searchButton, 'search');

    var keysHandlers = function(e) {
        switch (e.which) {
            case 27:
                e.preventDefault();
                if (!e.ctrlKey) {
                    UserMenuInitialize(userMenuButton);
                } else {
                    if (!startedApplications["Chat"]["getJQElements"]('main-block').is(ContentApp["getApplication"]())) {
                        startedApplications["TopBar"]["detachElement"](openedApplication.name);
                        startedApplications["Menu"]["resetSelection"]();
                        showChat();
                    }
                }
                return false;
                break;
            case 70:
                if (e.ctrlKey) {
                    e.preventDefault();
                    SearchFormInitialize(searchButton);
                    return false;
                }
                break;
        }
    };
    addHandlers(win, "keydown", keysHandlers);
};
var closeHandlers = [];

var addHandlers = function(a, b, c) {
    closeHandlers.push({
        obj: a,
        action: b,
        method: c
    });
    a.bind(b, c);
};

var deleteHandlers = function(a, b, c) {
    var newHandlers = [];
    for (var i = 0; i < closeHandlers.length; i++) {
        var contr = closeHandlers[i];
        if (contr["obj"] != a && contr["action"] != b && contr["method"] != c) {
            newHandlers.push(contr);
        }
    }
    a.unbind(b, c);
    closeHandlers = newHandlers;
};

var closeController = function() {
    for (var i = 0; i < closeHandlers.length; i++) {
        var contr = closeHandlers[i];
        contr["obj"].unbind(contr["action"], contr["method"]);
    }
};

var messengerIni = function() {
    var button = $('<div>').addClass('chat-button float-left').click(function() {
        startApp("Messenger", function(app) {
            ContentApp["setApplication"](app["getJQElements"]('main-block'), button);
            startedApplications["Menu"]["resetSelection"]();
        });
    });
    startedApplications["TopBar"]["addElement"](button, 'messenger');
    startedApplications["TopBar"]["addElement"]($("<div>").addClass("separator float-left"));
};

//--- Инициализация чата ---
var chatInitialize = function() {
    var app = startedApplications["Chat"];
    startedApplications["TopBar"]["addElement"](app["getJQElements"]('chat-button').addClass("float-left"), 'chat');
    app.Event.addHandlers({
        "onSendMessage": function(message) {
            startedApplications["XMPPConnector"]["sendMessage"](message);
        }
    });
    app.Event.addHandlers({
        onChangeCount: function() {
            startedApplications["TopBar"]["iniAppsBar"]();
        }
    });
    app["getJQElements"]('chat-button').click(function() {
        ContentApp["setApplication"](app["getJQElements"]('main-block'), app["getJQElements"]('chat-button'));
        app['onShow']();
        startedApplications["Menu"]["resetSelection"]();
    });
    ContentApp["setApplication"](
        app["getJQElements"]('main-block'),
        app["getJQElements"]('chat-button')
    );
};

//--- Инициализайция приложения "Поиск" ---
var SearchFormInitialize = function(button) {
    startApp("Search", function(app) {
        ContentApp["setApplication"](app["getJQElements"]('main-block'), button);
    });
};

//--- Инициализация меню пользователя ---
var UserMenuInitialize = function(button) {
    var started = startedApplications["UserMenu"];
    startApp("UserMenu", function(app) {
        ContentApp["setApplication"](app["getJQElements"]('main-block'), button);
        if (!started) {
            iniUserMenu(app);
        }
    });
};

//--- Показываем/скрываем выбор языка ---
var triggerLanguageForm = function(app, params) {
    if (params.selected) {
        params.item.addClass("selected");
        app["getJQElements"]("main-block").appendTo(params.item);
    } else {
        params.item.removeClass("selected");
        app["getJQElements"]("main-block").detach();
    }
};

//--- Инициализация формы смены языка ---
var languageFormInitialize = function(app, params) {
    app.Event.addHandlers({
        onChange: function(code) {
            Functions.loadLanguage(code, function() {
                for (var i in startedApplications) {
                    if (startedApplications[i] && startedApplications[i]["setLanguage"]) {
                        startedApplications[i]["setLanguage"]();
                    }
                }
            });
        }
    });
};

//--- Инициализайция меню пользователя ---
var iniUserMenu = function(UserMenu) {
    UserMenu.Event.addHandlers({
        onSelectItem: function(itemParams) {
            if (itemParams["application"]) {
                var started = startedApplications[itemParams["application"]];
                startApp(itemParams["application"], function(application) {
                    switch (itemParams["application"]) {
                        case "StartLanguage":
                            triggerLanguageForm(application, itemParams);
                            if (!started) {
                                languageFormInitialize(application, itemParams);
                            }
                            break;
                    }
                });
                } else if (itemParams["controller"]) {
                for (var i in startedApplications) {
                    var apps = startedApplications[i];
                    apps.Close();
                    startedApplications[i] = false;
                }
                //SYSTEM["Controller"](itemParams["controller"]);
                closeController();
                Functions.logOut();
                window.location = window.location;
            }
        }
    });
};
var MainMenuInitialize = function() {
    var Menu = startedApplications["Menu"];
    Menu.Event.addHandlers({
        //--- Для отслеживания нажатий пунктов меню ---
        onChange: function(data) {
            startApplication(data);
        },
        onChangeMenu: function(data) {
            $.cookie("menu", JSON.stringify(data), {expires: 30});
        }
    })
};

//--- Вешаем обработчики событий приложений ---
var showChat = function() {
    ContentApp["setApplication"](
        startedApplications["Chat"]["getJQElements"]('main-block'),
        startedApplications["Chat"]["getJQElements"]('chat-button')
    );
};

var openedApplication = false;

var startApplication = function(data) {
    var started = startedApplications[data["application"]];
    var iniApp = function() {
        startApp(data["application"], function(app) {
            var topButton = startedApplications["TopBar"]["addElement"](false, data["label"], {
                appName: app.name,
                onclose: function() {
                    if (app["getJQElements"]('main-block').is(ContentApp["getApplication"]())) {
                        startedApplications["Menu"]["resetSelection"]();
                        showChat();
                    }
                },
                onselect: function(button) {
                    startedApplications["Menu"]["setSelection"](data["button"], data);
                    startApplication(data);
                }
            }, data["title"]);
            ContentApp["setApplication"](app["getJQElements"]('main-block'), topButton);
            app.onShow ? app.onShow() : false;
            openedApplication = app;
            if (started) return;

            switch(app.name) {
                case "Information":
                    informationInitialize(app);
                    break;
                case "Character":
                    characterInitialize(app);
                    break;
                case "Presents":
                    presentsInitialize(app);
                    break;
                case "Outfit":
                    outfitInitialize(app);
                    break;
                case "Transport":
                    transportInitialize(app);
                    break;
                case "Avatar":
                    avatarInitialize(app);
                    break;
                case "Authority":
                    authorityInitialize(app);
                    break;
                case "Friends":
                    friendsInitialize(app);
                    break;
            }
        });
    };
    iniApp();
};
var informationInitialize = function(app) {
    var items = app["getItems"]();
    for (var k = 0; k < items.length; k++) {
        if (items[k]["params"]["application"]) {
            iniDropItem(items[k]);
        }
    }
    app.Event.addHandlers({
        onChange: function(data) {
            data.button = startedApplications["Menu"].getItem(data.label) || data.button;
            startedApplications["Menu"]["setSelection"](data["button"], data);
            startApplication(data);
        }
    });
};

//--- Инициализация приложения Authority ---
var authorityInitialize = function(app) {
    app["setUserBalance"](getUserBalance());
};

var onAddToDesires = function() {
    appsFactory("Wishes")["updateList"]();
};

//--- Инициализация приложения Friends ---
var friendsInitialize = function(app) {
    startedApplications["XMPPConnector"]["getRoster"](function(data) {
        app["setFriendsList"](data);
    });
};

//--- Инициализация приложения Transport ---
var transportInitialize = function(app) {
    app.Event.addHandlers({
        "onAddToDesires": onAddToDesires,
        "onChangeBalance": getUserBalance
    });
    var SOURCE = RESOURCES["comp"][selfUser["male"] ? "male" : "female"];
    var shopApplication = startedApplications["Shop"]["shopShowcase"]({"products": SOURCE, "price-list": priceList["comp"]});
    app["setUserBalance"](getUserBalance());
    app["setShopApplication"](shopApplication);
};

//--- Инициализация приложения Character ---
var characterInitialize = function(app) {
    var SOURCE = RESOURCES["pers"][selfUser["male"] ? "male" : "female"];
    app.Event.addHandlers({
        "onAddToDesires": onAddToDesires,
        "onChangeBalance": getUserBalance
    });
    var shopApplication = startedApplications["Shop"]["shopShowcase"]({
        "products": SOURCE,
        "price-list": priceList["pers"]
    });
    app["setUserBalance"](getUserBalance());
    app["setShopApplication"](shopApplication);
};

//--- Инициализация приложения Presents ---
var presentsInitialize = function(app) {
    var SOURCE = RESOURCES["gifts"][selfUser["male"] ? "male" : "female"];
    app.Event.addHandlers({
        "onAddToDesires": onAddToDesires
    });
    var shopApplication = startedApplications["Shop"]["shopShowcase"]({
        "products": SOURCE,
        "price-list": priceList["gifts"]
    });
    app["setUserBalance"](getUserBalance());
    app["setShopApplication"](shopApplication);
};

//--- Инициализация приложения Outfit ---
var outfitInitialize = function(app) {
    var gender = selfUser["male"] ? "male" : "female",
        SOURCE = RESOURCES["clothes"][gender];
    var models = RESOURCES["models"][gender]["byInd"];
    var Personage = PersonagesApp["newPersonage"]({
        "gender": gender,
        "size": 3,
        "personage": {
            "models": selfUser["view"]["model"] ? selfUser["view"]["model"]["id"] : models[models.length - 1]["id"]
        },
        "loader": $('<div class="pre-loader medium">').append(
            $("<div>")
        )
    });
    Personage.load();
    app["setPersonage"](Personage);
    app.Event.addHandlers({
        "onAddToDesires": onAddToDesires,
        "onChangeBalance": getUserBalance
    });
    var shopApplication = startedApplications["Shop"]["shopShowcase"]({
        "products": SOURCE,
        "price-list": priceList["clothes"]
    });
    app["setUserBalance"](getUserBalance());
    app["setShopApplication"](shopApplication);
};

var avatarInitialize = function(app) {
    app.Event.addHandlers({
        "onChangeBalance": getUserBalance
    });
    app["setUserBalance"](getUserBalance());
};

//--- Инициализация пункта меню для перетаскивания в доп. меню ---
var iniDropItem = function(item) {
    var mainMenuObject = startedApplications["Menu"];
    var jqItem = item['item'];
    var params = item['params'];
    var startPosition = {}, cloneItem = false, bodyMask = false;
    var containerParameters = {};
    var containerDop = mainMenuObject["getDopMenuContainer"]();
    var mouseDown = function(e) {
        var offsetItem = jqItem.offset();
        startPosition = {
            top: e.clientY - offsetItem.top,
            left: e.clientX - offsetItem.left
        };
        addHandlers(win, "mousemove", createCloneItem);
        addHandlers(win, "mouseup", resetHandlers);
        deleteHandlers(jqItem, "mousedown", mouseDown);
    };
    var resetHandlers = function() {
        addHandlers(jqItem, "mousedown", mouseDown);
        deleteHandlers(win, "mousemove", createCloneItem);
        deleteHandlers(win, "mouseup", resetHandlers);
    };
    var inDropArea = false;
    var createCloneItem = function(e) {
        e.preventDefault();
        cloneItem = $("<div>").css({
            position: "absolute",
            zIndex: 99,
            left: e.clientX - startPosition.left,
            top: e.clientY - startPosition.top
        }).addClass(jqItem[0].className).html(jqItem.html());
        bodyMask = $("<div>").css({
            position: "absolute",
            left: 0,
            top: 0,
            zIndex: 100,
            width: "100%",
            height: "100%"
        }).appendTo(body);
        cloneItem.appendTo(body);

        mainMenuObject["activateForDrag"]();
        var h = containerDop["outerHeight"](),
            w = containerDop["outerWidth"](),
            offset = containerDop.offset();

        containerParameters = {
            left: offset.left,
            top: offset.top,
            fleft: offset.left + w,
            ftop: offset.top + h
        };
        deleteHandlers(win, "mousemove", createCloneItem);
        deleteHandlers(win, "mouseup", resetHandlers);
        addHandlers(win, "mousemove", mouseMove);
        addHandlers(win, "mouseup", endDrag);
        return false;
    };
    var mouseMove = function(e) {
        e.preventDefault();
        cloneItem.css({
            "left": e.clientX - startPosition.left,
            "top": e.clientY - startPosition.top
        });
        if ((e.clientX > containerParameters.left && e.clientX < containerParameters.fleft) &&
            (e.clientY > containerParameters.top && e.clientY < containerParameters.ftop)) {
            inDropArea = true;
            containerDop.addClass('dropped');
        } else {
            if (inDropArea) {
                inDropArea = false;
                containerDop.removeClass('dropped');
            }
        }
        return false;
    };
    var endDrag = function() {
        deleteHandlers(win, "mousemove", mouseMove);
        deleteHandlers(win, "mouseup", endDrag);
        addHandlers(jqItem, "mousedown", mouseDown);
        mainMenuObject["deActivateForDrag"]();
        bodyMask.remove();
        cloneItem.remove();
        if (inDropArea) {
            containerDop.removeClass('dropped');
            var allItems;
            if (allItems = mainMenuObject["addItem"](params)) {
                mainMenuObject["iniHeight"]();
                $.cookie("menu", JSON.stringify(allItems), {expires: 30});
            }
        }
    };
    addHandlers(jqItem, "mousedown", mouseDown);
};
var XMPPConnectorInitialize = function() {
    startedApplications["XMPPConnector"].join(selfUser["id"], selfUser["community"]);
};
var countryInitialize = function() {
    var bgs = RESOURCES["bg"]["male"]["byInd"];
    startedApplications["CountryPreview"]["setBackground"](
        bgs[bgs.length - 1]["views"]["size-2"]["img"]
    );
    startedApplications["CountryPreview"]["getJQElements"]("main-block").prependTo(
        ContentApp["getJQElements"]("main-block")
    );
    startedApplications["Menu"]["iniHeight"]();
    startedApplications["CountryPreview"]["iniCountry"]();
    startedApplications["UsersChat"]["setPersonageFactory"](PersonagesApp["newPersonage"]);
    startedApplications["UsersChat"].Event.addHandlers({
        onRemovePersonage: function(data) {
            startedApplications["CountryPreview"]["removePersonage"](data);
            startedApplications["Chat"]["delUser"]();
        },
        onCreatePersonage: function(data) {
            startedApplications["CountryPreview"]["addPersonage"](data);
            startedApplications["Chat"]["addUser"]();
        },
        onUpdateUser: startedApplications["Chat"]["updateUserOfMessages"]
    });

    startedApplications["XMPPConnector"].Event.addHandlers({
        onPresences: function(data) {
            for (var i = 0; i < data.length; i++) {
                startedApplications["UsersChat"]["User"](data[i]);
            }
        },
        onMessages: function(data) {
            for (var k = 0; k < data.length; k++) {
                switch (data[k]["type"]) {
                    case "groupchat":
                        switch (data[k]["message"]["type"]) {
                            case "text":
                                var userId = data[k]["from"][1], user = startedApplications["UsersChat"]["getUser"](userId);
                                if (!user) {
                                    startedApplications["UsersChat"]["loadUserData"](userId, function(userData) {
                                        startedApplications["Chat"]["updateUserOfMessages"](userData);
                                    });
                                }
                                data[k]['user'] = user || false;
                                if (userId == selfUser["id"]) {
                                    data[k]["self"] = true;
                                }
                                startedApplications["Chat"]["addMessage"](data[k]);
                                break;
                            case "json":
                                startedApplications["UsersChat"]["updateUser"](data[k]["message"]["json"]);
                                break;
                        }
                        break;
                }
            }
        }
    });
};
