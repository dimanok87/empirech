var body, win;
var $ = jQuery;
var Functions = {};

var translateCollections = {};
var TRANSLATES = false;

/* Константы для работы с приложениями */
var _CONSTS_ = {
    "PHPServer": "/proxy.au/",
    "APIHost": "/proxy/AS2/",
    "UserPicPath": "/proxy.ud/{sz}/{userId}",
    "SourcesPath": "/proxy.rs/i/",
    "ApplicationPath": "js/apps/{{APP_NAME}}.js",
    "ControllersPath": "js/controllers/{{CONTROLLER_NAME}}.js",
    "CollectionSource": 'http://rs.empirech.com/i/'
};

var isWebStorage = 'localStorage' in window && window['localStorage'] !== null;

(function($) {

    Functions.loadApplication = function(appName, callback) {
        $.ajax({
            url: _CONSTS_["ApplicationPath"].replace(/\{\{APP_NAME\}\}/g, appName),
            cash: false,
            dataType: 'text',
            success: function(data) {
                if (data) {
                    callback(Function(data));
                }
            }
        });
    };

    Functions.startController = function(controllerName, callback) {
        $.ajax({
            url: _CONSTS_["ControllersPath"].
                replace(/\{\{CONTROLLER_NAME\}\}/g, controllerName),
            dataType: 'text',
            success: function(data) {
                callback(Function(data));
            }
        });
    };

    Functions.sendForm = function(url, form) {
        var formData = new FormData();
        $("input", form).each(function() {
            var name = this.getAttribute("name");
            var val = false;
            if (this.getAttribute("type") != "file") {
                val = this.value;
                formData.append(name, val);
            } else {
                for (var i = 0, file; file = this.files[i]; i++) {
                    formData.append(name, file);
                }
            }
        });
        var xhr = new XMLHttpRequest();
        xhr.open('POST', _CONSTS_["APIHost"] + url, true);
        xhr.setRequestHeader('Authorization', getDigestHeader({method : "POST"}));
        xhr.send(formData);
    };

    Functions.getCaptcha = function(cb) {
        $.ajax({
            url: _CONSTS_["PHPServer"] + "?action=captcha",
            dataType: "html",
            success: function(data, textStatus, request) {
                cb({
                    image: data,
                    CKey: request.getResponseHeader('CKey')
                });
            }
        });

    };

    var _A1 = false, nc = 1, cnonce, uri = location.origin, realm = "Empire Realm", nonce, login;

    var getDigestHeader = function(data) {
        if (!nonce) return false;
        var response = [
            'realm="' + realm + '"',
            'username="' + login + '"',
            'nonce="' + nonce + '"',
            'qop=auth',
            'uri="' + uri + '"',
            'nc=' + nc,
            'cnonce="' + cnonce + '"',
            'response="' + MD5.hexdigest(_A1 + ':' + nonce + ':' + nc + ':' + cnonce + ':auth:' + MD5.hexdigest(data["method"] + ":" + uri)) + '"'
        ];
        nc++;
        return "Digest " + response.join(', ');
    };

    Functions.logOut = function() {
        _A1 = false;
        nc = 1;
        cnonce = false;
        nonce = false;
        login = false;
        $.cookie("USID", false, {expires: -1});
    };

    var updateDigest = function(userData, cb) {
        if (!(login || userData)) return;
        $.ajax({
            url: _CONSTS_["PHPServer"] + "?action=auth",
            dataType: "json",
            success: function(data) {
                nonce = data["nonce"] + "==";
                cnonce = MD5.hexdigest((Math.random() * 1234567890));
                login = login || userData["login"].toString().toLowerCase();
                _A1 = _A1 || MD5.hexdigest(login + ":" + realm + ":" + userData["pwd"]);
                cb ? cb() : false;
            }
        });
    };

    Functions.authUser = function(userData, callback) {
        var cookie = $.cookie("USID");
        if (!(userData || cookie)) return false;
        if (cookie) {
            var arrCookie = cookie.split(" ");
            _A1 = arrCookie[0];
            login = arrCookie[1];
        }
        updateDigest(userData, function() {
            $.ajax({
                url: _CONSTS_["APIHost"] + "api/user",
                dataType: "json",
                headers: {
                    'Authorization': getDigestHeader({method : "GET"})
                },
                success: function(data) {
                    callback ? callback(data) : false;
                    $.cookie("USID", _A1 + " " + login, userData["remember"] ? {expires: 30} : false);
                },
                error: function(error) {
                    login = false;
                    _A1 = false;
                    callback ? callback({"error": error.status}) : false;
                }
            });
        });
        return true;
    };
    setInterval(updateDigest, 200000);
    Functions.sendToAPI = function(url, callback, type, data) {
        callback = callback || function(){};
        var requestData = {
            url: _CONSTS_["APIHost"] + url,
            contentType: "application/json",
            dataType: "text",
            success: function(text) {
                var data = text ? JSON.parse(text) : {};
                callback(data, text);
            },
            error: function(error) {
                callback({"error": error.status});
            },
            complete: function() {}
        };
        if (data) {
            requestData["data"] = JSON.stringify(data);
            requestData["type"] = "POST";
        } else {
            requestData["type"] = "GET";
        }
        var DGheader = false;
        if (DGheader = getDigestHeader({"method": requestData["type"]})) {
            requestData.headers = {'Authorization': DGheader};
        }
        $.ajax(requestData);
    };



    Functions.getBase64Image = function(file, callback) {
        var filereader = new FileReader();
        filereader.onload = function(event) {
            callback ? callback(event.target.result) : false;
        };
        filereader.readAsDataURL(file);
    };

    Functions.getLanguage = function() {
        return LNG_CODE;
    };
    var LNG_CODE = false;
    Functions.loadLanguage = function(code, callback) {
        $.cookie("lng", code, {expires: 30});
        if (translateCollections[LNG_CODE = code]) {
            TRANSLATES = translateCollections[code];
            callback();
            return;
        }
        if (isWebStorage && window.localStorage["lng-" + code]) {
            TRANSLATES = translateCollections[code] = JSON.parse(window.localStorage["lng-" + code]);
            callback();
            return;
        }
        $.ajax({
            url: "/languages/" + code + ".json",
            dataType: "JSON",
            success: function(data) {
                window.localStorage["lng-" + code] = JSON.stringify(data);
                TRANSLATES = translateCollections[code] = data;
                callback();
            }
        });
    };

    //------------------------------------------------------------------------------------------------------------//                                    //---- Новые методы для работы с ресурсами ---//

    var GENDERS = {
        "g1": {
            name: "male",
            url_element: "m"
        },
        "g2": {
            name: "female",
            url_element: "f"
        }
    };
    var SOURCES_PATHS = {
        "bodies": "body/{gender}/{size}/{file}",
        "heads": "head/{gender}/{size}/{file}",
        "gifts": "gift/{size}/{category}/{file}",
        "pers": "pers/{gender}/{size}/{category}/{file}",
        "comp": "cars/{size}/{file}",
        "bg": "bg/{size}/{file}",
        "models": "model/{gender}/{size}/{file}",
        "clothes": "clothes/{gender}/{size}/{file}",
        "magic": "magic/{size}/{file}"
    };

    var SOURCES = {
        "products": {},
        "categories": {}
    };

    var createSourceElement = function(data) {
        var params = data["element"],
            productUID = "element-" + params["id"],
            source = data["source"];
        var gend = GENDERS["g" + params["gender"]] || false;

        var cats = SOURCES["categories"][source] || false;
        cats = cats["category-" + params["stype"]] || false;

        if (!SOURCES[source]["all"][productUID]) {
            var el = SOURCES[source]["all"][productUID] = {
                id: params["id"],
                name: params["name"],
                source: source,
                views: {},
                type: params["stype"]
            };

            switch (params["gender"]) {
                case 1:
                    SOURCES[source][GENDERS["g1"]["name"]]["byId"][productUID] = el;
                    SOURCES[source][GENDERS["g1"]["name"]]["byInd"].push(el);
                    break;
                case 2:
                    SOURCES[source][GENDERS["g2"]["name"]]["byId"][productUID] = el;
                    SOURCES[source][GENDERS["g2"]["name"]]["byInd"].push(el);
                    break;
                case 0:
                    SOURCES[source][GENDERS["g1"]["name"]]["byId"][productUID] = el;
                    SOURCES[source][GENDERS["g1"]["name"]]["byInd"].push(el);
                    SOURCES[source][GENDERS["g2"]["name"]]["byId"][productUID] = el;
                    SOURCES[source][GENDERS["g2"]["name"]]["byInd"].push(el);
                    break;
            }
            if (data["product"]) {
                SOURCES["products"]["product-" + params["id"]] = el;
            }
            if (data["fullView"]) {
                el["views"]["fv"] =  _CONSTS_["SourcesPath"] + SOURCES_PATHS[source].
                    replace('{gender}', gend ? gend["url_element"] : "").
                    replace('{size}', data["fullView"]["size"]).
                    replace('{file}', (data["fullView"]["type"] ? data["fullView"]["type"] + "/" : "") + params["view"]["fname"]).
                    replace('{category}', cats ? cats["category"] || "" : "")
            }
        }

        var element = SOURCES[source]["all"][productUID];
        var view = params["view"] || false;

        if (view) {
            element["views"]["size-" + data["size"]] = {
                x: isNaN(view.x) ? false : view.x,
                y: isNaN(view.y) ? false : view.y,
                h: isNaN(view.h) ? false : view.h,
                w: isNaN(view.w) ? false : view.w,
                img: _CONSTS_["SourcesPath"] + SOURCES_PATHS[source].
                    replace('{gender}', gend ? gend["url_element"] : "").
                    replace('{size}', data["size"]).
                    replace('{file}', params["view"]["fname"]).
                    replace('{category}', cats ? cats["category"] || "" : "")
            };
        }
    };

    var loadOneSizeProducts = function(params, cb) {
        var itemSize = params["size"] || false,
            nameSource = params["source"],
            isProduct = params["product"];

        SOURCES[nameSource] = SOURCES[nameSource] || {
            all: {},
            male: {
                byId: {},
                byInd: []
            },
            female: {
                byId: {},
                byInd: []
            }
        };
        var createSourcesObject = function(data) {
            for (var k = 0; k < data.length; k++) {
                createSourceElement({
                    element: data[k],
                    size: itemSize,
                    source: nameSource,
                    product: isProduct,
                    fullView: params["shopFv"]
                });
            }
            cb(data);
        };

        if (isWebStorage && window.localStorage[nameSource + (itemSize ? '_' + itemSize : '')]) {
            createSourcesObject(JSON.parse(window.localStorage[nameSource + (itemSize ? '_' + itemSize : '')]));
            return;
        }
        Functions.sendToAPI('api/p/' + nameSource + (itemSize ? '?sz=' + itemSize : ''), function(data, originalData) {
            if (isWebStorage && (originalData || false)) {
                window.localStorage[nameSource + (itemSize ? '_' + itemSize : '')] = originalData;
            }
            createSourcesObject(data);
        })
    };


    Functions.loadSource = function(params, cb) {
        var size = params["params"]["size"] || [false],
            nameSource = params["name"],
            sourceSize = 0;
        for (var i = 0; i < size.length; i++) {
            loadOneSizeProducts({
                size: size[i],
                source: nameSource,
                product: params["params"]["product"] || false,
                shopFv: params["params"]["shopFv"] || false
            }, function(data) {
                sourceSize++;
                if (sourceSize == size.length) cb(data);
            });
        }
    };
    Functions.getSources = function() {
        return SOURCES;
    };
    Functions.setImageToBlock = function(image, block, fl) {
        fl = fl || false;
        $(image).css({width: '', height: ''});
        var containerSize = {
            w: block.width(),
            h: block.height()
        };
        var widthK = containerSize['w'] / $(image).width(), heightK = containerSize['h'] / $(image).height();
        if ((widthK > 1 && widthK < heightK) || (widthK < heightK)) {
            if (!fl || $(image).height() > containerSize["h"]) {
                $(image).css({
                    height: '100%'
                });
            }

            $(image).css({
                'margin-top': '0',
                'margin-left': - ($(image).width() - containerSize['w']) / 2
            });
        } else {
            if (!fl || $(image).width() > containerSize["w"]) {
                $(image).css({
                    width: '100%'
                });
                $(image).css({
                    'margin-left': '0',
                    'margin-top': - ($(image).height() - containerSize['h']) / 2
                });
            }
        }
    };

    Functions.loadCategory = function(params, cb) {
        var createCategoriesList = function(data) {
            var cat = SOURCES["categories"][params["name"]] = {};
            if (!data["male"]) {
                for (var z = 0; z < data.length; z++) {
                    cat["category-" + data[z]["id"]] = {
                        type: data[z]["id"],
                        label: data[z]["name"],
                        title: data[z]["name"],
                        category: data[z]["name"]
                    };
                }
            } else {
                for (var g in data) {
                    cat[g] = {};
                    for (var z = 0; z < data[g].length; z++) {
                        cat[g]["category-" + data[g][z]["id"]] = {
                            type: data[g][z]["id"],
                            label: data[g][z]["label"],
                            title: data[g][z]["title"],
                            category: data[g][z]["label"]
                        };
                    }
                }
            }
            cb(cat);
        };
        if (isWebStorage && window.localStorage["categories_" + params["name"]]) {
            createCategoriesList(JSON.parse(window.localStorage["categories_" + params["name"]]));
            return;
        }
        $.ajax({
            url: params["url"],
            dataType: "JSON",
            success: function(data) {
                if (isWebStorage) {
                    window.localStorage["categories_" + params["name"]] = JSON.stringify(data);
                }
                createCategoriesList(data);
            }
        });
    };

    $(function() {
        body = $("body:first");
        win = $(window);
    });
})(jQuery);
